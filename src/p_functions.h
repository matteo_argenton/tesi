#include <stdlib.h>

#ifndef __LBM_FUNC_H
#define __LBM_FUNC_H



#define scalar_index(x_, y_    ) ( (NX+2)*(y_) + (x_) )
#define field_index( x_, y_, d_) ( (NX+2)*((NY+2)*(d_)+ (y_) ) + (x_) ) //+2 per presenza halo layer attorno al reticolo efficace di dimensioni NX*NY


/*-------------------------------------geometria del reticolo--------------------------------*/
//dimensioni che definiscono l'area efficace, senza considerare un'ulteriore cornice di halo layer
#define NX (800)
#define NY (200)

//vecchie condizioni leggermente asimmetriche
/*
const unsigned int INLET_START = (NX+2)/3;   //cella di inizio inlet, il +2 perché poi nel codice le dimensioni (in questo caso X, hanno lunghezza NX+2 per via dell'halo)
const unsigned int INLET_DIM   = 6;	     	 //numero di celle che compongono l'inlet

const unsigned int OUTLET_DIM   = INLET_DIM;	
const unsigned int OUTLET_START = 2*(NX+2)/3 - OUTLET_DIM ;   
*/

//nuove condizioni simmetriche

const unsigned int INLET_START  = (NX+2)/3;
const unsigned int INLET_DIM    = 6;

const unsigned int OUTLET_DIM   = INLET_DIM;	
const unsigned int OUTLET_START = (NX+1) - (NX+2)/3 - INLET_DIM +1;


//inversione in/out
/*
const unsigned int OUTLET_START = (NX+2)/3;
const unsigned int INLET_DIM   = 6;

const unsigned int OUTLET_DIM   = INLET_DIM;	
const unsigned int INLET_START = (NX+1) - (NX+2)/3 - INLET_DIM +1;
*/
/*--------------------------------variabili simulazione (passi, soglie)----------------------*/

const unsigned int STEPS   = 1000;
const double THRESHOLD_RHO = 1e-10;
const double THRESHOLD_U   = 1e-10;


const double tau   = 1.5;
const double nu    = ( tau - 0.5 )*(1./3.);
const double rho_0 = 1.0;

/*---------------------------------------------stencil----------------------------------------*/

const unsigned int ndir  = 9;
const size_t size_ndir   = sizeof(double)*(NX+2)*(NY+2)*ndir;  //il +2 è per generare un halo layer unitario attorno all'area "efficace"
const size_t size_scalar = sizeof(double)*(NX+2)*(NY+2);	   //anche i campi scalari hanno una cornice di halo, ma solo per uniformare gli indici 
															   //l'area efficace va da 1 a NX inclusi o NX+1 escluso (utile per i cicli), idem in Y

const size_t num_scalar  = (NX+2)*(NY+2);					   //è come sopra, ma da usare per la calloc, che ha parametri di ingresso diversi dalla malloc

				   //

const double w0 = 4.0 / 9.0;  // zero weight
const double ws = 1.0 / 9.0;  // adjacent weight
const double wd = 1.0 /36.0;  // diagonal weight


const double wi[] = {w0,ws,wd,ws,wd,ws,wd,ws,wd};
const int  dirx[] = {0 ,1 ,1 ,0 ,-1,-1,-1, 0, 1};
const int  diry[] = {0 ,0 ,1 ,1 , 1, 0,-1,-1,-1};
const int   opp[] = {0 ,5 ,6 ,7 , 8, 1, 2, 3, 4};



/*-------------------------------------velocità inlet e outlet---------------------------------*/

const double ux_in  = 0.0;
const double uy_in  = 1e-5;

const double ux_out = 0.0;
const double uy_out = -1e-5;

/*-----------------------------------parametri forzante esterna--------------------------------*/


const double tau_D 	   = 1e5;    //questo valore è per ora molto a caso perché non so trasformare le unità del reticolo in unità fisiche
const double C_g_inv   = 1e-5;   //lavoriamo in local capacitance approximation e i valori consigliati sono, per ora, 1e3 - 1e6 (a denominatore. Io moltiplico per l'inverso)



/*----------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------*/


void electrons_init(unsigned int,double*,double*,double*);			//inizializzazione problema (densità e velocità iniziali)
void init_equilibrium(double*,double*,double*,double*);				//calcolo equilibrio pre-simulazione
double f_eq(int , int , int , double, double, double*);				//calcolo funzione di equilibrio date le coordinate, l'indice di velocità, due valori ux, uy e il vettore delle densità

double avg_rho(double*);  											//calcolo massa o densità del sistema. Massa per ora.
void avg_u(double*, double*, double*);								//calcolo modulo velocità punto per punto
double avg_u_diff(double*, double*);								//media della differenza punto per punto del modulo di u tra due popolazioni a passi diversi
void knudsen_check();


#endif
