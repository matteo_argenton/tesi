#include "debug_func.h"
#include "p_functions.h"
#include <stdio.h>


void print_population_point(const char* filepath, double *pop, int x, int y){

	FILE* fp;

	char fname[200];

	sprintf(fname, "%s//pop(%d,%d).dat", filepath, x,y);

	fp=fopen(fname, "w");


	for(int i=0; i<ndir; i++){
		fprintf(fp, "%d = %lf\n", i, pop[field_index(x,y,i)]);
	}

	fclose(fp);


}


void print_population_set(const char* filepath, double *pop, int x_in, int x_end, int y_in, int y_end){

	FILE* fp;

	char fname[200];

	sprintf(fname, "%s//pop.dat", filepath);

	fp=fopen(fname, "w");


	for(int y = y_in; y <= y_end; y++){

		for(int x = x_in; x <= x_end; x++){

			for(int i=0; i<ndir; i++){
				fprintf(fp, "%d,%d (%d) = %lf\n", x,y,i, pop[field_index(x,y,i)]);
			}
		}	
	}


	fclose(fp);

}



void print_population_borders(const char* filepath, double *pop){

	FILE* fp;

	char fname[200];

	sprintf(fname, "%s//pop_borders.dat", filepath);

	fp=fopen(fname, "w");


		/*bordo inferiore*/
		fprintf(fp, "BORDO INFERIORE:\n\n");
		for(int x=1; x<=NX; x++){
			for(int i=0; i<ndir; i++){
				fprintf(fp, "x=%d, y=1, i=%d\tpop=%.10lf\n", x,i,pop[field_index(x,1,i)]);
			}
		}

		/*bordo superiore*/
		fprintf(fp, "BORDO SUPERIORE:\n\n");
		for(int x=1; x<=NX; x++){
			for(int i=0; i<ndir; i++){
				fprintf(fp, "x=%d, y=%d, i=%d\tpop=%.10lf\n", x,NY,i,pop[field_index(x,NY,i)]);
			}
		}

		/*lato sx - ripeto gli angoli per comodità*/
		fprintf(fp, "LATO SINISTRO:\n\n");
		for(int y=1; y<=NY; y++){
			for(int i=0; i<ndir; i++){
				fprintf(fp, "x=1, y=%d, i=%d\tpop=%.10lf\n", y,i,pop[field_index(1,y,i)]);
			}
		}

		/*lato dx - ripeto gli angoli per comodità*/
		fprintf(fp, "LATO DESTRO:\n\n");
		for(int y=1; y<=NY; y++){
			for(int i=0; i<ndir; i++){
				fprintf(fp, "x=%d, y=%d, i=%d\tpop=%.10lf\n", NX,y,i,pop[field_index(NX,y,i)]);
			}
		}

	fclose(fp);

}

void print_velocity_borders(const char* filepath, double *ux, double *uy){

	FILE* fp;

	char fname[200];

	sprintf(fname, "%s//u_borders.dat", filepath);

	fp=fopen(fname, "w");



		/*bordo inferiore*/
		fprintf(fp, "BORDO INFERIORE:\n\n");
		for(int x=1; x<=NX; x++){

			fprintf(fp, "x=%d, y=1 \t ux=%.10lf \t uy=%.10lf\n", x, ux[scalar_index(x,1)], uy[scalar_index(x,1)]);

		}

		/*bordo superiore*/
		fprintf(fp, "BORDO SUPERIORE:\n\n");
		for(int x=1; x<=NX; x++){

			fprintf(fp, "x=%d, y=1 \t ux=%.10lf \t uy=%.10lf\n", x, ux[scalar_index(x,NY)], uy[scalar_index(x,NY)]);
		
		}

		/*lato sx - ripeto gli angoli per comodità*/
		fprintf(fp, "LATO SINISTRO:\n\n");
		for(int y=1; y<=NY; y++){

			fprintf(fp, "x=%d, y=1 \t ux=%.10lf \t uy=%.10lf\n", y, ux[scalar_index(1,y)], uy[scalar_index(1,y)]);
		
		}

		/*lato dx - ripeto gli angoli per comodità*/
		fprintf(fp, "LATO DESTRO:\n\n");
		for(int y=1; y<=NY; y++){

			fprintf(fp, "x=%d, y=1 \t ux=%.10lf \t uy=%.10lf\n", y, ux[scalar_index(NX,y)], uy[scalar_index(NX,y)]);
			
		}

	fclose(fp);

}

//procedura per ottenere un file con i dati di un qualunque campo scalare in ingresso. Punto per punto
void get_scalar_data(const char* filepath, double *scalar, const char* name){ 

	FILE* fp;

	char fname[200];

	sprintf(fname, "%s//debug_data_%s.dat", filepath, name);

	fp=fopen(fname, "w");

		for(unsigned int y = 1; y < NY+1; y++){

			for(unsigned int x = 1; x < NX+1; x++){

				fprintf(fp, "(%u %u) -> %.20lf\n" ,y,x,scalar[scalar_index(x,y)]);

			}
		}
	fclose(fp);
}
