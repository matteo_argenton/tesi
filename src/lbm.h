#ifndef __LBM_MAIN_H
#define __LBM_MAIN_H


void stream(double*, double*, double*);
void compute_rho_u(double*, double*, double*, double*);										//calcolo densità e velocità punto per punto
void collide(double*, double*, double*, double*, double*, double*, double*, double*);
void compute_rho(double*, double*);		
void compute_u(double*, double*, double*,double*, double*, double*, double*, double*);		//calcolo delle sole velocità lungo x e y
void potential_calc(double*, double*);														//calcolo del pontenziale punto per punto
void compute_Fx(double*, double*, double*);													//calcolo componente x della forzante
void compute_Fy(double*, double*, double*);


#endif