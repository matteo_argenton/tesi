#ifndef _FORCING
#define _FORCING

void compute_Fx(double*, double*, double*);     	//calcolo della componente x della forzante legata al potenziale elettrico (densità di forza)
void compute_Fy(double*, double*, double*);

void compute_Fx_diss(double*, double*, double*);	//calcolo componente x della forzante legata alla dissipazione con il reticolo
void compute_Fy_diss(double*, double*, double*);

void potential_calc(double*, double*);			//calcolo potenziale


#endif