#include "p_functions.h"
#include <math.h>
#include <stdio.h>
#include "dump.h"
#include "lbm.h"
#include "macro.h"                    /*contiene le macro per i blocchi #ifdef*/
#include "debug_func.h"
#include "forcing.h"


int main(int argc, char* argv[]){


	double *f1 		  = (double*) malloc(size_ndir);
	double *f2 		  = (double*) malloc(size_ndir);
	double *rho 	  = (double*) malloc(size_scalar);
	double *ux 		  = (double*) malloc(size_scalar);
	double *uy 		  = (double*) malloc(size_scalar);
	double *u_mod_old = (double*) malloc(size_scalar);
	double *u_mod_new = (double*) malloc(size_scalar);
	double *potential = (double*) malloc(size_scalar);
	double *Fx 		  = (double*) calloc(num_scalar, sizeof(double));         //vengono inizializzati a 0 per non avere problemi se anche le forzanti fossero disabilitate
	double *Fy 		  = (double*) calloc(num_scalar, sizeof(double));
	double *Fx_diss   = (double*) calloc(num_scalar, sizeof(double));
	double *Fy_diss   = (double*) calloc(num_scalar, sizeof(double));


//	double *Fx 		  = (double*) malloc(size_scalar);
//	double *Fy 		  = (double*) malloc(size_scalar);
//	double *Fx_diss   = (double*) malloc(size_scalar);
//	double *Fy_diss   = (double*) malloc(size_scalar);


	unsigned int iteration = 0;
	double 		 old_avg_rho, new_avg_rho, diff_rho;
	double       diff_u;
	int          debug_flag = 0;

	const char 	 *dump_folder_name;

	//stampa a schermo lo stato della macro FEQ
	#ifdef FEQ
		printf("FEQ attiva \n");
	#else
		printf("FEQ *non* attiva\n");
	#endif


	//controllo che i parametri abbiano un senso - knudsen
	knudsen_check();

	//dump folder
	init_dump_folder();
	dump_folder_name = setup_dump_folder(NX,NY,INLET_DIM);

	//dump file con caratteristiche simulazione
	dump_settings(dump_folder_name);

	//rho, ux, uy initialization at t=0
	electrons_init(0,rho,ux,uy);


	//f1 initialization as equilibrium distribution at t=0
	init_equilibrium(f1,rho,ux,uy);

	//mass computation for conservation check
	old_avg_rho = avg_rho(f1);

	//pointwise velocity modulus computation
	avg_u(ux, uy, u_mod_old);

	//info sulle caratteristiche geometriche del reticolo, da usare nel fit - da implementare un dump in un qualche file
	printf("********************\n");
	printf("inizio inlet: %d\t, fine inlet %d\n", INLET_START, INLET_START+INLET_DIM-1);   		//il -1 perché nel ciclo delle boundary c'è un < e non <=
	printf("inizio outlet: %d\t, fine outlet %d\n", OUTLET_START, OUTLET_START+OUTLET_DIM-1);   //il -1 perché nel ciclo delle boundary c'è un < e non <=
	printf("********************\n");

	do{

		iteration++;

		for(unsigned int i=0; i<STEPS; i++){

			stream(f1,f2,rho); 	

			compute_rho(f2, rho);

			potential_calc(rho, potential);

			#ifdef F_INDUCED
			compute_Fx(potential, Fx, rho);
			compute_Fy(potential, Fy, rho);
			#endif

			compute_u(f2, ux, uy, rho, Fx, Fy, Fx_diss, Fy_diss);
	
			#ifdef F_DISS
			compute_Fx_diss(rho, ux, Fx_diss);
			compute_Fy_diss(rho, uy, Fy_diss);
			#endif

			collide(f2, rho, ux, uy, Fx, Fy, Fx_diss, Fy_diss);

			double *temp = f1;
			f1 = f2;
			f2 = temp;
		}

		//computation of rho, ux, uy to be used for macroscopic variable convergence check. Without this we use pre-last-collision values even if f1 is up to date
		/*Non sono affatto sicuro che sia corretto calcolare ancora le var macro. Perché questo introduce un doppio calcolo delle stesse ad ogni inizio iterazione*/
		/*la disattivo. In questo modo perdo l'ultima collide a livello di rho, ux, uy. Che non è drammatico, direi*/

		//compute_rho_u(f1,rho,ux,uy);                                          //   <<<<<<---------------------------  DA PENSARE BENE

		get_main_data_iter(dump_folder_name, f1, rho, ux, uy, potential, Fx, Fy, Fx_diss, Fy_diss, iteration);


		new_avg_rho = avg_rho(f1);
		diff_rho = abs(new_avg_rho - old_avg_rho);

		//pointwise u modulus calculation from current ux, uy values
		avg_u(ux, uy, u_mod_new);
		diff_u = abs(avg_u_diff( u_mod_new, u_mod_old ));



		printf("Iterazione numero %u, differenza massa attuale %.16lf, differenza target %.16lf, densità attuale %.16lf, densità_old= %.16lf\n", iteration, diff_rho, THRESHOLD_RHO, new_avg_rho, old_avg_rho);
		printf("Iterazione numero %u, differenza velocità attuale %.20lf\n", iteration, diff_u);


		/*resettig the variables for the new iterations*/

		old_avg_rho=new_avg_rho;         

		double *temp      = u_mod_old;
			    u_mod_old = u_mod_new;
			    u_mod_new = temp;
		/************************************************/


/* blocco di debug, non elegante, solo per comodità, da sistemare*/

#ifdef debug

		debug_flag = 1;
		
	} while(debug_flag = 0);

	print_population_borders(dump_folder_name, f1);      //esporto lo stato delle popolazioni di bordo
	print_velocity_borders(dump_folder_name, ux, uy);	 //esporto le velocità dei punti di bordo per verificare che siano nulle

#else

	//} while((diff_rho>THRESHOLD_RHO) || (diff_u>THRESHOLD_U));
	  } while(diff_u>THRESHOLD_U);
#endif
	

	free(f1);
	free(f2);
	free(rho);
	free(ux);
	free(uy);
	free(u_mod_new);
	free(u_mod_old);
	free(Fx);
	free(Fy);
	free(potential);
	free(Fx_diss);
	free(Fy_diss);

	return 0;
}