#include "forcing.h"
#include "p_functions.h"


void compute_Fx(double* pot, double* Fx, double* rho){

	//se mi trovo tra due punti di fluido (nella direzione della derivata) uso d(f)/d(x) = [f(x+delta) - f(x-delta)] / (2*delta)  -> errore delta^2
	//altrimenti uso solo il punto prima o il punto dopo, con un errore nell'ordine delta  (e non divido per 2, classica derivata)

	//in questa simulazione tutti i punti sono fluido, ma ho comunque dei punti di interfaccia. Per i punti interni uso la prima formula
	//per i punti di bordo uso la seconda, nel limite destro o sinistro, alto o basso, in base a dove si trovano sul bordo

	//una versione migliorata dovrà prevedere un controllo meno becero sulla natura del punto


	/*PUNTO INTERNO + BORDO SUPERIORE + BORDO INFERIORE*/
	for(unsigned int y = 1; y < NY+1 ; y++){					//salto halo , quindi y=1 fino a NY   (NY+1 halo, NY primo layer)
		for(unsigned int x = 2; x < NX ; x++){					//salto le x di bordo, che sono problematiche e trattate a parte

			Fx[scalar_index(x,y)] = ( pot[scalar_index(x+1,y)] - pot[scalar_index(x-1,y)] ) * 0.5 * rho[scalar_index(x,y)];

		}
	}
	//non ho fatto eccezioni per inlet e outlet perché mi sembra che nel mio caso tutte le popolazioni siano ben definite - controlla


	/*BORDO LATERALE SINISTRO - X=1 */
	unsigned int X = 1; 						//leggibilità
	for(unsigned int y = 1; y < NY+1 ; y++){

		Fx[scalar_index(X,y)] = ( pot[scalar_index(X+1,y)] - pot[scalar_index(X,y)] ) * rho[scalar_index(X,y)];
	}

	/*BORDO LATERALE DESTRO - X=NX */
	X = NX; 									//leggibilità
	for(unsigned int y = 1; y < NY+1 ; y++){

		Fx[scalar_index(X,y)] = ( pot[scalar_index(X,y)] - pot[scalar_index(X-1,y)] ) * rho[scalar_index(X,y)];
	}

}




void compute_Fy(double* pot, double* Fy, double* rho){

	//descrizione in compute_Fx

	/*PUNTO INTERNO + LATERALE SINISTRO + LATERALE DESTRO*/
	for(unsigned int y = 2; y < NY ; y++){						//salto halo e primo layer , quindi y=2 fino a NY-1   (NY+1 halo, NY primo layer)
		for(unsigned int x = 1; x < NX+1 ; x++){				//le x invece vanno tutte bene tanto la derivata convolge vicini verticali

			Fy[scalar_index(x,y)] = ( pot[scalar_index(x,y+1)] - pot[scalar_index(x,y-1)] ) * 0.5 * rho[scalar_index(x,y)];

		}
	}

	//non faccio eccezioni per inlet e outlet perché mi sembra che nel mio caso tutte le popolazioni siano ben definite - controlla :

	/*BORDO INFERIORE - Y=1 */
	unsigned int Y = 1; 						//leggibilità
	for(unsigned int x = 1; x < NX+1 ; x++){

		Fy[scalar_index(x,Y)] = ( pot[scalar_index(x,Y+1)] - pot[scalar_index(x,Y)] ) * rho[scalar_index(x,Y)];
	}

	/*BORDO SUPERIORE - Y=NY */
	Y = NY; 									//leggibilità
	for(unsigned int x = 1; x < NX+1 ; x++){

		Fy[scalar_index(x,Y)] = ( pot[scalar_index(x,Y)] - pot[scalar_index(x,Y-1)] ) * rho[scalar_index(x,Y)];
	}

}




void potential_calc(double* rho, double* pot){        //calcolo del potenziale punto per punto in local capacitance approx

	for(unsigned int y = 1; y < NY+1; y++){
		for(unsigned int x = 1; x < NX+1; x++){

			pot[scalar_index(x,y)] = ( rho[scalar_index(x,y)] - rho_0) * C_g_inv;    	//è la (99) di https://arxiv.org/pdf/1909.04502.pdf
		}																				//non sto facendo distinzioni per in/out <<<<<-------------
																						//non sto mettendo un - e non lo sto mettendo neppure in F \sim -grad(pot)
	}


	//non ho fatto eccezioni per inlet e outlet perché mi sembra che nel mio caso tutte le popolazioni siano ben definite - controlla

}

void compute_Fx_diss(double *rho, double *ux, double *Fx){								//<<<<<<<<<<<<-------- controlla se è una densità di forza

	for(unsigned int y = 1; y < NY+1; y++){
		for(unsigned int x = 1; x < NX+1; x++){

			Fx[scalar_index(x,y)] = - rho[scalar_index(x,y)] * ux[scalar_index(x,y)] / tau_D;

		}
	}

}

void compute_Fy_diss(double *rho, double *uy, double *Fy){                               //<<<<<<<<<<<<-------- controlla se è una densità di forza

	for(unsigned int y = 1; y < NY+1; y++){
		for(unsigned int x = 1; x < NX+1; x++){

			Fy[scalar_index(x,y)] = - rho[scalar_index(x,y)] * uy[scalar_index(x,y)] / tau_D;

		}
	}

}