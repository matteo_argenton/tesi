/*contiene le funzioni che gestiscono l'output su file*/

#ifndef _DUMP_
#define _DUMP_

void init_dump_folder();																										//crea ../dump  , comune a tutte le simulazioni
void dump_settings(const char*);																								//salva un file con i detagli della simulazione
const char *setup_dump_folder(int, int, int);																					//crea la dircotory per il dump dei file della simulazione corrente
void print_results(double*, double*, double*, double*);                 														//funzione di debug
void get_main_data_iter(const char*,double*, double*, double*, double*, double*, double*, double*, double*, double*, int);		//crea i file .dat alla fine dei blocchi STEPS iterazioni
void get_data_final(const char*,double*);																						//esporta la matrice n_ delle densità all'equilibrio per il calcolo del potenziale chimico


#endif