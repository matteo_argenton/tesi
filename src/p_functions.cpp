/*contiene le funzioni di supporto al main: inizializzazione e controllo stato stazionario*/

/*************************************************************************MOLTO IMPORTANTE: *****************************************************************************/
/*                                                                                                                                                                      */
/*le matrici delle popolazioni f sono di dimensione (NX+2)*(NY+2)*ndir, mentre quelle dei campi rho, ux, uy sono (NX+2)*(NY+2), con un loro halo layer (non utilizzato) */
/*            presente per uniformare gli indici. Questo è il motivo per cui i cicli partono da 1 e finiscono a N&+1 escluso, così si saltano i due layer extra         */
/************************************************************************************************************************************************************************/

#include "p_functions.h"
#include <math.h>
#include <stdio.h>



//descrizione più rigorosa delle funzioni nell'header


/*--------------------------------------------------inizializzazione rho u -----------------------------------------------------*/

void electrons_init(unsigned int t, unsigned int x, unsigned int y, double *r, double *u, double *v)
{

	
	double X = x;			//venivano usati per eventuali shift di mezzo punto reticolare
	double Y = y;			

	double ux = 0.0;
	double uy = 0.0;

	double rho = 1.0;		//ogni punto viene inizializzato con densità 1.0

	*r = rho;
	*u = ux;
	*v = uy;

}

void electrons_init(unsigned int t, double *r, double *u, double *v)
{
    for(unsigned int y = 1; y < NY+1; y++)              //+1 per via dell'halo
    {
        for(unsigned int x = 1; x < NX+1; x++)
        {
            size_t sidx = scalar_index(x,y);

            electrons_init(t,x,y,&r[sidx],&u[sidx],&v[sidx]);
        }
    }
}




/*--------------------------------------------------inizializzazione equilibrio -------------------------------------------------------*/


void init_equilibrium(double *f, double *r, double *u, double *v)
{
	for(unsigned int y = 1; y < NY+1; y++)
    {
        for(unsigned int x = 1; x < NX+1; x++)
        {


	            double rho = r[scalar_index(x,y)];
	            double ux  = u[scalar_index(x,y)];
	            double uy  = v[scalar_index(x,y)];

	            for(unsigned int i=0; i<ndir; i++){
	           
	            	double cidotu = dirx[i]*ux + diry[i]*uy;
					      f[field_index(x,y,i)] = wi[i]*rho*(1.0 + 3.0*cidotu+4.5*cidotu*cidotu-1.5*(ux*ux+uy*uy));
            	}	
            

        }
    }
}




/*--------------------------------------------------calcolo funzione equilibrio -------------------------------------------------------*/
//può avere senso pensarla come inline?
double f_eq(int x, int y, int i, double ux, double uy, double *r){

		        double rho = r[scalar_index(x,y)];

	            double cidotu = dirx[i]*ux + diry[i]*uy;

	            return wi[i]*rho*(1.0 + 3.0*cidotu+4.5*cidotu*cidotu-1.5*(ux*ux+uy*uy));

}



/*------------------------------------------funzioni per controllo stato stazionario --------------------------------------------------*/

double avg_rho(double *f){

	double rho = 0.0;

	for(unsigned int y=1; y<NY+1; y++){
		for(unsigned int x=1; x<NX+1; x++){
			for(unsigned int i=0; i<ndir; i++){

				rho += f[field_index(x,y,i)];
			}
		}
	}

	//rho=rho/(NX*NY); //per ora solo massa, non densità
	return rho;

}



void avg_u(double *ux, double *uy, double *u_mod){

	double ux_tot = 0.0;
	double uy_tot = 0.0;

	for(unsigned int y=1; y<NY+1; y++){
		for(unsigned int x=1; x<NX+1; x++){

				u_mod[scalar_index(x,y)] = sqrt( ux[scalar_index(x,y)]*ux[scalar_index(x,y)] + uy[scalar_index(x,y)]*uy[scalar_index(x,y)] );
		}
	}

}



double avg_u_diff(double *u_new, double *u_old ){

	double avg_diff = 0.0;

	for(unsigned int y=1; y<NY+1; y++){
		for(unsigned int x=1; x<NX+1; x++){

			avg_diff += u_new[scalar_index(x,y)] - u_old[scalar_index(x,y)];

		}
	}

	return avg_diff/(NX*NY);

}


void knudsen_check(){

	double c_s = sqrt(1./3.);
	double kn = nu/(c_s * NX);

	if(kn <= 0.005)
		printf("Il numero di Knudsen per la simulazione corrente è %lf, proseguo. (accettabili <= 0.005 - 0.001)\n", kn);
	else{
		fprintf(stderr, "Numero di knudsen troppo grande (%lf)", kn);
		exit(4);
	}
}