#include "lbm.h"
#include "p_functions.h"
#include "macro.h"
#include <stdio.h>


void stream(double *f_src, double *f_dst, double *r){

	unsigned int X;
	unsigned int Y;

	for(unsigned int y = 1; y < NY+1; y++){
		for(unsigned int x = 1; x < NX+1; x++){
			for(unsigned int i = 0; i < ndir; i++){

				X = x+dirx[i];   							   
				Y = y+diry[i];		

				f_dst[field_index(X,Y,i)]=f_src[field_index(x,y,i)];       //ogni (x,y) propaa le proprie popolazioni in (X,Y), alcune finiscono nell'halo e alcune di f_dst non sono popolate perché devono arrivare dalle boundary
				
			}
		}
	}

	//nota che le popolazioni 0 di f_dst vengono già tutte assegnate



	/*-------------------------------------------------------------------------------------------*/
	/*----------------------------------bordi superiore e inferiore------------------------------*/
	/*-------------------------------------------------------------------------------------------*/



	//Approccio Bounce Back mid grid. Il muro è tra halo layer e primo nodo del reticolo. Non uso l'algoritmo di lista_progetti.pdf
	//perché quello è un full-way BB. Quindi non assegno 1=5 perché escono dalla propagazione iniziale e le varie f_i non sono uguali ed
	//opposte alle f_{opp[i]}. Sono nodi di fludo e si muovono. Il no-slip è garantito dal modo in cui si specchia
	//l'equazione per il bordo inferiore è: f_dst(x,y,i) = f_src(x,y,opp[i]) per i=2,3,4  ; 1,5,6,7,8 escono dalla propagazione; 0 è inizializzato


	//bordo superiore - dove non ci sono inlet e outlet quindi ogni nodo è da trattare
	for(unsigned int x=1; x<NX+1; x++){

		for(unsigned int i = 2; i < 5; i++){
			f_dst[field_index(x,NY,opp[i])]=f_src[field_index(x,NY,i)];      //è il rimbalzo delle vecchie popolazioni
		}																	 //avrei potuto usare l'halo (è stato popolato prima) scrivendo
																			 //f_dst[field_index(x,NY,opp[i])]=f_dst[field_index(x,NY+1,i)]; 
	}

	//bordo inferiore - contiene inlet e outlet e non devo trattare ora quei nodi perché sono moving wall
	for(unsigned int x=1; x<NX+1; x++){

		if(   (!(x> INLET_START  && x<( INLET_START+ INLET_DIM))) &&
		 	  (!(x> OUTLET_START && x<(OUTLET_START+OUTLET_DIM)))
		  ){

			for(unsigned int i = 2; i < 5; i++){
				f_dst[field_index(x,1,i)]=f_src[field_index(x,1,opp[i])];	//stesse considerazioni di qui sopra, avrei potuto usare l'halo in modo analogo
			}
	
		}
	}






	/*---------------------------------------------------------------------------------------------*/
	/*--------------------------------------laterali-----------------------------------------------*/
	/*---------------------------------------------------------------------------------------------*/


	for(unsigned int y=2; y<NY; y++){     //parto da y=2 perché non tratto né lo 0 che è halo, né 1 che è il bordo inferiore (angoli)


	//nuovo codice:
	//assegno le 3 popolazioni laterali, il resto viene già propagato prima


		//bordo sinistro
		f_dst[field_index(1,y,1)]=f_src[field_index(1,y,opp[1])];
		f_dst[field_index(1,y,2)]=f_src[field_index(1,y,opp[2])];
		f_dst[field_index(1,y,8)]=f_src[field_index(1,y,opp[8])];


		//bordo destro
		f_dst[field_index(NX,y,opp[1])]=f_src[field_index(NX,y,1)];
		f_dst[field_index(NX,y,opp[2])]=f_src[field_index(NX,y,2)];
		f_dst[field_index(NX,y,opp[8])]=f_src[field_index(NX,y,8)];

	}



	/*-------------------------------------------------------------------------------------------*/
	/*--------------------------------------angoli-----------------------------------------------*/
	/*-------------------------------------------------------------------------------------------*/


	/*--------basso SX----------*/
	for(unsigned int i=1; i<=4; i++){
		f_dst[field_index(1,1,i)]=f_src[field_index(1,1,opp[i])];        //mi serve soprattutto assegnare le popolazioni 1,2,3; 4 è indifferente tanto viene posta uguale a 8 e si elidono (sono quelle sulla diagonale principale)
	}
	f_dst[field_index(1,1,opp[4])]=f_dst[field_index(1,1,4)];			 



	/*--------basso DX----------*/
	for(unsigned int i=2; i<6; i++){
		f_dst[field_index(NX,1,i)]=f_src[field_index(NX,1,opp[i])];
	}
	f_dst[field_index(NX,1,opp[2])]=f_dst[field_index(NX,1,2)];


	/*--------alto SX----------*/
	for(unsigned int i=2; i<6; i++){
		f_dst[field_index(1,NY,opp[i])]=f_src[field_index(1,NY,i)];
	}
	f_dst[field_index(1,NY,2)]=f_dst[field_index(1,NY,opp[2])];


	/*--------alto DX----------*/
	for(unsigned int i=1; i<=4; i++){
		f_dst[field_index(NX,NY,opp[i])]=f_src[field_index(NX,NY,i)];
	}
	f_dst[field_index(NX,NY,4)]=f_dst[field_index(NX,NY,opp[4])];





	/*---------------------------------------------------------------------------------------------------*/
	/*--------------------------------------inlet e outlet-----------------------------------------------*/
	/*---------------------------------------------------------------------------------------------------*/

	//modifico le popolazioni 2,3,4 di f_dst (quelle entranti) affinché siano le vecchie f_src rimbalzate + contributo di moving wall normale alla direzione del muro1
	const int y = 1;    //y non verrà più usata e aiuta la leggibilità del codice in/out. Mi posiziono sulla riga di in/out, che è y=1 perché 0 è il livello halo

	/*inlet*/
	for(unsigned int x=INLET_START; x<(INLET_START+INLET_DIM); x++){


		#ifdef FEQ  														   //se si vuole usare la funzione di equilibrio come boundary

			for(int i=1; i<=4; i++){
				f_dst[field_index(x,y,i)] = f_eq(x , y+1, i, ux_in, uy_in, r);  //richiamo la funzione di eq. con la velocità dell'inlet e Y=y+1=2 per prendere la densità
			}																    //dalla cella superiore (in y=1 siamo nell'inlet e la densità non è ben definita)

		#else

			/*versione 1.0: prendo la densità dalla cella superiore, dove è definita correttamente*/
			for(int i=1; i<=4; i++){
				f_dst[field_index(x,y,i)] = f_src[field_index(x,y,opp[i])] -2*wi[opp[i]]*r[scalar_index(x,y+1)]*(dirx[opp[i]]*ux_in + diry[opp[i]]*uy_in)*3.0;
			}

		#endif
	}


	/*outlet*/
	for(unsigned int x=OUTLET_START; x<(OUTLET_START+OUTLET_DIM); x++){

		#ifdef FEQ  														   //se si vuole usare la funzione di equilibrio come boundary

			for(int i=1; i<=4; i++){
				f_dst[field_index(x,y,i)] = f_eq(x , y+1, i, ux_out, uy_out, r);  //richiamo la funzione di eq. con la velocità dell'outlet e y=2, vedi sopra
			}																    

		#else		

			/*versione 1.0: prendo la densità dalla cella superiore, dove è definita correttamente*/
			for(int i=1; i<=4; i++){
				
				f_dst[field_index(x,y,i)] = f_src[field_index(x,y,opp[i])] -2*wi[opp[i]]*r[scalar_index(x,y+1)]*(dirx[opp[i]]*ux_out + diry[opp[i]]*uy_out)*3.0;
			}

		#endif

	}
	
}




/*
void compute_rho_u(double *f, double *r, double *u, double *v){
	for(unsigned int y = 1; y < NY+1; y++){
		for(unsigned int x = 1; x < NX+1; x++){
			double rho = 0.0;
			double ux = 0.0;
			double uy = 0.0;

			for(unsigned int i = 0; i < ndir; ++i){
				rho += f[field_index(x,y,i)];
				ux  += dirx[i]*f[field_index(x,y,i)];
				uy  += diry[i]*f[field_index(x,y,i)];
			}
				
				r[scalar_index(x,y)] = rho;

				u[scalar_index(x,y)] = ux/rho;
				v[scalar_index(x,y)] = uy/rho;
			}

		}
	}
}

*/


//funzione incompleta perché non più utilizzata
void compute_rho_u(double *f, double *r, double *u, double *v){   //questa versione del codice calcola rho e ux, uy nella stessa funzione
																  //con la forzante creo funzioni separate perché mi serve prima rho -> pot -> correzione a ux, uy.
																  //Come si vede è comunque da rimaneggiare perché stava venendo adattata a F, ma era da finire

	double rho;
	double ux;
	double uy;

	for(unsigned int y = 1; y < NY+1; y++){						  
		for(unsigned int x = 1; x < NX+1; x++){

			rho = 0.0;
			ux  = 0.0;
			uy  = 0.0;

			for(unsigned int i = 0; i < ndir; ++i){
				rho += f[field_index(x,y,i)];
				ux  += dirx[i]*f[field_index(x,y,i)];
				uy  += diry[i]*f[field_index(x,y,i)];
			}
				
				r[scalar_index(x,y)] = rho;


/*--------------------------------------------------vecchio codice senza forzante------------------------------------------------*/
#if !defined(DISSIPATIVE_F) && !defined(SELF_INDUCED_F)
	/*questo controllo non ha senso: le cose già devono funzionare con le boundary giuste. Sul bordo non deve esserci velocità tangenziale*/
	/*nota però che, con le mie condizioni, il bordo non è un punto del reticolo, ma qualcosa di intermedio tra halo e primo layer, quindi è accettabile che ci sia componente normale */


	//			if((y!=1)&&(y!=NY)&&(x!=1)&&(x!=NX)){

					u[scalar_index(x,y)] = ux/rho;
					v[scalar_index(x,y)] = uy/rho;
	//			}
/*--------------------------------------------------------------------------------------------------------------------------------*/

#elif defined(DISSIPATIVE_F) && !defined(SELF_INDUCED_F)     //solo il secondo termine della (98)

/*------------------------------------------------------nuovo codice con forzante-------------------------------------------------*/

					u[scalar_index(x,y)] = ux * ( 1.0/rho - tau/tau_D ); 	//questa formula esce da una semplice sostituzione della F in (98) con
					v[scalar_index(x,y)] = uy * ( 1.0/rho - tau/tau_D ); 	//la formula per la forzante nelle note di sistemi complessi

/*--------------------------------------------------------------------------------------------------------------------------------*/

#elif !defined(DISSIPATIVE_F) && defined(SELF_INDUCED_F)	//solo il primo termine

					//u[scalar_index(x,y)] = ux * ( 1.0/rho - tau/tau_D ); 	
					//v[scalar_index(x,y)] = uy * ( 1.0/rho - tau/tau_D ); 	

#else //ambedue i contributi

					//TODO

#endif

		}
	}
}


//vecchia collide pre-forcing
/*
void collide(double *f, double *r, double *u, double *v){
	// costanti per non calcolarle nei cicli
	const double tauinv = 2.0/(6.0*nu+1.0); // 1/tau
	const double omtauinv = 1.0-tauinv; // 1 - 1/tau

	for(unsigned int y = 1; y < NY+1; y++){
		for(unsigned int x = 1; x < NX+1; x++){

			double rho = r[scalar_index(x,y)];
			double ux = u[scalar_index(x,y)];
			double uy = v[scalar_index(x,y)];

			for(unsigned int i = 0; i < ndir; i++){

				double cidotu = dirx[i]*ux + diry[i]*uy;
				// compute equilibrium
				double feq = wi[i]*rho*(1.0 + 3.0*cidotu+4.5*cidotu*cidotu-1.5*(ux*ux+uy*uy));
				// relax to equilibrium
				f[field_index(x,y,i)] = omtauinv*f[field_index(x,y,i)] + tauinv*feq;

			}

		}
	}
}
*/


void collide(double *f, double *r, double *u, double *v, double *Fx, double *Fy, double *Fx_diss, double *Fy_diss){

	// costanti per non calcolarle nei cicli
	const double tauinv   		  = 2.0 / (6.0*nu+1.0); 	 				// 1/tau
	const double omtauinv 		  = 1.0 - tauinv; 			 				// 1 - 1/tau
	const double source_prefactor = 1.0 - ( 0.5 * tauinv );  				// 1 - 1/(2*tau)

	double cidotu;
	double feq;
	double sourc_i_1, sourc_i_2, sourc_i_3;    				//ho diviso i termini di una formula lunghissima, anche se il prezzo è avere 3 variabili extra
	double sourc_diss_i_1, sourc_diss_i_2, sourc_diss_i_3;	//stessa cosa, ma per il termine dissipativo. Separati per poterli attivare selettivamente, non ottimale però

	double sourc_i      = 0.0;								//inizializzati a parte per prevedere il caso in cui i termini di forzante siano spenti
	double sourc_diss_i = 0.0;

	double rho;
	double ux;
	double uy;


	for(unsigned int y = 1; y < NY+1; y++){
		for(unsigned int x = 1; x < NX+1; x++){


			rho = r[scalar_index(x,y)];

			if( y==1 )										//<<<<<<<<<<<<------- questo è un tentativo rapido per capire se il fastidio sta nella densità dei punti del bordo inferiore
				rho = r[scalar_index(x,y+1)];


			ux  = u[scalar_index(x,y)];
			uy  = v[scalar_index(x,y)];

			for(unsigned int i = 0; i < ndir; i++){

				cidotu = dirx[i]*ux + diry[i]*uy;

				// compute equilibrium
				feq = wi[i]*rho*(1.0 + 3.0*cidotu+4.5*cidotu*cidotu-1.5*(ux*ux+uy*uy));


				#ifdef F_INDUCED
					//compute source term for the self-induced force
					sourc_i_1 = ( dirx[i] * Fx[scalar_index(x,y)] + diry[i] * Fy[scalar_index(x,y)] ) * 3.0;
					sourc_i_2 = ( dirx[i] * Fx[scalar_index(x,y)] + diry[i] * Fy[scalar_index(x,y)] ) * ( dirx[i] * u[scalar_index(x,y)] + diry[i] * v[scalar_index(x,y)] ) * 9.0;
					sourc_i_3 = ( u[scalar_index(x,y)] * Fx[scalar_index(x,y)] + v[scalar_index(x,y)] * Fy[scalar_index(x,y)] ) * 3.0;

					sourc_i = source_prefactor * wi[i] * ( sourc_i_1 + sourc_i_2 - sourc_i_3 );
				#endif


				#ifdef F_DISS
					//compute source term for the dissipative force
					sourc_diss_i_1 = ( dirx[i] * Fx_diss[scalar_index(x,y)] + diry[i] * Fy_diss[scalar_index(x,y)] ) * 3.0;
					sourc_diss_i_2 = ( dirx[i] * Fx_diss[scalar_index(x,y)] + diry[i] * Fy_diss[scalar_index(x,y)] ) * ( dirx[i] * u[scalar_index(x,y)] + diry[i] * v[scalar_index(x,y)] ) * 9.0;
					sourc_diss_i_3 = ( u[scalar_index(x,y)] * Fx_diss[scalar_index(x,y)] + v[scalar_index(x,y)] * Fy_diss[scalar_index(x,y)] ) * 3.0;

					sourc_diss_i = source_prefactor * wi[i] * ( sourc_diss_i_1 + sourc_diss_i_2 - sourc_diss_i_3 );
				#endif

				// relax to equilibrium - now with source term - forcing
				f[field_index(x,y,i)] = omtauinv*f[field_index(x,y,i)] + tauinv*feq + sourc_i + sourc_diss_i;  //sourc_* sono i termini di forzante

			}

		}
	}
}



void compute_rho(double *f, double *r){

	double rho;

	for(unsigned int y = 1; y < NY+1; y++){						  
		for(unsigned int x = 1; x < NX+1; x++){

			rho = 0.0;

			for(unsigned int i = 0; i < ndir; i++){
				rho += f[field_index(x,y,i)];
			}
				
				r[scalar_index(x,y)] = rho;
		}
	}

}


//versione originale e affatto ottimizzata
/*
	void compute_u(double *f, double *u, double *v, double *rho, double *Fx, double *Fy, double *Fx_diss, double *Fy_diss){

		double ux;
		double uy;

		for(unsigned int y = 1; y < NY+1; y++){						  
			for(unsigned int x = 1; x < NX+1; x++){	

			ux = 0.0;
			uy = 0.0;

				for(unsigned int i = 0; i < ndir; ++i){

					ux  += dirx[i]*f[field_index(x,y,i)];
					uy  += diry[i]*f[field_index(x,y,i)];
				}

			u[scalar_index(x,y)] = ux/rho[scalar_index(x,y)];				//questi sono i contributi standard. A cui va sommato quello della forzante              
			v[scalar_index(x,y)] = uy/rho[scalar_index(x,y)];				//questi sono i contributi standard. A cui va sommato quello della forzante

			}
		}


		#ifdef F_INDUCED

			for(unsigned int y = 1; y < NY+1; y++){						  
				for(unsigned int x = 1; x < NX+1; x++){

					u[scalar_index(x,y)] += Fx[scalar_index(x,y)] / (2.0 * rho[scalar_index(x,y)] );
					v[scalar_index(x,y)] += Fy[scalar_index(x,y)] / (2.0 * rho[scalar_index(x,y)] );

				}
			}
		#endif



		#ifdef F_DISS

			for(unsigned int y = 1; y < NY+1; y++){						  
				for(unsigned int x = 1; x < NX+1; x++){

					u[scalar_index(x,y)] += Fx_diss[scalar_index(x,y)] / (2.0 * rho[scalar_index(x,y)] );
					v[scalar_index(x,y)] += Fy_diss[scalar_index(x,y)] / (2.0 * rho[scalar_index(x,y)] );

				}
			}
		#endif

	}
*/



void compute_u(double *f, double *u, double *v, double *rho, double *Fx, double *Fy, double *Fx_diss, double *Fy_diss){

	double ux;
	double uy;

	for(unsigned int y = 1; y < NY+1; y++){						  
		for(unsigned int x = 1; x < NX+1; x++){	

			ux = 0.0;
			uy = 0.0;

			//if( (y!=1) && (y!=NY) && (x!=1) && (x!=NX) ){    //tentativo con velocità dai bordi forzate a 0
															   //sembra faccia tornare un po' i dati, ma non ho ancora capito il perché

						for(unsigned int i = 0; i < ndir; ++i){

							ux  += dirx[i]*f[field_index(x,y,i)];
							uy  += diry[i]*f[field_index(x,y,i)];
						}

						u[scalar_index(x,y)] = ux/rho[scalar_index(x,y)];				//questi sono i contributi standard. A cui va sommato quello della forzante              
						v[scalar_index(x,y)] = uy/rho[scalar_index(x,y)];				//questi sono i contributi standard. A cui va sommato quello della forzante

					#ifdef F_INDUCED    /*PARTE DI CODICE PER LA FORZANTE AUTOINDOTTA */

						u[scalar_index(x,y)] += Fx[scalar_index(x,y)] / (2.0 * rho[scalar_index(x,y)] );
						v[scalar_index(x,y)] += Fy[scalar_index(x,y)] / (2.0 * rho[scalar_index(x,y)] );

					#endif

					#ifdef F_DISS 	    /*PARTE DI CODICE PER LA FORZANTE LEGATA ALLE IMPURITA'*/

						u[scalar_index(x,y)] += Fx_diss[scalar_index(x,y)] / (2.0 * rho[scalar_index(x,y)] );
						v[scalar_index(x,y)] += Fy_diss[scalar_index(x,y)] / (2.0 * rho[scalar_index(x,y)] );


					#endif
			//}//fine if
		}
	}

}