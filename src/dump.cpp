#include "dump.h"
#include "p_functions.h"
#include <stdio.h>
#include <sys/stat.h>
#include <errno.h>
#include "macro.h"




void dump_settings(const char* filepath){

	FILE* fp;

	char fname[200];

		sprintf(fname, "%s//settings.txt", filepath);

		fp=fopen(fname, "w");

			#ifdef FEQ
				fprintf(fp, "FEQ abilitata nelle boundaries\n");
			#else
				fprintf(fp, "FEQ non abilitata nelle boundaries\n");
			#endif

			#ifdef F_DISS
				fprintf(fp, "Forzante dissipativa abilitata nelle boundaries\n");
			#else
				fprintf(fp, "Forzante dissipativa non abilitata nelle boundaries\n");
			#endif

			#ifdef F_INDUCED
				fprintf(fp, "Forzante autoindotta abilitata nelle boundaries\n");
			#else
				fprintf(fp, "Forzante autoindotta non abilitata nelle boundaries\n");
			#endif


				fprintf(fp, "L=%d\n W=%d\n tau=%lf\n nu=%lf\n soglia massa=%.20lf \
							\n soglia velocità=%.20lf\n inizio inlet=%u\n inizio outlet=%d\n ampiezza in/out=%u\n numero di step per iterazione=%u \
							\n velocità in/out=%lf\n C_g_inv=%lf\n", NX, NY, tau, nu, THRESHOLD_RHO, THRESHOLD_U, INLET_START, OUTLET_START, INLET_DIM, STEPS, uy_in, C_g_inv);


		fclose(fp);

}



void init_dump_folder(){
	
	const int dir_err = mkdir("../dump", 0777);
		if (dir_err==-1){
			if(errno == EEXIST){
    			fprintf(stderr,"../dump already existing, continuing...\n");
   	 		}
   	 		else{
   	 			perror("Can't create the new directory - error:");
   	 			exit(2);
   	 		}
		}

}


const char *setup_dump_folder(int nx, int ny, int dim){

	static char dump_folder_name[200];

	sprintf(dump_folder_name, "../dump/%dx%dx%d_inout_normali_29_12_debugpicchiIFlower", nx, ny, dim); 

	const int dir_err = mkdir(dump_folder_name, 0777);
		if (dir_err==-1){
			if(errno == EEXIST){
    			fprintf(stderr,"%s already existing, can't continue\n", dump_folder_name);
   	 			exit(1);
   	 		}
   	 		else{
   	 			perror("Can't create the new directory, error:\n");
   	 			exit(3);
   	 		}
		}
	return dump_folder_name;

}

void print_results(double *f, double *r, double *u, double *v){      //funzione di stampa a schermo degli array con densità e velocità

	for(unsigned int y = 1; y < NY+1; y++){

		for(unsigned int x = 1; x < NX+1; x++){

			double rho = r[scalar_index(x,y)];
			double ux = u[scalar_index(x,y)];
			double uy = v[scalar_index(x,y)];

			printf("\nUx=%.10lf,Uy=%.10lf\t", ux,uy);
		}

		printf("\n");
	}
}




void get_main_data_iter(const char* filepath, double *f, double *r, double *u, double *v, double *phi, double* Fx, double* Fy, double* Fx_diss, double* Fy_diss, int iter){ 

	double Fx_tot = 0.0;
	double Fy_tot = 0.0;

	FILE* fp;

	char fname[200];

	sprintf(fname, "%s//result%d.dat", filepath, iter);

	fp=fopen(fname, "w");

		for(unsigned int y = 1; y < NY+1; y++){

			for(unsigned int x = 1; x < NX+1; x++){

				Fx_tot = Fx[scalar_index(x,y)] + Fx_diss[scalar_index(x,y)];
				Fy_tot = Fy[scalar_index(x,y)] + Fy_diss[scalar_index(x,y)];

				fprintf(fp, "%u %u %.20lf %.20lf %.20lf %.20lf %.20lf %.20lf\n",y,x,r[scalar_index(x,y)],u[scalar_index(x,y)],v[scalar_index(x,y)], phi[scalar_index(x,y)], Fx_tot, Fy_tot);  //il formato è tempo, y,x, etc...

			}
		}
	fclose(fp);
}




void get_data_final(const char* filepath, double *n_){ 

	FILE* fp;

	char fname[200];

	sprintf(fname, "%s//resultsNdef.dat", filepath);

	fp=fopen(fname, "w");

		for(unsigned int y = 1; y < NY+1; y++){

			for(unsigned int x = 1; x < NX+1; x++){

				fprintf(fp, "%u %u %.20lf\n" ,y,x,n_[scalar_index(x,y)]);

			}
		}
	fclose(fp);
}