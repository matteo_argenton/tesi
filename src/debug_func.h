#ifndef _DEBF_
#define _DEBF_

void print_population_point(const char*, double*, int, int);           			//stampa a file la popolazione di uno specifico punto del reticolo
void print_population_set(const char* , double*, int, int, int, int);  			//stampa a file la popolazione di un insieme (connesso) di punti
void print_population_borders(const char* , double*);							//stampa a file la popolazione di bordo del reticolo
void print_velocity_borders(const char*, double*, double*);						//stampa a file le velocità dei punti di bordo
void get_scalar_data(const char*, double*, const char*);						//stampa a file il valore punto per punto di un campo scalare

#endif