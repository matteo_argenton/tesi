\documentclass[]{report}
\usepackage{microtype}
\usepackage{hyperref}
\usepackage{float}
\usepackage[utf8x]{inputenc}
\usepackage{graphicx}
\usepackage{xcolor}
\usepackage{amsmath,amssymb}

\title{Report tesi}
\author{Matteo Argenton}

\begin{document}
	\maketitle

	\begin{abstract}
		Questo lavoro ha come scopo la replicazione di una simulazione di un fluido che rappresenti il moto in regime idrodinamico di elettroni nel grafene, usando algoritmi Lattice Boltzmann. In questo report, per ora, si presenterà un primo stadio per il calcolo del potenziale elettrochimico misurato vicino a inlet e outlet nella "vicinity geometry".
		
		\textbf{ Rispetto alla versione precedente sono stati riscritte le sezioni da 1.4.1 ed è stata aggiunta una sezione sulla forzante. I commenti di questa versione sono in rosso; in blu i vecchi commenti ancora in un qualche modo rilevanti, o solo inclusi come promemoria.}
		
		\textbf{Eventualmente, se venisse comodo vedere le differenze tra versioni del report, ho caricato tutto su overleaf e aggiunto nella history una label per queste -attualmente- due versioni:}
		
		\url{https://www.overleaf.com/6328835965pbbpcmxmhkdg}


	\end{abstract}


	
	\chapter{Simulazione numerica del potenziale elettrochimico}
	\section{Geometria e regime idrodinamico}
	
	Con l'obiettivo di ottenere una simulazione lattice boltzmann che riproduca le predizioni analitiche per il potenziale elettrochimico in un singolo foglio di grafene (sperimentalmente circondato da materiale dielettrico) ottenute in origine in \cite{PhysRevB.92.165433}, e facendo riferimento ai risultati in \cite{Gabbana_2020}, si è immaginato un reticolo 500x200 (per ora le varie unità di misura presentate sono da riferirsi alla simulazione e non a unità fisiche) con un inlet e un outlet posizionati rispettivamente a 1/3 e 2/3 del bordo inferiore.
	
	Su questo reticolo è stato implementato un tipico algoritmo LBM, dove,  per prima cosa viene controllato che il numero di Knudsen
	
	\begin{equation}
		K_n = \frac{\nu}{c_s L} \le 0.005
	\end{equation}
	
	sia sufficientemente piccolo; dove $\nu$ è la viscosità cinematica del fluido di elettroni, $c_s$ la velocità del suono, e L è la dimensione in x del reticolo. $K_n$ piccolo permette di entrare nel limite idrodinamico della teoria di Boltzmann, recuperando le equazioni di Navier-Stokes \cite{Gabbana_2018}.
	
	
	\section{Condizioni al contorno}
	
	Con riferimento a \cite{Torre_2015} e \cite{Bandurin_2016}  si è deciso di applicare ad ogni punto di bordo del reticolo, escludendo inlet e outlet, delle condizioni al contorno di \emph{no-slip}, identiche a quelle che verrebbero usate per un liquido classico.
	
	\begin{figure}
		\centering
		\includegraphics[width=0.2\linewidth]{fig/stencil}
		\caption{Stencil D2Q9 (si noti la scelta di numerazione delle velocità microscopiche, diversa da altre convenzioni, come quella in \cite{book})}
		\label{fig:stencil}
	\end{figure}
	
	
	Facendo riferimento ad uno stencil di velocità organizzato come in fig.\ref{fig:stencil}, a livello algoritmico la scelta è stata quindi quella di imporre sul bordo inferiore e a tempo di stream:
	
	\begin{eqnarray}
		\label{BC}
		f_2(x,y,t+\Delta t) = f_6(x,y,t) \nonumber \\
		f_3(x,y,t+\Delta t) = f_7(x,y,t) 		   \\
		f_4(x,y,t+\Delta t) = f_8(x,y,t) \nonumber
	\end{eqnarray}
	
	dove con $f_i(x,y,t)$ si è indicata l'i-ma popolazione nel punto (x,y) al tempo t. $f_1, f_5, f_6, f_7, f_8$ al tempo $t+\Delta t$ sono invece semplicemente ottenute dalle vecchie popolazioni dei punti adiacenti tramite un passo di stream. Le varie condizioni \ref{BC} sono consistenti con un algoritmo bounce back mid-grid \cite{Aranega} (o half-way in \cite{book}). Non si sono poste le popolazioni opposte uguali tra loro perché in questo caso i nodi di bordo sono fluidi quindi hanno una propria velocità diversa da zero. Questo è anche il motivo per cui non si è posto $f_1(x,y,t+\Delta t)=f_5(x,y,t+\Delta t)$. Sono stati trattati in modo analogo il bordo superiore e i due bordi laterali. 
	
	Inlet e outlet sono invece stati pensati secondo l'approccio moving wall, quindi un bounce back con un contributo ulteriore per simulare l'immissione e estrazione di fluido, secondo la relazione
	
	\begin{equation}
		\label{movingW}
		f_{i}\left(x,y, t+\Delta t\right)=f_{i}^{*}\left(x,y, t\right)-2 w_{i} \rho_{\mathrm{w}} \frac{c_{i} \cdot u_{\mathrm{w}}}{c_{\mathrm{s}}^{2}}
	\end{equation}
	per $i=2,3,4$, le popolazioni entranti, dove $u_{\mathrm{w}}$ è la velocità del muro, che nel nostro caso è stata fissata a $v_{in}=10^{-5}$ per l'inlet e $v_{out}=-10^{-5}$ per l'outlet; $c_i$ sono le velocità discrete. In questa prima versione della simulazione si è scelto di prendere per $\rho_{\mathrm{w}}$ la densità calcolata non nel punto di inlet/outlet, ma quella della cella appena superiore.
	
	Da ultimo si precisa che per simulare inlet e outlet si è spesso scelto di utilizzare 6 punti reticolari come ampiezza A; questo almeno per $L\ge500$.
	
	\section{Flusso}
	
	\begin{figure}
		\centering
		\includegraphics[width=1.1\linewidth]{fig/colormap_u}
		\caption{Grafico delle linee di flusso associate alla velocità dei punti del reticolo, all'equilibrio idrodinamico, con soglia sulla differenza tra la media delle velocità puntuali tra due set di iterazioni (1000 passi l'uno) di $10^{-10}$}
		\label{fig:colormapu}
	\end{figure}



	
	
	Dalle simulazioni ottenute con i parametri citati nei paragrafi precedenti, e fissando $\tau=1.5$ come tempo di rilassamento, si è ottenuta fig.\ref{fig:colormapu} che mostra la mappa delle velocità del fluido nel reticolo, e da queste le linee di flusso. Dalla figura si nota che, all'equilibrio idrodinamico, appaiono vortici ai lati esterni di inlet e outlet, in accordo con le aspettative. Si nota inoltre che, vicino al punto di inlet, si evidenziano due zone dove la velocità del fluido è minore; una situazione analoga, ma opposta vale invece per l'outlet. 
	Le equazioni idrodinamiche su cui si basa la teoria che si sta indagando sono	
		\begin{equation}
			\nabla \cdot \boldsymbol{J}(\boldsymbol{r})=0
		\end{equation}	
		e
		\begin{equation}
			\label{eqdin2}
			\frac{\sigma_{0}}{e} \nabla \phi(\boldsymbol{r})+D_{v}^{2} \nabla^{2}\boldsymbol{J}(\boldsymbol{r})-\boldsymbol{J}(\boldsymbol{r})=0
		\end{equation}
		che ci si è riproposti di approfondire studiando l'espansione di Chapman-Enskog (che verrà messo in appendice).
	
	
	\section{Verso il potenziale elettrochimico}
	
	Con l'intento di riprodurre fig.17 di \cite{Gabbana_2020} si sono estratti dalla simulazione i valori di densità $n(x,y)$ per i vari punti del reticolo.
	La relazione analitica per il potenziale elettrochimico vicino a inlet e outlet a cui si fa riferimento è
	
	\begin{equation}
		\label{Phi_ec}
		\Phi(x, y)=\frac{m v I}{\bar{n} e^{2} W^{2}}\left[F_{2}\left(x-x_{0}, y\right)-F_{2}\left(x+x_{0}, y\right)\right]
	\end{equation}
	
	con 
	
	\begin{equation}
		\label{F2}
		F_{2}(x, y)=\pi \frac{1 - \cosh (\pi x / W) \cos (\pi y / W)}{[\cosh (\pi x / W) - \cos (\pi y / W)]^{2}}
	\end{equation}
	
	Dove il potenziale elettrochimico è il potenziale elettrico 2D \cite{Torre_2015} corretto per le variazioni di pressione vicino a inlet e outlet come mostrato in \cite{Gabbana_2018}
	\begin{equation}
		\label{eqpot}
		\phi=\varphi(\boldsymbol{r})-\frac{\delta P(\boldsymbol{r})}{e \bar{n}}
	\end{equation}
	
	In un primo momento ci si è limitati ad analizzare il secondo termine a secondo membro della \ref{eqpot}, escludendo quindi l'utilizzo di forzanti (maggiori dettagli nei prossimi paragrafi). Dalla simulazione numerica si sono estratti i valori di densità all'equilibrio idrodinamico (sempre con la già citata soglia di differenze di velocità di $10^{-10}$) e si è fissata la densità all'equilibrio $\overline{n}=1.0$.
	
	
	
	
	\begin{figure}
		\centering
		\includegraphics[width=1\linewidth]{fig/fit}
		\caption{\textcolor{red}{immagine del fit forzato. Lasciata come promemoria, ma problema discusso nel paragrafo 1.4.1} Secondo termine del potenziale elettrochimico $\Phi$ (eq. \ref{eqpot}). Le curve tratteggiate rappresentano la soluzione analitica fornita dalle eqq. \ref{Phi_ec}, \ref{F2}, mentre i punti sono i valori numerici estratti dalla simulazione con L=500, W=200, soglia di differenza velocità tra gruppi di step successivi per l'equilibrio idrodinamico $10^{-10}$, $K_n=0.0011$, $\tau=1.5$, $v_{in}=-v_{out}=10^{-5}$. Ogni curva è da riferirsi ai valori estratti per un certo valore di y vicino a inlet/outlet}
		\label{fig:fit}
	\end{figure}
	
	
	Si mostrano in fig.\ref{fig:fit} i risultati per valori di $y=7,10,13$ e un loro confronto con la soluzione analitica.
	
	\textcolor{red}{Edit: Lascio queste considerazioni solo per ricordare da dove escono le figure. Le tolgo appena risolvo il problema del fit} \textcolor{blue}{Questa è per ora forse la parte più problematica: fig.\ref{fig:fit} è ottenuta non dallo script "plot\_chem.py", ma da "fit\_chem.py" . Il primo è quello che hai riadattato tu, dove il coefficiente che fattorizza $\left[ F_{2_{-}} -F_{2_{+}}\right]$ nella \ref{Phi_ec} è
		\begin{equation}
			\label{prefattore}
			\frac{\nu v_{in}A}{\overline{n}W^2}
		\end{equation}
		dove A è l'ampiezza di inlet e outlet. Il fit di fig.\ref{fig:fit} non è particolarmente utile, ed è stato solo un tentativo preliminare dove si è ricercato un prefattore k unico per le tre curve, tramite un fit. In realtà, per ora questo prefattore viene calcolato in un modo non molto convincente: faccio 3 fit diversi per le 3 curve e calcolo una semplice media dei prefattori $k_i$ così ottenuti. 
	}
	Un tentativo di reale paragone tra i risultati numerici e la soluzione analitica con prefattore \ref{prefattore} è proposto in fig.\ref{fig:500x200confronto}, con dettaglio in fig.\ref{fig:500x200confrontodettaglio} . Si nota una forte differenza tra le due, non correggibile con modifiche $\pm 2$ all'ampiezza di inlet e outlet in \ref{prefattore}.
	
	\begin{figure}
		\centering
		\includegraphics[width=1\linewidth]{fig/500x200confronto}
		\caption{Secondo termine del potenziale elettrochimico $\Phi$ (eq. \ref{eqpot}). Le curve tratteggiate rappresentano la soluzione analitica fornita dalle eqq. \ref{Phi_ec}, \ref{F2}, mentre i punti sono i valori numerici estratti dalla simulazione con L=500, W=200, soglia di differenza velocità tra gruppi di step successivi per l'equilibrio idrodinamico $10^{-10}$, $K_n=0.0011$, $\tau=1.5$, $v_{in}=-v_{out}=10^{-5}$. Ogni curva è da riferirsi ai valori estratti per un certo valore di y vicino a inlet/outlet}
		\label{fig:500x200confronto}
	\end{figure}

	\begin{figure}
		\centering
		\includegraphics[width=1\linewidth]{fig/500x200_confronto_dettaglio}
		\caption{Dettaglio per il grafico in fig.\ref{fig:500x200confronto}}
		\label{fig:500x200confrontodettaglio}
	\end{figure}
	
	
	Allo stato attuale delle prove si è notato come forzando le velocità dei nodi di bordo ad essere nulla (anche se questo non dovrebbe ragionevolmente succedere, non essendo di fatto dei nodi di muro, ma dei nodi di fluido) si otterrebbe un confronto migliore tra simulazione e fisica. Si riporta questa situazione in fig.\ref{fig:800x200noborder}; questa figura viene riportata solo a scopo di possibile indizio sui problemi del codice. Si riferisce infatti ad un reticolo di dimensioni maggiori rispetto a quelli proposti fino ad ora, ma utilizzando gli stessi altri parametri. Questo è il motivo per cui si nota che le curve numeriche sono, lontano dai picchi, relativamente più lontane dalle analitiche; situazione discussa nei prossimi paragrafi e risolvibile aumentando le iterazioni della simulazione.
	
	\begin{figure}
		\centering
		\includegraphics[width=1\linewidth]{fig/800x200_noborder}
		\caption{Secondo termine del potenziale elettrochimico $\Phi$ (eq. \ref{eqpot}). Le curve tratteggiate rappresentano la soluzione analitica fornita dalle eqq. \ref{Phi_ec}, \ref{F2}, mentre i punti sono i valori numerici estratti dalla simulazione con L=800, W=200, soglia di differenza velocità tra gruppi di step successivi per l'equilibrio idrodinamico $10^{-10}$, $K_n=0.0007$, $\tau=1.5$, $v_{in}=-v_{out}=10^{-5}$. Ogni curva è da riferirsi ai valori estratti per un certo valore di y vicino a inlet/outlet. Velocità di bordo forzate a 0}
		\label{fig:800x200noborder}
	\end{figure}
	
	
	
	\subsection{Nuovo tentativo di fit con utilizzo di $f_{eq}$}
	

	
	
	Tentando di capire il motivo per cui, usando \ref{prefattore} in combinazione a \ref{Phi_ec} e \ref{F2}, la simulazione numerica produca risultati troppo grandi, si è effettuata una simulazione utilizzando le popolazioni all'equilibrio $f_{eq}$ al posto di \ref{movingW}
	
	\begin{equation}
		\label{feq}
		f_{i}^{\mathrm{eq}}=\rho \omega_{i}\left[1+\frac{\boldsymbol{e}_{i} \cdot \boldsymbol{u}}{c_{s}^{2}}+\frac{\left(\boldsymbol{e}_{i} \cdot \boldsymbol{u}\right)^{2}}{2 c_{s}^{4}}-\frac{\boldsymbol{u}^{2}}{2 c_{s}^{2}}\right]
	\end{equation}
	
	con le velocità $\boldsymbol{u}$ poste uguali alle $u_{\mathrm{w}}$ della \ref{movingW} e il risultato è riportato in fig.\ref{fig:800x200FEQ_fit}. Confrontando quest'ultima con \ref{fig:800x200noFEQ_fit} non si notano sostanziali differenze e il fit risulterebbe avvicinarsi alle soluzioni numeriche solo correggendo $A$ di \ref{prefattore} di un fattore 10, che si sta ancora cercando di capire dove venga perso.
	
	
	\begin{figure}
		\centering
		\includegraphics[width=1\linewidth]{fig/800x200_feq.pdf}
		\caption{Secondo termine del potenziale elettrochimico $\Phi$ (eq. \ref{eqpot}). Le curve tratteggiate rappresentano la soluzione analitica fornita dalle eqq. \ref{Phi_ec}, \ref{F2}, mentre i punti sono i valori numerici estratti dalla simulazione con L=800, W=200, soglia di differenza velocità tra gruppi di step successivi per l'equilibrio idrodinamico $10^{-10}$, $K_n=0.0007$, $\tau=1.5$, $v_{in}=-v_{out}=10^{-5}$. \emph{Utilizzo di $f_{eq}$ nelle boundary per inlet e outlet}}
		\label{fig:800x200FEQ_fit}
	\end{figure}
	
	
	
	\subsection{Asimmetria e perdita di massa}
	Nelle vecchie simulazioni si ottenevano curve per il potenziale chimico con una evidente asimmetria tra lato inlet e lato outlet. Questa differenza è stata apparentemente molto migliorata con le BC delle nuove versioni del codice.
	
	In particolare si nota come l'asimmetria risulti essere sempre meno pronunciata al crescere di $L$. Si riportano, per un confronto, due simulazioni, una con un reticolo $500x200$ in fig.\ref{fig:500x200_num} e una in un reticolo $800x200$ in fig.\ref{fig:800x200_num}
	
	
	\begin{figure}
		\centering
			\includegraphics[width=1\linewidth]{fig/500x200_num.png}
			\caption{Secondo termine del potenziale elettrochimico $\Phi$ (eq. \ref{eqpot}). L=500, W=200, soglia di differenza velocità tra gruppi di step successivi per l'equilibrio idrodinamico $10^{-10}$, $K_n=0.0011$, $\tau=1.5$, $v_{in}=-v_{out}=10^{-5}$}
		\label{fig:500x200_num}
	\end{figure}
	
	\begin{figure}
		\centering
			\includegraphics[width=1\linewidth]{fig/800x200_num.png}
			\caption{Secondo termine del potenziale elettrochimico $\Phi$ (eq. \ref{eqpot}). L=800, W=200, soglia di differenza velocità tra gruppi di step successivi per l'equilibrio idrodinamico $10^{-10}$, $K_n=0.0007$, $\tau=1.5$, $v_{in}=-v_{out}=10^{-5}$}
		\label{fig:800x200_num}
	\end{figure}


	
	Si riportano i valori di $\Phi(L=0)$, $\Phi(L_{max}/2)$ e $\Phi(L_{max})$ oltre alla differenza tra $\Phi(L=0)$ ed $\Phi(L_{max})$, sia per il caso 500x200 (tabella \ref{tab500x200}), che per quello 800x200 (tabella \ref{tab800x200}).
	Dai dati si nota come effettivamente la differenza tra gli estremi diminuisca all'aumentare di L, ma anche che i punti estremali sono più vicini allo 0; tuttavia nel caso 800x200 l'estremo $\Phi(L=0)$ prende valori negativi e questo potrebbe essere da indagare ulteriormente. \textcolor{red}{Ho messo anche il punto centrale per capire se le curve si fossero abbassate in generale, ma apparentemente no. Non so se queste differenze di valori rappresentino ancora una asimmetria significativa o se è tutto sommato normale per una simulazione numerica. Notavo, ma potrei sbagliarmi, che anche in \cite{Gabbana_2020} sembra esserci un minimo di asimmetria nella fig.17, ma forse è solo un inganno ottico.}

\begin{table}[H]
\centering
	\begin{tabular}{|c|c|c|c|}
		\hline
			                         & y=7       &  y=10        &  y=13      \\
		\hline
		\ $\Phi(L=0)$                & 2.45e-09  &  2.45e-09    &  2.46e-09  \\
		\hline
		\ $\Phi(L=400)$              & -2.16e-09 & -2.16e-09    & -2.16e-09  \\
		\hline
		\ $\Phi(L=800)$              & -7.07e-09 & -7.07e-09    & -7.08e-09  \\
		\hline
		\ $\Phi(L=0) - \Phi(L=800)$  & 9.53e-09  &  9.52e-09    &  9.55e-09  \\
		\hline
	\end{tabular}
\caption{reticolo 500x200}
\label{tab500x200}
\end{table}

	
	
\begin{table}[H]
\centering
		\begin{tabular}{|c|c|c|c|}
		\hline
		                            & y=7       & y=10      & y=13       \\
		\hline
		\ $\Phi(L=0)$ 				& -1.84e-09 & -1.85e-09 & -1.85e-09  \\
		\hline
		\ $\Phi(L=400)$ 			& -1.13e-09 & -1.13e-09 & -1.13e-09  \\
		\hline
		\ $\Phi(L=800)$ 			& -5.22e-10 & -5.10e-10 & -5.09e-10  \\
		\hline
		\ $\Phi(L=0) - \Phi(L=800)$ & -1.32e-09 & -1.34e-09 & -1.34e-09  \\
		\hline
	\end{tabular}
\caption{reticolo 800x200}
\label{tab800x200}
\end{table}


Rimane un problema di perdita di massa nelle simulazioni, come citato anche nella versione precedente del report, e su questo non è stato fatto molto di nuovo. 


Si riportano, infine, alcuni risultati che possono risultare utili a capire dove sia da ricercare il problema di perdita di massa e/o di asimmetria. Notiamo per prima cosa che, abbassando dal solito $10^{-10}$ a $10^{-14}$ la soglia sulla differenza di velocità media tra due configurazioni, richiesta per poter ipotizzare l'equilibrio idrodinamico, si ottengono risultati come fig.\ref{fig:800x200x6-14} con una ben più marcata asimmetria rispetto ai risultati mostrati fino ad ora. Questo potrebbe essere dovuto all'incremento di iterazioni di simulazione necessario per ottenere un equilibrio più preciso, e quindi alla perdita di massa più importante. Si nota che è soprattutto il lato destro del reticolo a mostrare un comportamento più problematico. 

Per comprendere se il problema possa essere legato ad una condizione nell'outlet, piuttosto che in un bug localizzato ad alti valori di L, si sono fatte due simulazioni con risultati riportati in fig.\ref{fig:800x200inver} e \ref{fig:800x200feq-14}. La prima con inversione di inlet e outlet, e la seconda con funzione di equilibrio come BC per inlet e outlet; ambedue con soglia sulle velocità medie di $10^{-14}$.
\textcolor{red}{Questi risultati sembrano suggerire che il problema non sia nelle BC di inlet e outlet; provo a cercare qualche bug nelle condizioni del bordo laterale destro o comunque della parte destra del reticolo.}


\begin{figure}
	\centering
	\includegraphics[width=1\linewidth]{fig/800x200x6_-14}
	\caption{Potenziale chimico per un reticolo 800x200, con gli stessi parametri di fig.\ref{fig:800x200_num}, ma soglia di differenza tra velocità medie successive per fermare la simulazione pari a $10^{-14}$ invece che il solito $10^{-10}$}
	\label{fig:800x200x6-14}
\end{figure}




\begin{figure}
	\centering
	\includegraphics[width=1\linewidth]{fig/800x200_inver_.14}
	\caption{Termine di pressione del potenziale elettrochimico per un reticolo 800x200 con inlet e outlet inverti, soglia $10^{-14}$ e rimanenti parametri identici a \ref{fig:800x200noFEQ_fit}}
	\label{fig:800x200inver}
\end{figure}




\begin{figure}
	\centering
	\includegraphics[width=1\linewidth]{fig/800x200_feq_-14}
	\caption{Termine di pressione del potenziale elettrochimico per un reticolo 800x200 con utilizzo di $f_{eq}$ nelle BC di inlet e outlet, soglia $10^{-14}$ e rimanenti parametri identici a \ref{fig:800x200noFEQ_fit}}
	\label{fig:800x200feq-14}
\end{figure}






Come da precedente report si ricorda anche che questo stesso codice produce risultati visibilmente asimmetrici (fig.\ref{fig:160x40x3num}) per reticoli più piccoli, ma con aspect ratio simile a quelli commentati fino ad ora. 


\begin{figure}
	\centering
	\includegraphics[width=1\linewidth]{fig/160x40x3_num}
	\caption{Esempio di risultato con reticolo 160x40 con 3 punti di inlet e outlet, $K_n=0.0036$ e rimanenti parametri identici a quelli delle simulazioni precedenti.}
	\label{fig:160x40x3num}
\end{figure}


\section{Forzante}	
	
Nonostante i dubbi ancora aperti, discussi nelle precedenti sezioni, si è comunque iniziato ad implementare nella simulazione il termine di forzante \cite{Gabbana_2020}

\begin{equation}
\label{force}
\boldsymbol{F}=-e \nabla \varphi-\frac{1}{\tau_{\mathrm{D}}} n \boldsymbol{u}
\end{equation}
	
dove il primo contributo è quello auto-indotto dagli elettroni stessi e il secondo è un contribto dagli urti tra fononi ed elettroni, e parametrizzato dal tempo di scattering $\tau_D = 10^5$ (il valore scelto in questo momento non è particolarmente ragionato). In particolare si è scelto di lavorare in local capacitance approximation, quindi

\begin{equation}
\varphi(x, y)=-\frac{e}{C_{\mathrm{g}}} \bar{n}(x, y)
\end{equation}

Si riportano in fig.\ref{fig:800x200x6ftot} i risultati numerici per il potenziale elettrochimico, secondo la \ref{eqpot}, con $C_{\mathrm{g}} = 10^5$, $e=1$, e 

\begin{equation}
\delta P(x, y) \approx T(x, y)(n(x, y)-\bar{n}(x, y))
\end{equation}

continuando ad assumere $T(x,y) = 1$.

\begin{figure}
	\centering
	\includegraphics[width=1\linewidth]{fig/800x200x6_Ftot}
	\caption{Reticolo 800x200 con 6 punti di inlet e outlet, con contributo della forzante \ref{force}, dove $C_{\mathrm{g}} = 10^5$ e $\tau_D = 10^5$. Rimanenti parametri identici alle simulazioni precedenti.}
	\label{fig:800x200x6ftot}
\end{figure}

Anche in questo caso un fit con i valori secondo la \ref{prefattore} risulterebbe in un fattore 10 di errore sul valore di inlet e outlet, e per questo non si è affollato il grafico con le curve analitiche; la situazione sarebbe molto simile a \ref{fig:800x200noFEQ_fit}. 
	
	

	

	
	
	
	
	
	
	\bibliography{bib}{}
	\thispagestyle{empty}
	\bibliographystyle{plain}
	
\end{document}