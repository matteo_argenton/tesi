import numpy as np
import matplotlib.pyplot as plt
from numpy import loadtxt




data_num1 = loadtxt('datanum_u1.5.csv', delimiter=',')
data_num2 = loadtxt('datanum_u0.8.csv', delimiter=',')

data_an1 = loadtxt('dataan_u1.5.csv', delimiter=',')
data_an2 = loadtxt('dataan_u0.8.csv', delimiter=',')

data_numsigma1 = loadtxt('datanum_sigmaxx1.5.csv', delimiter=',')
data_numsigma2 = loadtxt('datanum_sigmaxx0.8.csv', delimiter=',')

data_ansigma1 = loadtxt('dataan_sigmaxx1.5.csv', delimiter=',')
data_ansigma2 = loadtxt('dataan_sigmaxx0.8.csv', delimiter=',')


#plot


fig, (ax1, ax2) = plt.subplots(2, figsize=(5,5), dpi=200)


ax1.plot(data_an1 ,'c', label = "analytical" + r", $\tau$ = 1.5")
ax1.plot(data_num1,'bo', markersize=1 , label = "numerical" + r", $\tau$ = 1.5")

ax1.plot(data_an2 ,'g', label = "analytical" + r", $\tau$ = 0.8")
ax1.plot(data_num2,'ko', markersize=1 , label = "numerical" + r", $\tau$ = 0.8")
ax1.legend(loc='best', fontsize='small')
ax1.grid()
ax1.set(ylabel=r"u/$u_0$ [lu] at y=20")


ax2.plot(data_ansigma1 ,'c', label = "analytical" + r", $\tau$ = 1.5")
ax2.plot(data_numsigma1,'bo', markersize=1 , label = "numerical" + r", $\tau$ = 1.5")
ax2.plot(data_ansigma2 ,'g', label = "analytical" + r", $\tau$ = 0.8")
ax2.plot(data_numsigma2,'ko', markersize=1 , label = "numerical" + r", $\tau$ = 0.8")
ax2.legend(loc='best', fontsize='small')
ax2.grid()
ax2.set(xlabel="Lx[lu]",ylabel=r"$\sigma_{xx}/P_0$ [lu] at y=20")

plt.subplots_adjust(left=0.2, bottom=None, right=None, top=None, wspace=0.4, hspace=None)
#plt.savefig("usigma17.png")
plt.show()