#plots the decay of velocity

import numpy as np
import matplotlib.pyplot as plt
from numpy import loadtxt




data_num1 = loadtxt('d2q9.0datanum_d0.004.csv', delimiter=',')
data_num2 = loadtxt('d2q17.0datanum_d0.0037953404.csv', delimiter=',')

data_an1 = loadtxt('d2q9.0dataan_d0.004.csv', delimiter=',')
data_an2 = loadtxt('d2q17.0dataan_d0.0037953404.csv', delimiter=',')


#plot


fig, ax1 = plt.subplots(1, figsize=(5,5), dpi=200)

fig.suptitle(r"$\nu=0.2309401076758503$")

ax1.plot(data_an1 ,'k' , linewidth=1, label = "analytical D2Q9")

ax1.plot(data_num1,'gx', markersize=2 , label = "numerical D2Q9")
ax1.plot(data_num2,'ro', markersize=0.7 , label = "numerical D2Q17")
ax1.plot(data_an2 ,'k' , linewidth=1, label = "analytical D2Q17")
ax1.legend()
ax1.grid()
ax1.set(xlabel="t[lu]",ylabel=r"<|u/$u_0$|> [lu] at y=20")



plt.show()