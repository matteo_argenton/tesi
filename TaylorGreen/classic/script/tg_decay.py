#computes and exports data for a certain avg velocity decay plot

import numpy as np
import matplotlib.pyplot as plt
from numpy import savetxt

# Lx = 150
# Ly = 100
# t_min = 0
# t_max = 250
# Iteration = 250.0
# tau   = 1.5

# #D2Q9
# cs2 = 1./3.

# #D2Q17
# #cs2 = 0.3702518670183399

# nu = ( tau - 0.5 )*cs2



with open('../dump/settings.txt') as f:
    lines = (line for line in f if not line.startswith('#'))
    setting = np.loadtxt(lines, delimiter=',', skiprows=0)

stencil = setting[0]
Lx 		= int(setting[1])
Ly 		= int(setting[2])
tau     = setting[3]
nu 		= setting[4]
kn   	= setting[5]
t_max 	= int(setting[6])
u0   	= setting[7]

t_min 	= 0

print(Lx,Ly,tau,nu,t_max,kn)

#analytical TG solution at t=t_max

def TGa(x,y,t):

	kx = 2.0*np.pi/Lx
	ky = 2.0*np.pi/Ly

	td = 1/(nu*(kx*kx + ky*ky))

	ux_a = -u0*np.sqrt(ky/kx)*np.cos(kx*x)*np.sin(ky*y) * (np.exp(-t/td))
	uy_a =  u0*np.sqrt(kx/ky)*np.sin(kx*x)*np.cos(ky*y) * (np.exp(-t/td))

	return np.sqrt(ux_a*ux_a + uy_a*uy_a)

#numerical solution

def TGn(x,y): return np.sqrt(ux[x,y]*ux[x,y] + uy[x,y]*uy[x,y])



mean_t = np.empty(t_max-t_min, dtype='float64')    #assumo un pace di 1
analytical_t = np.empty(t_max-t_min, dtype='float64')
err = np.empty(t_max-t_min, dtype='float64')


for t in range(t_min, t_max):

	data = np.loadtxt("../dump/result" + str(t) + ".dat")

	rho  = data[:,2]
	ux   = data[:,3]
	uy   = data[:,4]

	ux    = np.reshape(ux , (Ly, Lx))  
	uy    = np.reshape(uy , (Ly, Lx))  
	rho   = np.reshape(rho, (Ly, Lx))  

	ux    = np.transpose(ux)
	uy    = np.transpose(uy)
	rho   = np.transpose(rho)

	data_num = np.empty(Lx*Ly, dtype='float64')
	data_num = np.reshape(data_num, (Lx, Ly))

	for x in range(0,Lx):
		for y in range(0,Ly):
			data_num[x,y]=TGn(x,y)/u0

	data_num = np.reshape(data_num, (1,Lx*Ly))

	data_num = np.absolute(data_num)
	mean_t[t] = np.mean(data_num)


	#analytical function

	data_a = np.empty(Lx*Ly, dtype='float64')
	data_a = np.reshape(data_num, (Lx, Ly))

	for x in range(0,Lx):
		for y in range(0,Ly):
			data_a[x,y] = TGa(x,y,t)/u0

	data_a = np.reshape(data_a, (1,Lx*Ly))
	data_a = np.absolute(data_a)	
	analytical_t[t] = np.mean(data_a)
	err[t] = (np.abs(analytical_t[t] - mean_t[t] )) / np.abs(analytical_t[t])




#export data to csv

savetxt('d2q'+ str(stencil) +'datanum_d' + str(kn) + '.csv', mean_t, delimiter=',')
savetxt('d2q'+ str(stencil) +'dataan_d' + str(kn) + '.csv', analytical_t, delimiter=',')



#plot	

fig, (ax1, ax2) = plt.subplots(2, figsize=(5,5), dpi=200)

ax1.plot(analytical_t, label = "analytical")
ax1.plot(mean_t, 'ro', markersize=0.5, label = "numerical")
ax1.legend()
ax1.grid()
ax1.set(ylabel="<|u|>")

ax2.plot(err,'ro', markersize=1, label = "relative error |analytical - numerical| / |analytical|")
ax2.legend()
ax2.grid()
ax2.set_yscale('log')
ax2.set(xlabel="t[lu]",ylabel="Error")

#fig.suptitle('u0 = 0.04,' + r'$ \tau = 0.51$,' + 'Lx=Ly=150,'+ 't=250', fontsize=15)


plt.show()