import numpy as np
import matplotlib.pyplot as plt
from numpy import savetxt

Lx = 150
Ly = 100
Iteration = 250
tau   = 1.5


#D2Q9
#cs2 = 1./3.

#D2Q17
cs2 = 0.3702518670183399

nu = ( tau - 0.5 )*cs2

#max initial velocity
u0 = 0.04

y_cut = 20

data = np.loadtxt("../dump/resultsTG.dat")

rho   = data[:,2]
ux    = data[:,3]
uy    = data[:,4]
sigma = data[:,5]

ux    = np.reshape(ux   , (Ly, Lx))  
uy    = np.reshape(uy   , (Ly, Lx))  
rho   = np.reshape(rho  , (Ly, Lx))  
sigma = np.reshape(sigma, (Ly, Lx))  
rhoeq = 1.


ux    = np.transpose(ux)
uy    = np.transpose(uy)
rho   = np.transpose(rho)
sigma = np.transpose(sigma)



#analytical TG solution at t=Iteration

def TGa(x,y):

	kx = 2.0*np.pi/Lx
	ky = 2.0*np.pi/Ly

	td = 1/(nu*(kx*kx + ky*ky))

	return 2*rho[x,y]*nu*u0*np.sqrt(kx*ky)*np.sin(kx*x)*np.sin(ky*y)*(np.exp(-Iteration/td))


#data arrays to be compared

data_an  = np.empty(Lx, dtype='float64')
data_num = np.empty(Lx, dtype='float64')

for i in range(0,Lx):
	data_an[i]  = TGa(i,y_cut)	/ (rho[i,y_cut]*u0*u0)  			#considero i punti con ordinata a metà reticolo
	data_num[i] = sigma[i,y_cut]/ (rho[i,y_cut]*u0*u0)  



#comparison numerical solution - analytic function: (analytical(x,y) - numerical(x,y))/analytical(x,y)

# err = np.empty(Lx, dtype='float64')

# for i in range(0,Lx):
# 	err[i] = (np.abs(data_an[i] - data_num[i] )) / np.abs(data_an[i])



#export data to csv

savetxt('datanum_sigmaxx' + str(tau) + '.csv', data_num, delimiter=',')
savetxt('dataan_sigmaxx' + str(tau) + '.csv', data_an, delimiter=',')


#plot


fig, ax1 = plt.subplots(1, figsize=(5,5), dpi=200)

ax1.plot(data_an ,label = "analytical")
ax1.plot(data_num, 'bo', markersize=1, label = "numerical")
ax1.legend()
ax1.grid()
ax1.set(ylabel=r"$\sigma_{xx}$ at y=20")

#ax2.plot(err,'ro', markersize=1, label = "relative error |analytical - numerical| / |analytical|")
#ax2.legend()
#ax2.grid()
#ax2.set_yscale('log')
#ax2.set(xlabel="Lx[lu]",ylabel="Error")


#fig.suptitle('u0 = 0.04,' + r'$ \tau = 1.1$,' + 'Lx=Ly=150,'+ 't=250', fontsize=15)

plt.show()