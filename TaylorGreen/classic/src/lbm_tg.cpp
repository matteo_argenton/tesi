#include "param.h"
#include "tg_functions.h"

void stream(double *f_src, double* f_dst){

	unsigned int x_dst;
	unsigned int y_dst;

	for(unsigned int y = 0; y < NY; y++){
		for(unsigned int x = 0; x < NX; x++){
			for(unsigned int i = 0; i < ndir; i++){

				x_dst = (NX+x-dirx[i]) % NX;       //periodic boundaries
				y_dst = (NY+y-diry[i]) % NY;

				f_dst[field_index(x,y,i)] = f_src[field_index(x_dst,y_dst,i)];
			}
		}
	}
}

void compute_rho_u(double *f, double *r, double *u, double *v){

	double rho;
	double ux;
	double uy;


	for(unsigned int y = 0; y < NY; y++){
		for(unsigned int x = 0; x < NX; x++){

			rho = 0.0;
			ux  = 0.0;
			uy  = 0.0;

			for(unsigned int i = 0; i < ndir; ++i){

				rho += f[field_index(x,y,i)];
				ux  += dirx[i]*f[field_index(x,y,i)];
				uy  += diry[i]*f[field_index(x,y,i)];
			}

			r[scalar_index(x,y)] = rho;
			u[scalar_index(x,y)] = ux/rho;
			v[scalar_index(x,y)] = uy/rho;
		}
	}
}


void collide(double *f, double *r, double *u, double *v){

	double rho, ux, uy, cidotu, feq;

	const double tauinv   = 1.0/tau; // 1/tau
	const double omtauinv = 1.0-tauinv; 	// 1 - 1/tau

		for(unsigned int y = 0; y < NY; y++){
			for(unsigned int x = 0; x < NX; x++){

				rho = r[scalar_index(x,y)];
				ux  = u[scalar_index(x,y)];
				uy  = v[scalar_index(x,y)];

					for(unsigned int i = 0; i < ndir; i++){

						feq = f_eq(x,y,i,ux,uy,rho);
						// relax to equilibrium
						f[field_index(x,y,i)] = omtauinv*f[field_index(x,y,i)] + tauinv*feq;

					}
			}
		}
}



void collide_sig(double *f, double *r, double *u, double *v, double* sigma){

	double rho, ux, uy, cidotu, feq;

	const double tauinv   = 1.0 / tau;
	const double omtauinv = 1.0-tauinv; 	// 1 - 1/tau

		for(unsigned int y = 0; y < NY; y++){
			for(unsigned int x = 0; x < NX; x++){

				rho = r[scalar_index(x,y)];
				ux  = u[scalar_index(x,y)];
				uy  = v[scalar_index(x,y)];
				sigma[scalar_index(x,y)] = 0;

					for(unsigned int i = 0; i < ndir; i++){

						feq = f_eq(x,y,i,ux,uy,rho);

						sigma[scalar_index(x,y)] += (-1.0 + 0.5 * tauinv) * dirx[i]*dirx[i] * ( f[field_index(x,y,i)] - feq);

						f[field_index(x,y,i)] = omtauinv*f[field_index(x,y,i)] + tauinv*feq;

					}
			}
		}
}
