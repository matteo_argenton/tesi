#ifndef _TGFUNC
#define _TGFUNC


/* fast computation for field indexes */

#define scalar_index(x_, y_    ) ( (NX)*(y_) + (x_) )
#define field_index( x_, y_, d_) ( (NX)*((NY)*(d_)+ (y_) ) + (x_) )

/**/


void init_macro_var(double*, double*, double*);      		//initializes density and both components of the macroscopic velocity
void init_equilibrium(double*, double*, double*, double*);  
double f_eq(int , int , int , double, double, double);
void compute_sigma(double*, double*, double*, double*, double*, double*, double*, double*);
void knudsen_check();

#endif