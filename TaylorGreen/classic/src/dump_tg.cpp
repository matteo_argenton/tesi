#include <stdio.h>
#include "tg_functions.h"
#include "param.h"
#include <sys/stat.h>
#include <errno.h>


void init_dump_folder(){
	
	const int dir_err = mkdir("../dump", 0777);
		if (dir_err==-1){
			if(errno == EEXIST){
    			fprintf(stderr,"../dump already existing, continuing...\n");
   	 		}
   	 		else{
   	 			perror("Can't create the new directory - error:");
   	 			exit(2);
   	 		}
		}
}

void get_data(double *r, double *u, double *v, double *sigma_xx, double *sigma_xy, double *sigma_yy, double *sigma_yx){ 

	FILE* fp;

	fp=fopen("../dump/resultsTG.dat", "w");

		for(unsigned int y = 0; y < NY; y++){

			for(unsigned int x = 0; x < NX; x++){

				fprintf(fp, "%u %u %.10lf %.10lf %.10lf %.10lf %.10lf %.10lf %.10lf\n", y,x,r[scalar_index(x,y)],u[scalar_index(x,y)],v[scalar_index(x,y)], sigma_xx[scalar_index(x,y)], sigma_xy[scalar_index(x,y)], sigma_yy[scalar_index(x,y)], sigma_yx[scalar_index(x,y)]);

			}
		}
	fclose(fp);
}

void get_data_iter(unsigned int iter, double *r, double *u, double *v){ 

	FILE* fp;

	char fname[200];

	sprintf(fname, "../dump/result%d.dat", iter);

	fp=fopen(fname, "w");

		for(unsigned int y = 0; y < NY; y++){

			for(unsigned int x = 0; x < NX; x++){

				fprintf(fp, "%u %u %.10lf %.10lf %.10lf\n", y,x,r[scalar_index(x,y)],u[scalar_index(x,y)],v[scalar_index(x,y)]);

			}
		}
	fclose(fp);
}



void dump_settings(){

	FILE* fp;

		char fname[200];

		sprintf(fname, "../dump/settings.txt");

		fp=fopen(fname, "w");

			fprintf(fp, "#LX=%d, LY=%d, tau=%16lf, nu=%16lf, kn=%.10lf, nsteps=%d, u0=%lf\n", NX, NY, tau, nu, nu/(cs*NX), NSTEPS, u_max);  //header

			#ifdef D2Q9
				fprintf(fp, "# D2Q9 stencil\n");
				fprintf(fp, "9\n");

			#else
				fprintf(fp, "# D2Q17 stencil\n");
				fprintf(fp, "17\n");
			#endif

			fprintf(fp, "%d\n%d\n%.16lf\n%.16lf\n%.10lf\n%d\n%lf", NX, NY, tau, nu, nu/(cs*NX), NSTEPS, u_max);     //numerical output ready to use



		fclose(fp);

}
