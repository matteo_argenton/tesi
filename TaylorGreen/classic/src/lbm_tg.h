#ifndef _LBM_TG
#define _LBM_TG

void stream(double*, double*);
void compute_rho_u(double*, double*, double*, double*);
void collide(double*, double*, double*, double*);
void collide_sig(double*, double*, double*, double*,double*);


#endif