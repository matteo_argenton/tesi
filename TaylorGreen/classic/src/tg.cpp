#include "param.h"
#include "tg_functions.h"
#include "lbm_tg.h"
#include "dump_tg.h"
#include <stdlib.h>

int main(int argc, char* argv[]){


	double *f1    = (double*) malloc(size_ndir);         		//populations, vector field
	double *f2    = (double*) malloc(size_ndir);
	double *rho   = (double*) malloc(size_scalar);	     		//desity, scalar field
	double *ux    = (double*) malloc(size_scalar);
	double *uy    = (double*) malloc(size_scalar);
	double *sigma_xx = (double*) calloc(NX*NY, sizeof(double));	//inizializzato a 0
	double *sigma_xy = (double*) calloc(NX*NY, sizeof(double));	//inizializzato a 0
	double *sigma_yy = (double*) calloc(NX*NY, sizeof(double));	//inizializzato a 0
	double *sigma_yx = (double*) calloc(NX*NY, sizeof(double));	//inizializzato a 0

	double *temp;


	//dump folder existence check/creation
	init_dump_folder();

	knudsen_check();

	dump_settings();

	init_macro_var(rho,ux,uy);

	init_equilibrium(f1,rho,ux,uy);

	for(unsigned int i=0; i<NSTEPS; i++){

		compute_rho_u(f1,rho,ux,uy);
		collide(f1,rho,ux,uy);
		stream(f1,f2);


		temp = f1;
		f1 = f2;
		f2 = temp;

		get_data_iter(i,rho,ux,uy);	   //qui sono valori pre collide/stream, quindi di fatto ad un tempo t-1

	}

	//compute_rho_u(f1,rho,ux,uy);
	//compute_sigma(f1, sigma_xx, sigma_xy, sigma_yy, sigma_yx, ux, uy, rho);
	//get_data(rho,ux,uy, sigma_xx, sigma_xy, sigma_yy, sigma_yx);

}