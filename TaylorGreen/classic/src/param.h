#ifndef _PARAM
#define _PARAM

#include <stdlib.h>
#include "macro.h"


/* simulation */

const unsigned int NSTEPS = 250;



/* D2Q9 */

#ifdef D2Q9
	
	const double cs  = 0.5773502691896257;
	const double cs2 = 1.0/3.0;

	const unsigned int ndir  = 9;

	const double w0 = 4.0/9.0;  // zero weight
	const double ws = 1.0/9.0;  // adjacent weight
	const double wd = 1.0/36.0; // diagonal weight

	const double wi[] = {w0,ws,ws,ws,ws,wd,wd,wd,wd};
	const int dirx[]  = {0,1,0,-1, 0,1,-1,-1, 1};
	const int diry[]  = {0,0,1, 0,-1,1, 1,-1,-1};

#endif



/* D2Q17 */

#ifdef D2Q17

	const double cs = 0.6084832512225295;
	const double cs2= 0.3702518670183399;

	const unsigned int ndir  = 17;

	const double w0 = 0.402005146909112625941664;
	const double ws = 0.116154866497781543874035;
	const double wd = 0.033006353622986913949754;
	const double we = 0.000079078602165918131237;
	const double wf = 0.000258414549787467559557;

	const double wi[] = {w0,ws,ws,ws,ws,wd,wd,wd,wd,we,we,we,we,wf,wf,wf,wf};
	const int dirx[]  = {0 ,-1, 0, 0, 1,-1,-1, 1, 1,-2,-2, 2, 2,-3, 0, 0, 3};
	const int diry[]  = {0 , 0,-1, 1, 0,-1, 1,-1, 1,-2, 2,-2, 2, 0,-3, 3, 0};

#endif




/* dynamics */

const double rho0  = 1.00;
//const double tau   = 1.5;
//const double nu    = ( tau - 0.5 )*cs2;


/*----------------------------for fixed nu experiment ------------------------------*/

const double u_max = 0.04;   
const double nu    = 0.2309401076758503;



/*---------------------- for fixed Kn and Re experiment ----------------------------*/

// #ifdef D2Q9
// const double u_max = 0.0401634969871044;
// const double nu    = 0.2309401076758503;   //D2Q9 -> Kn =0.004
// #endif

// #ifdef D2Q17
// const double u_max = 0.04232926965026293;
// const double nu    = 0.24339330048901184;     //D2Q17 -> Kn = 0.004
// #endif

/*************************************************************************************/

const double tau   = nu / cs2 + 0.5;


/* geometry */

const unsigned int NX = 100;
const unsigned int NY = 150;


/* Lattice */
const size_t size_ndir   = sizeof(double)*NX*NY*ndir;
const size_t size_scalar = sizeof(double)*NX*NY;

#endif