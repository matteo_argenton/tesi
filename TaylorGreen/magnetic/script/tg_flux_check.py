#plots the flux

import numpy as np
import matplotlib.pyplot as plt


Lx=150
Ly=150

data = np.loadtxt("../dump/resultsTG.dat")

rho  = data[:,2]
ux   = data[:,3]
uy   = data[:,4]

ux    = np.reshape(ux , (Ly, Lx))  
uy    = np.reshape(uy , (Ly, Lx))  
rho   = np.reshape(rho, (Ly, Lx))  
rhoeq = 1.

u = (ux**2 + uy**2)**0.5

x    = np.arange(0, Lx) 
y    = np.arange(0, Ly) 
X, Y = np.meshgrid(x, y)



############################################################################################
#plot

fig, axs = plt.subplots(2,1)

axs[0].set_title("x")
axs[0].streamplot(X, Y, ux, uy, color = u, linewidth = 2, cmap ='autumn')

axs[1].set_title("Flux")
strm = axs[1].imshow(rho-rhoeq, cmap='jet')
fig.colorbar(strm, orientation="horizontal")

plt.show()