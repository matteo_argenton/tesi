import numpy as np
import matplotlib.pyplot as plt
from numpy import loadtxt
import sys

K1 = float(sys.argv[1])
K2 = float(sys.argv[2])

xx_num1 = loadtxt('xxnum-3.csv', delimiter=',')
xx_an1  = loadtxt('xxB-3.csv', delimiter=',')
xx_p1   = loadtxt('xxP-3.csv', delimiter=',')

xy_num1 = loadtxt('xynum-3.csv', delimiter=',')
xy_an1  = loadtxt('xyB-3.csv', delimiter=',')
xy_p1   = loadtxt('xyP-3.csv', delimiter=',')

xx_num2 = loadtxt('xxnum-2.csv', delimiter=',')
xx_an2  = loadtxt('xxB-2.csv', delimiter=',')
xx_p2   = loadtxt('xxP-2.csv', delimiter=',')

xy_num2 = loadtxt('xynum-2.csv', delimiter=',')
xy_an2  = loadtxt('xyB-2.csv', delimiter=',')
xy_p2   = loadtxt('xyP-2.csv', delimiter=',')

xx_num3 = loadtxt('xxnum-1.csv', delimiter=',')
xx_an3  = loadtxt('xxB-1.csv', delimiter=',')
xx_p3   = loadtxt('xxP-1.csv', delimiter=',')

xy_num3 = loadtxt('xynum-1.csv', delimiter=',')
xy_an3  = loadtxt('xyB-1.csv', delimiter=',')
xy_p3   = loadtxt('xyP-1.csv', delimiter=',')


#plot


#fig, ((ax1, ax2), (ax3, ax4), (ax5, ax6)) = plt.subplots(3,2, figsize=(5,5), dpi=200)

fig, ((ax1, ax2), (ax5, ax6)) = plt.subplots(2,2, figsize=(5,5), dpi=200)

fig.suptitle('t=1000 [lu]', fontsize=16)

ax1.title.set_text(r'$K$ = $10^{-3}$')
ax1.plot(xx_p1 ,'c', label = "Pellegrino"+ r", $\eta=\eta_{num}\simeq 0.33$," + "\n" +r" $ \eta_H=-0.000628$")
ax1.plot(xx_num1,'bo', markersize=1 , label = "numerical")
ax1.plot(xx_an1,'k^', markersize=1 , label = "analytical B=0")
ax1.grid()
ax1.set(ylabel=r"$\sigma_{xx}/P_0$ at y=20")
ax1.legend(loc='best', fontsize='small')

ax2.title.set_text(r'$K$ = $10^{-3}$')
ax2.plot(xy_p1 ,'c', label = "Pellegrino" + r", $\eta=\eta_{num}\simeq 0.33$," + "\n" +r" $ \eta_H=-0.000628$")
ax2.plot(xy_num1,'bo', markersize=1 , label = "numerical")
ax2.plot(xy_an1,'k^', markersize=1 , label = "analytical B=0")
ax2.grid()
ax2.set(ylabel=r"$\sigma_{xy}/P_0$ at y=20")
ax2.legend(loc='best', fontsize='small')

# ax3.title.set_text(r'$K$ = $10^{-2}$')
# ax3.plot(xx_p2 ,'c', label = "Pellegrino"+ r", $\eta=\eta_{num}\simeq 0.33$, $ \eta_H=-0.0053137713$")
# ax3.plot(xx_num2,'bo', markersize=1 , label = "numerical")
# ax3.plot(xx_an2,'k^', markersize=1 , label = "analytical B=0")
# ax3.grid()
# ax3.set(xlabel="Lx[lu]",ylabel=r"$\sigma_{xx}/P_0$ at y=20")
# ax3.legend(loc='upper left', fontsize='small')

# ax4.title.set_text(r'$K$ = $10^{-2}$')
# ax4.plot(xy_p2 ,'c', label = "Pellegrino"+ r", $\eta=\eta_{num}\simeq 0.33$, $ \eta_H=-0.0053137713$")
# ax4.plot(xy_num2,'bo', markersize=1 , label = "numerical")
# ax4.plot(xy_an2,'k^', markersize=1 , label = "analytical B=0")
# ax4.grid()
# ax4.set(xlabel="Lx[lu]",ylabel=r"$\sigma_{xy}/P_0$ at y=20")
# ax4.legend(loc='best', fontsize='small')

ax5.title.set_text(r'$K$ = $10^{-1}$')
ax5.plot(xx_p3 ,'c', label = "Pellegrino"+ r", $\eta=\eta_{num}\simeq 0.33$," + "\n" +r" $ \eta_H=-0.101580$")
ax5.plot(xx_num3,'bo', markersize=1 , label = "numerical")
ax5.plot(xx_an3,'k^', markersize=1 , label = "analytical B=0")
ax5.grid()
ax5.set(xlabel="Lx[lu]",ylabel=r"$\sigma_{xx}/P_0$ at y=20")
ax5.legend(loc='best', fontsize='small')

ax6.title.set_text(r'$K$ = $10^{-1}$')
ax6.plot(xy_p3 ,'c', label = "Pellegrino"+ r", $\eta=\eta_{num}\simeq 0.33$," + "\n" +r" $ \eta_H=-0.101580$")
ax6.plot(xy_num3,'bo', markersize=1 , label = "numerical")
ax6.plot(xy_an3,'k^', markersize=1 , label = "analytical B=0")
ax6.grid()
ax6.set(xlabel="Lx[lu]",ylabel=r"$\sigma_{xy}/P_0$ at y=20")
ax6.legend(loc='bottom left', fontsize='small')

plt.show()