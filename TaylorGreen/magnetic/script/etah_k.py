#plot for the K dependence of eta_H

import numpy as np
import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression

data = np.loadtxt("fitvalueHDEBUG.csv")

K  = data[:,0]
H  = data[:,1]
status = data[:,3]
exx = data[:,4]
exy = data[:,5]




good_K = []
good_H = []
bad_K  = []
bad_H  = []
rxx    = []
rxy    = []

for i in range(len(K)):
	if (status[i] == 0.0000000000):
		good_K.append(K[i])
		good_H.append(H[i])
		rxx.append(exx[i])
		rxy.append(exy[i])
	else:
		bad_K.append(K[i])
		bad_H.append(H[i])

good_K = np.array(good_K)
good_H = np.array(good_H)
bad_K  = np.array(bad_K)
bad_H  = np.array(bad_H)
rxx=np.array(rxx)
rxy=np.array(rxy)




K1=good_K.reshape((-1, 1))
model = LinearRegression().fit(K1, good_H)

r_sq = model.score(K1, good_H)
print('coefficient of determination:', r_sq)

def linear(x):
	return model.intercept_ + x*model.coef_

x = np.linspace(0.0001,0.1555,1000)
y = linear(x)

fig, (ax1, ax3) = plt.subplots(2,1, figsize=(5,5), dpi=200)

fig.suptitle('minimize_scalar() with Brent algorithm', fontsize=16)

ax1.title.set_text('Successful fits')
ax1.plot(good_K, good_H, '-o',  c='orange', mfc='red', mec='k', markersize=3)
ax1.grid()
ax1.set(ylabel=r"$\eta_H$")
#ax1.set_xscale('log')
#ax1.plot(x,y, label=r"linear regression, $R^2$="+str("{:.2e}".format(r_sq)))


# ax2.grid()
# ax2.title.set_text('Failed fits')
# ax2.plot(bad_K, bad_H, '^',  c='red', mfc='red', mec='k', markersize=1)
# ax2.set(ylabel=r"$\eta_H$")

ax3.title.set_text(r'$R^2$ for fitted sigma')
ax3.plot(good_K, rxx, 'x',  c='green', mfc='red', mec='k', markersize=4, label='xx')
ax3.plot(good_K, rxy, 'o',  c='black', mfc='red', mec='k', markersize=4, label='xy')
ax3.grid()
ax3.legend(loc='best', fontsize='small')
ax3.set(xlabel="K[lu]", ylabel=r'$R^2$')


plt.show()
