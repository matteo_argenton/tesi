import numpy as np
import matplotlib.pyplot as plt
from numpy import savetxt

Lx = 150
Ly = 100
Iteration = 250
tau   = 1.5
nu = ( tau - 0.5 )*(1./3.)

#max initial velocity
u0 = 0.04

y_cut = 20

data = np.loadtxt("../dump/resultsTG.dat")

rho   = data[:,2]
ux    = data[:,3]
uy    = data[:,4]
sigma_xx = data[:,5]
sigma_xy = data[:,6]
sigma_yy = data[:,7]
sigma_yx = data[:,8]

ux    = np.reshape(ux   , (Ly, Lx))  
uy    = np.reshape(uy   , (Ly, Lx))  
rho   = np.reshape(rho  , (Ly, Lx))  
sigma_xx = np.reshape(sigma_xx, (Ly, Lx))
sigma_xy = np.reshape(sigma_xy, (Ly, Lx))  
sigma_yy = np.reshape(sigma_yy, (Ly, Lx))  
sigma_yx = np.reshape(sigma_yx, (Ly, Lx))  



ux    = np.transpose(ux)
uy    = np.transpose(uy)
rho   = np.transpose(rho)
sigma_xx = np.transpose(sigma_xx)
sigma_xy = np.transpose(sigma_xy)
sigma_yy = np.transpose(sigma_yy)
sigma_yx = np.transpose(sigma_yx)



#analytical TG solution for sigma_xx at t=Iteration   made adimensional by / (rho[x,y_cut]*u0*u0)

def TGaN_xx(x,y):

	kx = 2.0*np.pi/Lx
	ky = 2.0*np.pi/Ly

	td = 1/(nu*(kx*kx + ky*ky))

	return (2*rho[x,y]*nu*u0*np.sqrt(kx*ky)*np.sin(kx*x)*np.sin(ky*y)*(np.exp(-Iteration/td)) ) / (rho[x,y]*u0*u0)


#analytical TG solution for sigma_xx at t=Iteration  made adimensional by / (rho[x,y_cut]*u0*u0)

def TGaN_xy(x,y):


	kx = 2.0*np.pi/Lx
	ky = 2.0*np.pi/Ly

	td = 1/(nu*(kx*kx + ky*ky))

	return (rho[x,y]*nu*u0 * ( np.sqrt(kx*kx*kx/ky) - np.sqrt(ky*ky*ky/kx) ) * np.cos(kx*x)*np.cos(ky*y)*(np.exp(-Iteration/td)) )/ (rho[x,y]*u0*u0)


#data arrays to be compared

data_an_xx   = np.empty(Lx, dtype='float64')
data_an_xy   = np.empty(Lx, dtype='float64')
data_an_yy   = np.empty(Lx, dtype='float64')
data_an_yx   = np.empty(Lx, dtype='float64')
data_num_xx  = np.empty(Lx, dtype='float64')
data_num_xy  = np.empty(Lx, dtype='float64')
data_num_yy  = np.empty(Lx, dtype='float64')
data_num_yx  = np.empty(Lx, dtype='float64')

for i in range(0,Lx):
	data_an_xx[i]  = TGaN_xx(i,20)    
	data_an_xy[i]  = TGaN_xy(i,20)  
	data_an_yy[i]  = -TGaN_xx(i,20)    
	data_an_yx[i]  = TGaN_xy(i,20)  
	data_num_xx[i] = sigma_xx[i,20] / (rho[i,y_cut]*u0*u0)
	data_num_xy[i] = sigma_xy[i,20] / (rho[i,y_cut]*u0*u0)
	data_num_yy[i] = sigma_yy[i,20] / (rho[i,y_cut]*u0*u0)
	data_num_yx[i] = sigma_yx[i,20] / (rho[i,y_cut]*u0*u0)


#plot


fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2,2, figsize=(5,5), dpi=200)

ax1.title.set_text("xx")
ax1.plot(data_num_xx,'bo', markersize=2 , label = "numerical")
ax1.plot(data_an_xx,'k^', markersize=1 , label = "analytical B=0")
ax1.grid()
ax1.set(ylabel=r"$\sigma_{xx}/P_0$ at y=20")
ax1.legend(loc='upper right', fontsize='small')

ax2.title.set_text("xy")
ax2.plot(data_num_xy,'bo', markersize=2 , label = "numerical")
ax2.plot(data_an_xy,'k^', markersize=1 , label = "analytical B=0")
ax2.grid()
ax2.set(ylabel=r"$\sigma_{xy}/P_0$ at y=20")
ax2.legend(loc='best', fontsize='small')

ax3.title.set_text("yy")
ax3.plot(data_num_yy,'bo', markersize=2 , label = "numerical")
ax3.plot(data_an_yy,'k^', markersize=1 , label = "analytical B=0")
ax3.grid()
ax3.set(xlabel="Lx[lu]",ylabel=r"$\sigma_{yy}/P_0$ at y=20")
ax3.legend(loc='upper left', fontsize='small')

ax4.title.set_text("yx")
ax4.plot(data_num_yx,'bo', markersize=2 , label = "numerical")
ax4.plot(data_an_yx,'k^', markersize=1 , label = "analytical B=0")
ax4.grid()
ax4.set(xlabel="Lx[lu]",ylabel=r"$\sigma_{yx}/P_0$ at y=20")
ax4.legend(loc='best', fontsize='small')

plt.show()