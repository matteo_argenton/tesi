#plots eta_H - time for different values of K

import numpy as np
import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression

data = np.loadtxt("fitvalueHDEBUG.csv")

K  = data[:,0]
H  = data[:,1]
t  = data[:,2]
status = data[:,3]
rxx = data[:,4]
rxy = data[:,5]

t1 = []
t2 = []
t3 = []


eta1 = []
eta2 = []
eta3 = []

s1 = []
s2 = []
s3 = []

rxx1= []
rxx2= []
rxx3= []

rxy1= []
rxy2= []
rxy3= []



for i in range(len(t)):
	if(K[i] == 0.0010000000):
		t1.append(t[i])
		eta1.append(H[i])
		s1.append(status[i])
		rxx1.append(rxx[i])
		rxy1.append(rxy[i])
	if(K[i] == 0.0100000000):
		t2.append(t[i])
		eta2.append(H[i])
		s2.append(status[i])
		rxx2.append(rxx[i])
		rxy2.append(rxy[i])
	if(K[i] == 0.1000000000):
		t3.append(t[i])
		eta3.append(H[i])
		s3.append(status[i])
		rxx3.append(rxx[i])
		rxy3.append(rxy[i])



good_t1 = []
good_e1 = []
bad_t1  = []
bad_e1  = []
good_rxx1 = []
good_rxy1 = []

good_t2 = []
good_e2 = []
bad_t2  = []
bad_e2  = []
good_rxx2 = []
good_rxy2 = []

good_t3 = []
good_e3 = []
bad_t3  = []
bad_e3  = []
good_rxx3 = []
good_rxy3 = []




for i in range(len(t1)):
	if (s1[i] == 0.0000000000):
		good_t1.append(t1[i])
		good_e1.append(eta1[i])
		good_rxx1.append(rxx1[i])
		good_rxy1.append(rxy1[i])
	else:
		bad_t1.append(t1[i])
		bad_e1.append(eta1[i])

for i in range(len(t2)):
	if (s2[i] == 0.0000000000):
		good_t2.append(t2[i])
		good_e2.append(eta2[i])
		good_rxx2.append(rxx2[i])
		good_rxy2.append(rxy2[i])
	else:
		bad_t2.append(t2[i])
		bad_e2.append(eta2[i])

for i in range(len(t3)):
	if (s3[i] == 0.0000000000):
		good_t3.append(t3[i])
		good_e3.append(eta3[i])
		good_rxx3.append(rxx3[i])
		good_rxy3.append(rxy3[i])
	else:
		bad_t3.append(t3[i])
		bad_e3.append(eta3[i])


good_t1 = np.array(good_t1)
good_e1 = np.array(good_e1)
bad_t1  = np.array(bad_t1)
bad_e1  = np.array(bad_e1)
good_rxx1=np.array(good_rxx1)
good_rxy1=np.array(good_rxy1)



good_t2 = np.array(good_t2)
good_e2 = np.array(good_e2)
bad_t2  = np.array(bad_t2)
bad_e2  = np.array(bad_e2)
good_rxx2=np.array(good_rxx2)
good_rxy2=np.array(good_rxy2)


good_t3 = np.array(good_t3)
good_e3 = np.array(good_e3)
bad_t3  = np.array(bad_t3)
bad_e3  = np.array(bad_e3)
good_rxx3=np.array(good_rxx3)
good_rxy3=np.array(good_rxy3)


fig, ( (ax1, ax2, ax7), (ax3,ax4,ax8), (ax5,ax6,ax9) ) = plt.subplots(3,3, figsize=(12,12), dpi=200)

fig.suptitle('minimize_scalar() with Brent algorithm', fontsize=16)

ax1.title.set_text('Successful fits - K=0.001')
ax1.plot(good_t1, good_e1, '-o',  c='orange', mfc='red', mec='k', markersize=3)
ax2.title.set_text('Failed fits')
ax2.plot(bad_t1, bad_e1, '^',  c='red', mfc='red', mec='k', markersize=3)
ax7.title.set_text(r'$R^2$ for fitted sigma')
ax7.plot(good_t1, good_rxx1, 'x-',  c='green', mfc='red', mec='k', markersize=4, label='xx')
ax7.plot(good_t1, good_rxy1, 'o--',  c='black', mfc='red', mec='k', markersize=4, label='xy')
ax7.get_yaxis().get_major_formatter().set_useOffset(False)
ax7.legend(loc='best', fontsize='small')
ax1.grid()
ax1.set(ylabel=r"$\eta_H$")

ax3.title.set_text('K=0.01')
ax3.plot(good_t2, good_e2, '-o',  c='orange', mfc='red', mec='k', markersize=3)
ax4.plot(bad_t2, bad_e2, '^',  c='red', mfc='red', mec='k', markersize=3)
ax8.plot(good_t2, good_rxx2, 'x-',  c='green', mfc='red', mec='k', markersize=4, label='xx')
ax8.plot(good_t2, good_rxy2, 'o--',  c='black', mfc='red', mec='k', markersize=4, label='xy')
ax8.legend(loc='best', fontsize='small')
ax3.grid()
ax3.set(ylabel=r"$\eta_H$")


ax5.title.set_text('K=0.1')
ax5.plot(good_t3, good_e3, '-o',  c='orange', mfc='red', mec='k', markersize=3)
ax6.plot(bad_t3, bad_e3, '^',  c='red', mfc='red', mec='k', markersize=3)
ax9.plot(good_t3, good_rxx3, 'x-',  c='green', mfc='red', mec='k', markersize=4, label='xx')
ax9.plot(good_t3, good_rxy3, 'o--',  c='black', mfc='red', mec='k', markersize=4, label='xy')
ax9.legend(loc='best', fontsize='small')
ax6.set(xlabel="t[lu]")
ax5.grid()
ax5.set(xlabel="t[lu]", ylabel=r"$\eta_H$")
ax9.set(xlabel="t[lu]")

plt.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=0.4, hspace=None)
plt.savefig("prova.png")
plt.show()