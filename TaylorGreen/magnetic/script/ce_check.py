#this script checks sigma for the functional form with Fu + uF

import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from scipy.optimize import minimize
from scipy.optimize import minimize_scalar
from numpy import savetxt
import sys

## parameters ##

y_cut     =  20              # data y-coordinate to consider
cs2       =  1.0/3.0         # D2Q9
dumpPath  =  '../dump/'



## import cpp simulation settings associated with data dump ##

with open('../dump/ce/settings.txt') as f:
    lines = (line for line in f if not line.startswith('#'))
    setting = np.loadtxt(lines, delimiter=',', skiprows=0)

stencil = setting[0]
Lx 		= int(setting[1])
Ly 		= int(setting[2])
tau     = setting[3]
nu 		= setting[4]
kn   	= setting[5]
t_max 	= int(setting[6])
u0   	= setting[7]
K       = setting[8]
cs2     = setting[9]


#Iteration = t_max


print("I parametri della simulazione sono: K=" + str(K) + ", u_0=" + str(u0) + " ,nu=" + str(nu) + " ,t_max=" + str(t_max) + " ,Lx=" + str(Lx) + " ,Ly=" + str(Ly))





## numerical data input and manipulation of the data structures. Dumped data has the form y,x,rho,ux,uy,sigma components and have to be transposed ##

#data = np.loadtxt("../dump/tdep/result" + str(Iteration) + "_" + str(K) + ".dat")
data = np.loadtxt("../dump/ce/resultsTG.dat")


rho      = data[:,2]
ux       = data[:,3]
uy       = data[:,4]
sigma_xx = data[:,5]
sigma_xy = data[:,6]
sigma_yy = data[:,7]
sigma_yx = data[:,8]

ux       = np.reshape(ux       , (Ly, Lx))
uy       = np.reshape(uy       , (Ly, Lx))
rho      = np.reshape(rho      , (Ly, Lx))
sigma_xx = np.reshape(sigma_xx , (Ly, Lx))
sigma_xy = np.reshape(sigma_xy , (Ly, Lx))
sigma_yy = np.reshape(sigma_yy , (Ly, Lx))  
sigma_yx = np.reshape(sigma_yx , (Ly, Lx))  


ux       = np.transpose(ux)
uy       = np.transpose(uy)
rho      = np.transpose(rho)
sigma_xx = np.transpose(sigma_xx)
sigma_xy = np.transpose(sigma_xy)
sigma_yy = np.transpose(sigma_yy)
sigma_yx = np.transpose(sigma_yx)



## eta inside the numerical simulation = rho*nu ##	

def eta_num(x,y): 
	if(not isinstance(x,int)):
		x = x.astype(np.int)
	if(not isinstance(y,int)):
		y = y.astype(np.int)

	return rho[x,y]*nu


## force terms F_alpha with the macroscopic velocity inside. The ones coming from  sum_i (F_i c_i_alpha) = F_alpha  ##

def Fx(x,y):       #solo la componente x di F_alpha, con la velocità macroscopica
	return  2.0 * rho[x,y] * uy[x,y] * K

def Fy(x,y):
	return -2.0 * rho[x,y] * ux[x,y] * K



## analytical TG solution for sigma_xx at t=Iteration   made adimensional by / (rho[x,y_cut]*u0*u0)  ##

def TGaN_xx(x,y):

	if(not isinstance(x,int)):
		x = x.astype(np.int)
	if(not isinstance(y,int)):
		y = y.astype(np.int)

	kx = 2.0*np.pi/Lx
	ky = 2.0*np.pi/Ly

	td = 1/(nu*(kx*kx + ky*ky))

	return (2*rho[x,y]*nu*u0*np.sqrt(kx*ky)*np.sin(kx*x)*np.sin(ky*y)*(np.exp(-t_max/td)) ) / (rho[x,y]*u0*u0)



def TGaN_xy(x,y):

	if(not isinstance(x,int)):
		x = x.astype(np.int)
	if(not isinstance(y,int)):
		y = y.astype(np.int)

	kx = 2.0*np.pi/Lx
	ky = 2.0*np.pi/Ly

	td = 1/(nu*(kx*kx + ky*ky))

	return (rho[x,y]*nu*u0 * ( np.sqrt(kx*kx*kx/ky) - np.sqrt(ky*ky*ky/kx) ) * np.cos(kx*x)*np.cos(ky*y)*(np.exp(-t_max/td)) )/ (rho[x,y]*u0*u0)



## partial derivative x direction with finite differences approximation ##

def Par_x(x,y,vec):

	if(not isinstance(x,int)):
		x = x.astype(np.int)	  #inside the fits sometimes x and y are given as floats and I need to use them as integer indexes

	fdx = vec[(x+1)%Lx 	  , y]    #I consider the right neighbour and if x is the last point of the array, because of periodicity I go back to point 0 with the modulus %Lx
	fsx = vec[(x-1+Lx)%Lx , y]	  #same as above, but for left neighbour

	return (fdx - fsx) / 2.0

def Par_y(x,y,vec):

	if(not isinstance(x,int)):
		x = x.astype(np.int)

	fup  = vec[x , (y+1)%Ly   ]
	fdwn = vec[x , (y-1+Ly)%Ly]

	return (fup - fdwn) / 2.0


## O(u^2) extra terms from chapman - enskog ##

def correction_xx(x,y):
	return (tau - 0.5) * ( 2 * ux[x,y] * Fx(x,y) )

def correction_xy(x,y):
	return (tau - 0.5) * ( ux[x,y] * Fy(x,y) + uy[x,y] * Fx(x,y) )


## O(u^3) extra terms from chapman - enskog ##

def correction_xx_3(x,y):
	return - (tau - 0.5) * ( ux[x,y]**3 * Par_x(x,y,rho) + ux[x,y]**2 * uy[x,y] * Par_y(x,y,rho) + rho[x,y]* ux[x,y]**2 * (Par_x(x,y,ux) + Par_y(x,y,uy)) + 2* rho[x,y] * ux[x,y]**2 * Par_x(x,y,ux) + 2* rho[x,y]*ux[x,y]*uy[x,y]*Par_y(x,y,ux))
	#return - (tau - 0.5) * ( Par_x(x,y,(rho * ux**3)) + Par_y(x,y,(rho * ux**2 * uy)) )

def correction_xy_3(x,y):
	return - (tau - 0.5) * ( ux[x,y]**2 * uy[x,y] * Par_x(x,y,rho) + ux[x,y] * uy[x,y]**2 * Par_y(x,y,rho) + rho[x,y]* ux[x,y]* uy[x,y] * (Par_x(x,y,ux) + Par_y(x,y,uy)) + rho[x,y] * ux[x,y]**2 * Par_x(x,y,uy) + rho[x,y] * ux[x,y] * uy[x,y] * Par_y(x,y,uy) + rho[x,y] * ux[x,y] * uy[x,y] * Par_x(x,y,ux) + rho[x,y] * uy[x,y]**2 * Par_y(x,y, ux) )


#classic sigma first order contribution

def ce_sigma_xx(x,y):
	#return (tau - 0.5) * (rho[x,y] * cs2) * ( 2* Par_x(x,y,ux) )
	return eta_num(x,y) * ( 2* Par_x(x,y,ux) )


def ce_sigma_xy(x,y):
	return eta_num(x,y) * ( Par_x(x,y,uy) + Par_y(x,y,ux) )


#Pellegrino analytical solutions made adimensional by / (rho[x,y_cut]*u0*u0)

def TGpN_xx(x,y, eta_H):

	if(not isinstance(x,int)):
		x = x.astype(np.int) 

	return (2*eta_num(x,y)*Par_x(x,y,ux) + eta_H * ( Par_x(x,y,uy) + Par_y(x,y,ux) ) )/ (rho[x,y]*u0*u0)

def TGpN_xy(x,y,eta_H):

	if(not isinstance(x,int)):
		x = x.astype(np.int)


	return (eta_num(x,y) * ( Par_x(x,y,uy) + Par_y(x,y,ux) ) + eta_H * ( -Par_x(x,y,ux) + Par_y(x,y,uy) ) )/ (rho[x,y]*u0*u0)




## data arrays to be compared ##
data_an_xx   	   = np.empty(Lx, dtype='float64')
data_an_xy   	   = np.empty(Lx, dtype='float64')
data_num_xx  	   = np.empty(Lx, dtype='float64')
data_num_xy  	   = np.empty(Lx, dtype='float64')
data_num_yy  	   = np.empty(Lx, dtype='float64')
data_num_yx  	   = np.empty(Lx, dtype='float64')
data_per_xx  	   = np.empty(Lx, dtype='float64')
data_per_xy  	   = np.empty(Lx, dtype='float64')
data_ce_xx_2       = np.empty(Lx, dtype='float64')
data_ce_xy_2       = np.empty(Lx, dtype='float64')
data_ce_xx_basic   = np.empty(Lx, dtype='float64')
data_ce_xy_basic   = np.empty(Lx, dtype='float64')
data_ce_xx_3       = np.empty(Lx, dtype='float64')
data_ce_xy_3       = np.empty(Lx, dtype='float64')


## adimensionalizing the numerical results /P0 = rho u_0^2 ##

sigma_xx = sigma_xx / (rho * u0 * u0)
sigma_xy = sigma_xy / (rho * u0 * u0)
sigma_yy = sigma_yy / (rho * u0 * u0)
sigma_yx = sigma_yx / (rho * u0 * u0)



######functions computing the global L2 relative error for each component of sigma ##############

def errL2xx(eta_H):

	err = 0.0
	den = 0.0

	for y in range(0,Ly):
		for x in range(0,Lx):
			err += np.square( (sigma_xx[x,y]-TGpN_xx(x,y,eta_H)) )    #L2 error
			den += np.square( TGpN_xx(x,y,eta_H) )

	return np.sqrt(err/den)



def errL2xy(eta_H):

	err = 0.0
	den = 0.0

	for y in range(0,Ly):
		for x in range(0,Lx):
			err += np.square( (sigma_xy[x,y]-TGpN_xy(x,y,eta_H)) )    #L2 error
			den += np.square( TGpN_xy(x,y,eta_H) )

	return np.sqrt(err/den)



def errL2yy(eta_H):

	err = 0.0
	den = 0.0

	for y in range(0,Ly):
		for x in range(0,Lx):
			err += np.square( (sigma_yy[x,y]-(-TGpN_xx(x,y,eta_H))) )    #L2 error
			den += np.square( TGpN_xx(x,y,eta_H) )    #il segno non è un problema perché è ^2

	return np.sqrt(err/den)


def errL2yx(eta_H):

	err = 0.0
	den = 0.0

	for y in range(0,Ly):
		for x in range(0,Lx):
			err += np.square( (sigma_yx[x,y]-TGpN_xy(x,y,eta_H)) )    #L2 error
			den += np.square( TGpN_xy(x,y,eta_H) )

	return np.sqrt(err/den)




##################### minimization algorithm #############################################################

def obj_f(eta_H):    #funzione obiettivo da minimizzare
	return errL2xx(eta_H) + errL2xy(eta_H) + errL2yy(eta_H) + errL2yx(eta_H)

res = minimize_scalar(obj_f)
print(res)


##### computing R^2 coefficient to evaluate goodness of the fitted curve

#sigma_xx, sigma_xy for fitted pellegrino for every point in the lattice:


# pel_xx   = np.empty(Lx*Ly, dtype='float64')
# pel_xy   = np.empty(Lx*Ly, dtype='float64')

# pel_xx   = np.reshape(pel_xx    , (Ly, Lx))
# pel_xy   = np.reshape(pel_xy    , (Ly, Lx))

# pel_xx   = np.transpose(pel_xx)
# pel_xy   = np.transpose(pel_xy)



# for y in range(0,Ly):	
# 	for x in range(0,Lx):
# 		pel_xx[x,y]=TGpN_xx(x,y,res.x)
# 		pel_xy[x,y]=TGpN_xy(x,y,res.x)


# ss_res_xx = np.sum(   np.ndarray.flatten( (sigma_xx - pel_xx) ** 2)   )
# ss_res_xy = np.sum(   np.ndarray.flatten( (sigma_xy - pel_xy) ** 2)   )

# ss_tot_xx = np.sum(( sigma_xx- np.mean(sigma_xx)) ** 2)
# ss_tot_xy = np.sum(( sigma_xy- np.mean(sigma_xy)) ** 2)


# r2_xx = 1 - (ss_res_xx / ss_tot_xx)
# r2_xy = 1 - (ss_res_xy / ss_tot_xy)





######################################### sigma components on cut ##################################################
# numerical and pellegrino data are already adimensionalized

for x in range(0,Lx): 
	data_an_xx[x]  		 =  TGaN_xx(x,y_cut)    
	data_an_xy[x]  		 =  TGaN_xy(x,y_cut) 
	data_per_xx[x] 		 =  TGpN_xx(x,y_cut,res.x)
	data_per_xy[x] 		 =  TGpN_xy(x,y_cut,res.x)
	data_num_xx[x] 		 =  sigma_xx[x,y_cut]      #already /P0 from earlier
	data_num_xy[x] 		 =  sigma_xy[x,y_cut]
	data_ce_xx_basic[x]  =  ce_sigma_xx(x,y_cut)   							   							  / ( rho[x,y_cut] * u0 * u0 )
	data_ce_xy_basic[x]  =  ce_sigma_xy(x,y_cut)   							   							  / ( rho[x,y_cut] * u0 * u0 )
	data_ce_xx_2[x]  	 = (ce_sigma_xx(x,y_cut) + correction_xx(x,y_cut) )    							  / ( rho[x,y_cut] * u0 * u0 )
	data_ce_xy_2[x]  	 = (ce_sigma_xy(x,y_cut) + correction_xy(x,y_cut) )    							  / ( rho[x,y_cut] * u0 * u0 )
	data_ce_xx_3[x]      = (ce_sigma_xx(x,y_cut) + correction_xx(x,y_cut) + correction_xx_3(x,y_cut) )    / ( rho[x,y_cut] * u0 * u0 )
	data_ce_xy_3[x]      = (ce_sigma_xy(x,y_cut) + correction_xy(x,y_cut) + correction_xy_3(x,y_cut) )    / ( rho[x,y_cut] * u0 * u0 )




## export data points data to csv ##

savetxt(dumpPath + 'xxan'  + str(K) + "_" +str(t_max) + "_" + str(y_cut) +'.csv', data_an_xx,       delimiter=',')
savetxt(dumpPath + 'xxnum' + str(K) + "_" +str(t_max) + "_" + str(y_cut) +'.csv', data_num_xx,      delimiter=',')
savetxt(dumpPath + 'xx1'   + str(K) + "_" +str(t_max) + "_" + str(y_cut) +'.csv', data_ce_xx_basic, delimiter=',')
savetxt(dumpPath + 'xx2'   + str(K) + "_" +str(t_max) + "_" + str(y_cut) +'.csv', data_ce_xx_2 ,    delimiter=',')
savetxt(dumpPath + 'xx3'   + str(K) + "_" +str(t_max) + "_" + str(y_cut) +'.csv', data_ce_xx_3,     delimiter=',')

savetxt(dumpPath + 'xyan'  + str(K) + "_" +str(t_max) + "_" + str(y_cut) +'.csv', data_an_xy,       delimiter=',')
savetxt(dumpPath + 'xynum' + str(K) + "_" +str(t_max) + "_" + str(y_cut) +'.csv', data_num_xy,      delimiter=',')
savetxt(dumpPath + 'xy1'   + str(K) + "_" +str(t_max) + "_" + str(y_cut) +'.csv', data_ce_xy_basic, delimiter=',')
savetxt(dumpPath + 'xy2'   + str(K) + "_" +str(t_max) + "_" + str(y_cut) +'.csv', data_ce_xy_2,     delimiter=',')
savetxt(dumpPath + 'xy3'   + str(K) + "_" +str(t_max) + "_" + str(y_cut) +'.csv', data_ce_xy_3,     delimiter=',')




## plot ##

fig, (ax1, ax2) = plt.subplots(2, figsize=(5,5), dpi=200)


ax1.plot(data_an_xx , 'k-', markersize=1, label = r"B=0 analytical")
ax1.plot(data_num_xx, 'bv' , markersize=1, label = r"numerical")
#ax1.plot(data_per_xx, 'k--' , markersize=1, label = r"Pellegrino")
ax1.plot(data_ce_xx_2, 'g--' , markersize=1, label = r"CE")
#ax1.plot(data_ce_xx_basic, 'r--' , markersize=1, label = r"CE base")
ax1.plot(data_ce_xx_3, 'ko' , markersize=1, label = r"CE 3")
ax1.legend()
ax1.grid()
ax1.set(ylabel=r"$\sigma_{xx}$/$P_0$ at y=20")


ax2.plot(data_an_xy , 'k-', markersize=1, label = r"B=0 analytical")
ax2.plot(data_num_xy, 'bv' , markersize=1, label = r"numerical")
#ax2.plot(data_per_xy, 'k--' , markersize=1, label = r"Pellegrino")
ax2.plot(data_ce_xy_2, 'g--' , markersize=1, label = r"CE")
#ax2.plot(data_ce_xy_basic, 'r--' , markersize=1, label = r"CE base")
ax2.plot(data_ce_xy_3, 'ko' , markersize=1, label = r"CE 3")
ax2.legend()
ax2.grid()
ax2.set(xlabel="Lx[lu]", ylabel=r"$\sigma_{xy}/P_0$ at y=20")


#fig.suptitle(r'K='+ str(K) +r', $\eta=0.33$, $\eta_H=$' + str("{:.2e}".format(res.x[0])) + ', u0 = 0.04,' + r'$ \tau = 1.5$,' + 'Lx=150, Ly=100,'+ 't=250', fontsize=13)

plt.show()
