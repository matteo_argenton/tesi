#script to produce the data for the decayplot of average u

import numpy as np
import matplotlib.pyplot as plt
from numpy import savetxt


Lx = 150
Ly = 100
t_min = 0
t_max = 250
tau   = 1.5
nu = ( tau - 0.5 )*(1./3.)
u0 = 0.04


#analytical TG solution at t=Iteration

def TGa(x,y,t):

	kx = 2.0*np.pi/Lx
	ky = 2.0*np.pi/Ly

	td = 1/(nu*(kx*kx + ky*ky))

	ux_a = -u0*np.sqrt(ky/kx)*np.cos(kx*x)*np.sin(ky*y) * (np.exp(-t/td))
	uy_a =  u0*np.sqrt(kx/ky)*np.sin(kx*x)*np.cos(ky*y) * (np.exp(-t/td))

	return np.sqrt(ux_a*ux_a + uy_a*uy_a)

	#numerical solution

def TGn(x,y): return np.sqrt(ux[x,y]*ux[x,y] + uy[x,y]*uy[x,y])



mean_t = np.empty(t_max-t_min, dtype='float64')    #assumo un pace di 1
analytical_t = np.empty(t_max-t_min, dtype='float64')
err = np.empty(t_max-t_min, dtype='float64')


for t in range(t_min, t_max):

	data = np.loadtxt("../dump/result" + str(t) + ".dat")

	rho  = data[:,2]
	ux   = data[:,3]
	uy   = data[:,4]

	ux    = np.reshape(ux , (Ly, Lx))  
	uy    = np.reshape(uy , (Ly, Lx))  
	rho   = np.reshape(rho, (Ly, Lx))  

	ux    = np.transpose(ux)
	uy    = np.transpose(uy)
	rho   = np.transpose(rho)

	data_num = np.empty(Lx*Ly, dtype='float64')
	data_num = np.reshape(data_num, (Lx, Ly))

	for x in range(0,Lx):
		for y in range(0,Ly):
			data_num[x,y]=TGn(x,y)

	data_num = np.reshape(data_num, (1,Lx*Ly))

	data_num = np.absolute(data_num)
	mean_t[t] = np.mean(data_num)


	#analytical function

	data_a = np.empty(Lx*Ly, dtype='float64')
	data_a = np.reshape(data_num, (Lx, Ly))

	for x in range(0,Lx):
		for y in range(0,Ly):
			data_a[x,y] = TGa(x,y,t)

	data_a = np.reshape(data_a, (1,Lx*Ly))
	data_a = np.absolute(data_a)	
	analytical_t[t] = np.mean(data_a)
	err[t] = (np.abs(analytical_t[t] - mean_t[t] )) / np.abs(analytical_t[t])



#export data to csv

savetxt('datanum_d' + str(tau)+ "-1" + '.csv', mean_t, delimiter=',')
savetxt('dataan_d' + str(tau) + "-1" +'.csv', analytical_t, delimiter=',')


#plot	

fig, (ax1, ax2) = plt.subplots(2, figsize=(5,5), dpi=200)

ax1.plot(analytical_t, label = "analytical")
ax1.plot(mean_t, 'ro', markersize=0.5, label = "numerical")
ax1.legend()
ax1.grid()
ax1.set(ylabel="<|u|>")

ax2.plot(err,'ro', markersize=1, label = "relative error |analytical - numerical| / |analytical|")
ax2.legend()
ax2.grid()
ax2.set_yscale('log')
ax2.set(xlabel="t[lu]",ylabel="Error")

fig.suptitle('K=1e-3, u0 = 0.04,' + r'$ \tau = 0.51$,' + 'Lx=Ly=150', fontsize=15)


plt.show()