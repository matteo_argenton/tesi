#!/bin/bash

for K in 0.001 0.01 0.1
do
	for t in {350..550..5}
	do
		cd /home/m/Desktop/tesi/TaylorGreen/magnetic/src/
		./TG $K $t
		cd /home/m/Desktop/tesi/TaylorGreen/magnetic/script/
		python3 fit_etaH_tot.py $K
	done
done
