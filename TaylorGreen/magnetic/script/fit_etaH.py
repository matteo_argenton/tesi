#
# This is the main script for fitting eta_h and producing the data for sigma plots (numerical, analytical_Pellegrino, analytical_B=0)
#
#####################################################################################################################################

import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from lmfit import minimize, Parameters, fit_report
from scipy.optimize import minimize
from scipy.optimize import minimize_scalar
from numpy import savetxt
import sys


y_cut     =  20              #ordinata della sezione di reticolo considerata


with open('../dump/ce/settings.txt') as f:
    lines = (line for line in f if not line.startswith('#'))
    setting = np.loadtxt(lines, delimiter=',', skiprows=0)

stencil = setting[0]
Lx 		= int(setting[1])
Ly 		= int(setting[2])
tau     = setting[3]
nu 		= setting[4]
kn   	= setting[5]
t_max 	= int(setting[6])
u0   	= setting[7]


K         = float(sys.argv[1])
Iteration = t_max




#numerical input

#data = np.loadtxt("../dump/tdep/result" + str(Iteration) + "_" + str(K) + ".dat")
data = np.loadtxt("../dump/ce/resultsTG.dat")


rho      = data[:,2]
ux       = data[:,3]
uy       = data[:,4]
sigma_xx = data[:,5]
sigma_xy = data[:,6]
sigma_yy = data[:,7]
sigma_yx = data[:,8]

ux       = np.reshape(ux       , (Ly, Lx))
uy       = np.reshape(uy       , (Ly, Lx))
rho      = np.reshape(rho      , (Ly, Lx))
sigma_xx = np.reshape(sigma_xx , (Ly, Lx))
sigma_xy = np.reshape(sigma_xy , (Ly, Lx))
sigma_yy = np.reshape(sigma_yy , (Ly, Lx))  
sigma_yx = np.reshape(sigma_yx , (Ly, Lx))  


ux       = np.transpose(ux)
uy       = np.transpose(uy)
rho      = np.transpose(rho)
sigma_xx = np.transpose(sigma_xx)
sigma_xy = np.transpose(sigma_xy)
sigma_yy = np.transpose(sigma_yy)
sigma_yx = np.transpose(sigma_yx)



#eta inside the numerical simulation = rho*nu	

def eta_num(x,y): 
	if(not isinstance(x,int)):
		x = x.astype(np.int)
	if(not isinstance(y,int)):
		y = y.astype(np.int)

	return rho[x,y]*nu


#partial derivative x direction with finite differences approx

def Par_x(x,y,vec):

	if(not isinstance(x,int)):
		x = x.astype(np.int)	  #curve_fit converte x a float64, ma io devo usarlo come indice

	fdx = vec[(x+1)%Lx 	  , y]    #considero il punto a destra e se arrivo a fine reticolo, viste le condizioni periodiche, torno all'inizio con il %Lx
	fsx = vec[(x-1+Lx)%Lx , y]	  #punto a sinistra e sommo sempre Lx così se sono a x=0 non vado a valori negativi, ma all'ultimo punto del reticolo con %Lx

	return (fdx - fsx) / 2.0

def Par_y(x,y,vec):

	if(not isinstance(x,int)):
		x = x.astype(np.int)

	fup  = vec[x , (y+1)%Ly   ]
	fdwn = vec[x , (y-1+Ly)%Ly]

	return (fup - fdwn) / 2.0



#analytical TG solution for sigma_xx at t=Iteration   made adimensional by / (rho[x,y_cut]*u0*u0)

def TGaN_xx(x,y):

	if(not isinstance(x,int)):
		x = x.astype(np.int)
	if(not isinstance(y,int)):
		y = y.astype(np.int)

	kx = 2.0*np.pi/Lx
	ky = 2.0*np.pi/Ly

	td = 1/(nu*(kx*kx + ky*ky))

	return (2*rho[x,y]*nu*u0*np.sqrt(kx*ky)*np.sin(kx*x)*np.sin(ky*y)*(np.exp(-Iteration/td)) ) / (rho[x,y]*u0*u0)


#analytical TG solution for sigma_xx at t=Iteration  made adimensional by / (rho[x,y_cut]*u0*u0)

def TGaN_xy(x,y):

	if(not isinstance(x,int)):
		x = x.astype(np.int)
	if(not isinstance(y,int)):
		y = y.astype(np.int)

	kx = 2.0*np.pi/Lx
	ky = 2.0*np.pi/Ly

	td = 1/(nu*(kx*kx + ky*ky))

	return (rho[x,y]*nu*u0 * ( np.sqrt(kx*kx*kx/ky) - np.sqrt(ky*ky*ky/kx) ) * np.cos(kx*x)*np.cos(ky*y)*(np.exp(-Iteration/td)) )/ (rho[x,y]*u0*u0)


#Pellegrino analytical solutions made adimensional by / (rho[x,y_cut]*u0*u0)

def TGpN_xx(x,y, eta_H):

	if(not isinstance(x,int)):
		x = x.astype(np.int) 

	return (2*eta_num(x,y)*Par_x(x,y,ux) + eta_H * ( Par_x(x,y,uy) + Par_y(x,y,ux) ) )/ (rho[x,y]*u0*u0)

def TGpN_xy(x,y,eta_H):

	if(not isinstance(x,int)):
		x = x.astype(np.int)


	return (eta_num(x,y) * ( Par_x(x,y,uy) + Par_y(x,y,ux) ) + eta_H * ( -Par_x(x,y,ux) + Par_y(x,y,uy) ) )/ (rho[x,y]*u0*u0)




#data arrays to be compared

data_an_xx   = np.empty(Lx, dtype='float64')
data_an_xy   = np.empty(Lx, dtype='float64')
data_an_yy   = np.empty(Lx, dtype='float64')
data_an_yx   = np.empty(Lx, dtype='float64')
data_num_xx  = np.empty(Lx, dtype='float64')
data_num_xy  = np.empty(Lx, dtype='float64')
data_num_yy  = np.empty(Lx, dtype='float64')
data_num_yx  = np.empty(Lx, dtype='float64')
data_per_xx  = np.empty(Lx, dtype='float64')
data_per_xy  = np.empty(Lx, dtype='float64')
data_per_yy  = np.empty(Lx, dtype='float64')
data_per_yx  = np.empty(Lx, dtype='float64')


#adimensionalizing the numerical results /P0 = rho u_0^2

sigma_xx = sigma_xx / (rho * u0 * u0)
sigma_xy = sigma_xy / (rho * u0 * u0)
sigma_yy = sigma_yy / (rho * u0 * u0)
sigma_yx = sigma_yx / (rho * u0 * u0)



######################functions computing the global L2 relative error for each component of sigma ##############

def errL2xx(eta_H):

	err = 0.0
	den = 0.0

	for y in range(0,Ly):
		for x in range(0,Lx):
			err += np.square( (sigma_xx[x,y]-TGpN_xx(x,y,eta_H)) )    #L2 error
			den += np.square( TGpN_xx(x,y,eta_H) )

	return np.sqrt(err/den)



def errL2xy(eta_H):

	err = 0.0
	den = 0.0

	for y in range(0,Ly):
		for x in range(0,Lx):
			err += np.square( (sigma_xy[x,y]-TGpN_xy(x,y,eta_H)) )    #L2 error
			den += np.square( TGpN_xy(x,y,eta_H) )

	return np.sqrt(err/den)



def errL2yy(eta_H):

	err = 0.0
	den = 0.0

	for y in range(0,Ly):
		for x in range(0,Lx):
			err += np.square( (sigma_yy[x,y]-(-TGpN_xx(x,y,eta_H))) )    #L2 error
			den += np.square( TGpN_xx(x,y,eta_H) )    #il segno non è un problema perché è ^2

	return np.sqrt(err/den)


def errL2yx(eta_H):

	err = 0.0
	den = 0.0

	for y in range(0,Ly):
		for x in range(0,Lx):
			err += np.square( (sigma_yx[x,y]-TGpN_xy(x,y,eta_H)) )    #L2 error
			den += np.square( TGpN_xy(x,y,eta_H) )

	return np.sqrt(err/den)


##########################################################################################################

##################### minimization algorithm #############################################################

def obj_f(eta_H):    #funzione obiettivo da minimizzare
	return errL2xx(eta_H) + errL2xy(eta_H) + errL2yy(eta_H) + errL2yx(eta_H)

res = minimize_scalar(obj_f)


#x0 = -0.0001
#res = minimize(obj_f, x0, method='Nelder-Mead')

#export data K-eta_H to csv
# if (res.success == True):
# 	K_fit_value = np.array([K, res.x[0], Iteration, 0])
# else:
# 	K_fit_value = np.array([K, res.x[0], Iteration, 1])
# with open("fitvalueHtdep.csv", "ab") as f:
# 	savetxt(f, K_fit_value[None], fmt='%.10lf', delimiter=' ')


print(res)





##### computing R^2 coefficient to evaluate goodness of the fitted curve

#sigma_xx, sigma_xy for fitted pellegrino for every point in the lattice:


# pel_xx   = np.empty(Lx*Ly, dtype='float64')
# pel_xy   = np.empty(Lx*Ly, dtype='float64')

# pel_xx   = np.reshape(pel_xx    , (Ly, Lx))
# pel_xy   = np.reshape(pel_xy    , (Ly, Lx))

# pel_xx   = np.transpose(pel_xx)
# pel_xy   = np.transpose(pel_xy)



# for y in range(0,Ly):	
# 	for x in range(0,Lx):
# 		pel_xx[x,y]=TGpN_xx(x,y,res.x)
# 		pel_xy[x,y]=TGpN_xy(x,y,res.x)


# ss_res_xx = np.sum(   np.ndarray.flatten( (sigma_xx - pel_xx) ** 2)   )
# ss_res_xy = np.sum(   np.ndarray.flatten( (sigma_xy - pel_xy) ** 2)   )

# ss_tot_xx = np.sum(( sigma_xx- np.mean(sigma_xx)) ** 2)
# ss_tot_xy = np.sum(( sigma_xy- np.mean(sigma_xy)) ** 2)


# r2_xx = 1 - (ss_res_xx / ss_tot_xx)
# r2_xy = 1 - (ss_res_xy / ss_tot_xy)





###################################### export #############################################################

# if (res.success == True):
# 	K_fit_value = np.array([K, res.x, Iteration, 0, r2_xx, r2_xy])
# else:
# 	K_fit_value = np.array([K, res.x, Iteration, 1, r2_xx, r2_xy])
# with open("fitvalueHDEBUG.csv", "ab") as f:
# 	savetxt(f, K_fit_value[None], fmt='%.10lf', delimiter=' ')






######################################### sigma components on cut ##################################################

#values for Pellegrino's fitted functions

for x in range(0,Lx):
	data_per_xx[x] = TGpN_xx(x,y_cut,res.x)
	data_per_xy[x] = TGpN_xy(x,y_cut,res.x)
	data_an_xx[x]  = TGaN_xx(x,y_cut)    
	data_an_xy[x]  = TGaN_xy(x,y_cut)   
	data_num_xx[x] = sigma_xx[x,y_cut]      #already /P0 from earlier
	data_num_xy[x] = sigma_xy[x,y_cut]


# #export data points data to csv

# savetxt(dumpPath + 'xxnum' + str(K) + "_" +str(Iteration) + "_" + str(y_cut) +'.csv', data_num_xx, delimiter=',')
# savetxt(dumpPath + 'xxP'   + str(K) + "_" +str(Iteration) + "_" + str(y_cut) +'.csv', data_per_xx, delimiter=',')
# savetxt(dumpPath + 'xxB'   + str(K) + "_" +str(Iteration) + "_" + str(y_cut) +'.csv', data_an_xx , delimiter=',')

# savetxt(dumpPath + 'xynum' + str(K) + "_" +str(Iteration) + "_" + str(y_cut) +'.csv', data_num_xy, delimiter=',')
# savetxt(dumpPath + 'xyP'   + str(K) + "_" +str(Iteration) + "_" + str(y_cut) +'.csv', data_per_xy, delimiter=',')
# savetxt(dumpPath + 'xyB'   + str(K) + "_" +str(Iteration) + "_" + str(y_cut) +'.csv', data_an_xy , delimiter=',')



# #plot

fig, (ax1, ax2) = plt.subplots(2, figsize=(5,5), dpi=200)


#ax1.plot(data_an_xx , 'g--', markersize=1, label = r"B=0 analytical")
ax1.plot(data_num_xx, 'bv' , markersize=1, label = r"numerical")
ax1.plot(data_per_xx, 'ro' , markersize=1, label = r"Pellegrino")
ax1.legend()
ax1.grid()
ax1.set(ylabel=r"$\sigma_{xx}$/$P_0$ at y=20")


#ax2.plot(data_an_xy , 'g--', markersize=1, label = r"B=0 analytical")
ax2.plot(data_num_xy, 'bv' , markersize=1, label = r"numerical")
ax2.plot(data_per_xy, 'ro' , markersize=1, label = r"Pellegrino")
ax2.legend()
ax2.grid()
ax2.set(xlabel="Lx[lu]", ylabel=r"$\sigma_{xy}/P_0$ at y=20")


#fig.suptitle(r'K='+ str(K) +r', $\eta=0.33$, $\eta_H=$' + str("{:.2e}".format(res.x[0])) + ', u0 = 0.04,' + r'$ \tau = 1.5$,' + 'Lx=150, Ly=100,'+ 't=250', fontsize=13)

plt.show()
