#plotting script for the decay of u with and without B

import numpy as np
import matplotlib.pyplot as plt
from numpy import loadtxt




data_num1 = loadtxt('datanum_d1.5.csv', delimiter=',')
data_num2 = loadtxt('datanum_d1.3.csv', delimiter=',')
data_an1 = loadtxt('dataan_d1.5.csv', delimiter=',')
data_an2 = loadtxt('dataan_d1.3.csv', delimiter=',')

data_numB1 = loadtxt('datanum_d1.5-3.csv', delimiter=',')
data_numB2 = loadtxt('datanum_d1.5-1.csv', delimiter=',')
data_anB = loadtxt('dataan_d1.5-3.csv', delimiter=',')


#plot


fig, (ax1,ax2) = plt.subplots(2, figsize=(5,5), dpi=200)

ax1.title.set_text("$B=0$")
ax1.text(-0.1, 1.05, "(a)", transform=ax1.transAxes, size=10)
ax1.plot(data_num1,'g^', markersize=1 , label = "numerical" + r", $\tau$ = 1.5")
ax1.plot(data_an1 ,'k', linewidth=1, label = "analytical" + r", $\tau$ = 1.5")
ax1.plot(data_num2,'b^', markersize=1 , label = "numerical" + r", $\tau$ = 1.3")
ax1.plot(data_an2 ,'c', linewidth=1 ,label = "analytical" + r", $\tau$ = 1.3")
ax1.legend()
ax1.grid()
ax1.set(xlabel="t[lu]",ylabel=r"u/$u_0$ at y=20")

ax2.title.set_text(r"$B\neq0$, " + r'$\tau$ = $1.5$')
ax2.text(-0.1, 1.05, "(b)", transform=ax2.transAxes, size=10)
ax2.plot(data_numB1,'g^', markersize=1 , label = "numerical" + r", $K$ = $10^{-3}$")
ax2.plot(data_numB2 ,'b^', markersize=1 ,label = "numerical" + r", $K$ = $10^{-1}$")
ax2.plot(data_anB ,'k', linewidth=1 ,label = "B=0 analytical")
ax2.legend()
ax2.grid()
ax2.set(xlabel="t[lu]",ylabel=r"u/$u_0$ at y=20")



plt.show()