#!/bin/sh

for K in 0.001 0.01 0.1
do
	for t in 250 500 750
	do
		python3 fit_etaH_tot.py $K $t
	done
done
