#this script finds a fit for eta, eta_H for both sigma_xx and sigma_xy together

import numpy as np
import matplotlib.pyplot as plt
from lmfit import minimize, Parameters, fit_report
from numpy import savetxt

Lx        = 150
Ly        = 100
Iteration = 250
tau       = 1.5
nu        = ( tau - 0.5 )*(1./3.)
n_bar     = 1.0
u0        = 0.04

y_cut     = 20              #ordinata della sezione di reticolo considerata


data = np.loadtxt("../dump/resultsTG.dat")

rho      = data[:,2]
ux       = data[:,3]
uy       = data[:,4]
sigma_xx = data[:,5]
sigma_xy = data[:,6]

ux       = np.reshape(ux       , (Ly, Lx))
uy       = np.reshape(uy       , (Ly, Lx))
rho      = np.reshape(rho      , (Ly, Lx))
sigma_xx = np.reshape(sigma_xx , (Ly, Lx))
sigma_xy = np.reshape(sigma_xy , (Ly, Lx))


ux       = np.transpose(ux)
uy       = np.transpose(uy)
rho      = np.transpose(rho)
sigma_xx = np.transpose(sigma_xx)
sigma_xy = np.transpose(sigma_xy)



#eta inside the numerical simulation = rho*nu	

def eta_num(x,y): 
	if(not isinstance(x,int)):
		x = x.astype(np.int)
	if(not isinstance(y,int)):
		y = y.astype(np.int)

	return rho[x,y]*nu


#partial derivative x direction with finite differences approx

def Par_x(x,y,vec):

	if(not isinstance(x,int)):
		x = x.astype(np.int)		  #curve_fit converte x a float64, ma io devo usarlo come indice

	fdx = vec[(x+1)%Lx 	  , y]    #considero il punto a destra e se arrivo a fine reticolo, viste le condizioni periodiche, torno all'inizio con il %Lx
	fsx = vec[(x-1+Lx)%Lx , y]	  #punto a sinistra e sommo sempre Lx così se sono a x=0 non vado a valori negativi, ma all'ultimo punto del reticolo con %Lx

	return (fdx - fsx) / 2.0

def Par_y(x,y,vec):

	if(not isinstance(x,int)):
		x = x.astype(np.int)

	fup  = vec[x , (y+1)%Ly   ]
	fdwn = vec[x , (y-1+Ly)%Ly]

	return (fup - fdwn) / 2.0



#analytical TG solution for sigma_xx at t=Iteration   made adimensional by / (rho[x,y_cut]*u0*u0)

def TGaN_xx(x,y):

	if(not isinstance(x,int)):
		x = x.astype(np.int)
	if(not isinstance(y,int)):
		y = y.astype(np.int)

	kx = 2.0*np.pi/Lx
	ky = 2.0*np.pi/Ly

	td = 1/(nu*(kx*kx + ky*ky))

	return (2*rho[x,y]*nu*u0*np.sqrt(kx*ky)*np.sin(kx*x)*np.sin(ky*y)*(np.exp(-Iteration/td)) ) / (rho[x,y]*u0*u0)


#analytical TG solution for sigma_xx at t=Iteration  made adimensional by / (rho[x,y_cut]*u0*u0)

def TGaN_xy(x,y):

	if(not isinstance(x,int)):
		x = x.astype(np.int)
	if(not isinstance(y,int)):
		y = y.astype(np.int)

	kx = 2.0*np.pi/Lx
	ky = 2.0*np.pi/Ly

	td = 1/(nu*(kx*kx + ky*ky))

	return (rho[x,y]*nu*u0 * ( np.sqrt(kx*kx*kx/ky) - np.sqrt(ky*ky*ky/kx) ) * np.cos(kx*x)*np.cos(ky*y)*(np.exp(-Iteration/td)) )/ (rho[x,y]*u0*u0)


#Pellegrino analytical solutions made adimensional by / (rho[x,y_cut]*u0*u0)

def TGpN_xx(x, eta, eta_H):

	if(not isinstance(x,int)):
		x = x.astype(np.int) 

	return (2*eta*Par_x(x,y_cut,ux) + eta_H * ( Par_x(x,y_cut,uy) + Par_y(x,y_cut,ux) ) )/ (rho[x,y_cut]*u0*u0)

def TGpN_xy(x, eta, eta_H):

	if(not isinstance(x,int)):
		x = x.astype(np.int)


	return (eta * ( Par_x(x,y_cut,uy) + Par_y(x,y_cut,ux) ) + eta_H * ( -Par_x(x,y_cut,ux) + Par_y(x,y_cut,uy) ) )/ (rho[x,y_cut]*u0*u0)


#data arrays to be compared

data_an_xx   = np.empty(Lx, dtype='float64')
data_an_xy   = np.empty(Lx, dtype='float64')
data_num_xx  = np.empty(Lx, dtype='float64')
data_num_xy  = np.empty(Lx, dtype='float64')
data_per_xx  = np.empty(Lx, dtype='float64')
data_per_xy  = np.empty(Lx, dtype='float64')





for i in range(0,Lx):
	data_an_xx[i]  = TGaN_xx(i,20)    
	data_an_xy[i]  = TGaN_xy(i,20)  
	data_num_xx[i] = sigma_xx[i,20] / (rho[i,y_cut]*u0*u0)    #le altre funzioni hanno un /rhou2 all'interno. Questi sono i dati grezzi ancora da dividere 
	data_num_xy[i] = sigma_xy[i,20] / (rho[i,y_cut]*u0*u0)


##lmfit

# residual function to minimize
def fit_function(params, x=None, dat1=None, dat2=None):

    model1 = TGpN_xx(x, params['eta'], params['eta_H'])
    model2 = TGpN_xy(x, params['eta'], params['eta_H'])

    resid1 = dat1 - model1
    resid2 = dat2 - model2

    return np.concatenate((resid1, resid2))

# setup fit parameters
params = Parameters()
params.add('eta_H', value =-0.0001)
params.add('eta'  , value = 0.33  )

x_data = np.arange(Lx, dtype=np.int)

# fit
out = minimize(fit_function, params, kws={"x": x_data, "dat1": data_num_xx, "dat2": data_num_xy})
#print(fit_report(out))
print(out.params['eta_H'].value)


#values for Pellegrino's fitted functions

for i in range(0,Lx):
	data_per_xx[i] = TGpN_xx(i,out.params['eta'].value, out.params['eta_H'].value)
	data_per_xy[i] = TGpN_xy(i,out.params['eta'].value, out.params['eta_H'].value)



#export data to csv

savetxt('xxnum' + str(1) + '.csv', data_num_xx, delimiter=',')
savetxt('xxP'   + str(1) + '.csv', data_per_xx, delimiter=',')
savetxt('xxB'   + str(1) + '.csv', data_an_xx , delimiter=',')

savetxt('xynum' + str(1) + '.csv', data_num_xy, delimiter=',')
savetxt('xyP'   + str(1) + '.csv', data_per_xy, delimiter=',')
savetxt('xyB'   + str(1) + '.csv', data_an_xy , delimiter=',')



#plot

fig, (ax1, ax2) = plt.subplots(2, figsize=(5,5), dpi=200)

#fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2)

ax1.plot(data_an_xx ,'g--', markersize=1, label = "B=0 analytical")
ax1.plot(data_num_xx, 'bv', markersize=1, label = r"numerical")
ax1.plot(data_per_xx, 'ro', markersize=1, label = r"Pellegrino")
ax1.legend()
ax1.grid()
ax1.set(ylabel=r"$\sigma_{xx}$/$P_0$ at y=20")


ax2.plot(data_an_xy , 'g--', markersize=1, label = "B=0 analytical")
ax2.plot(data_num_xy, 'bv', markersize=1, label = r"numerical")
ax2.plot(data_per_xy, 'ro', markersize=1, label = r"Pellegrino")
ax2.legend()
ax2.grid()
ax2.set(xlabel="Lx[lu]", ylabel=r"$\sigma_{xy}/P_0$ at y=20")



fig.suptitle(r'k=1e-2, $\eta=$' + str("{:.2e}".format(out.params['eta'].value)) + r', $\eta_H=$' + str("{:.2e}".format(out.params['eta_H'].value)) + ', u0 = 0.04,' + r'$ \tau = 1.5$,' + 'Lx=150, Ly=100,'+ 't=250', fontsize=13)

plt.show()

