#generic plotting script (adapted for 3x3 grids)

import numpy as np
import matplotlib.pyplot as plt
from numpy import loadtxt
import sys
import decimal
from matplotlib.patches import BoxStyle


class MyRArrow(BoxStyle.RArrow):
        def __init__(self, pad=0.3, width=220):
            self.width_ = width
            super(MyRArrow, self).__init__(pad)

        def transmute(self, x0, y0, width, height, mutation_size):
            p = BoxStyle.RArrow.transmute(self, x0, y0,
                                          width, height, mutation_size)
            x = p.vertices[:, 0]
            p.vertices[1:3, 0] = x[1:3] - self.width_
            p.vertices[0, 0]   = x[0]   + self.width_
            p.vertices[3:, 0]  = x[3:]  + self.width_
            return p

BoxStyle._style_list["myrarrow"] = MyRArrow


def format_number(num):

    try:
        dec = decimal.Decimal(num)
    except:
        return 'bad'
    tup = dec.as_tuple()
    delta = len(tup.digits) + tup.exponent
    digits = ''.join(str(d) for d in tup.digits)
    if delta <= 0:
        zeros = abs(tup.exponent) - len(tup.digits)
        val = '0.' + ('0'*zeros) + digits
    else:
        val = digits[:delta] + ('0'*tup.exponent) + '.' + digits[delta:]
    val = val.rstrip('0')
    if val[-1] == '.':
        val = val[:-1]
    if tup.sign:
        return '-' + val
    return val





dumpPath = "../dump/"

y_cut=0

dataH = np.loadtxt("fitvalueH.csv")

K    = dataH[:,0]
etaH = dataH[:,1]
t    = dataH[:,2]


for i in range(len(K)):
	K[i] = format_number(K[i])
	t[i] = format_number(t[i])



def find_etaH(k_,t_):                  #è scritta davvero male e dà per scontato che la tupla esista

	for i in range(len(K)):
		if( (k_ == K[i]) and (t_ == t[i]) ):
			return etaH[i]


class plotting:

	def __init__(self, component, k, time, y_cut):
		self.num=loadtxt(dumpPath + component + 'num'+ str(k)+ "_" +str(time) + "_" + str(y_cut) +'.csv', delimiter=',')
		self.B  =loadtxt(dumpPath + component + 'B'  + str(k)+ "_" +str(time) + "_" + str(y_cut) +'.csv', delimiter=',')
		self.pel=loadtxt(dumpPath + component + 'P'  + str(k)+ "_" +str(time) + "_" + str(y_cut) +'.csv', delimiter=',')



def plotter(data1, data2, ax, k, time, y_cut, eta_H):

	ax.title.set_text('K = '+str(k) + ', t=' + str(time) + r', $\eta_H$=' + str("{:.2e}".format(eta_H)) )

	ax.plot(data1.pel ,'r', label = r"$xx_{Pel}$")
	ax.plot(data1.num ,'bo', markersize=1 , label = r"$xx_{num}$")
	ax.plot(data2.pel ,'c', label = r"$xy_{Pel}$")
	ax.plot(data2.num ,'ko', markersize=1 , label = r"$xy_{num}$")

	ax.grid()
	ax.set(ylabel=r"$\sigma_{\alpha\beta}/P_0$ at y=" + str(y_cut))
	#ax.legend(loc='best', fontsize='small')

	return ax





K1 = float(sys.argv[1])
K2 = float(sys.argv[2])
K3 = float(sys.argv[3])
t1 = int(sys.argv[4])
t2 = int(sys.argv[5])
t3 = int(sys.argv[6])


p1 = plotting('xx',K1,t1,y_cut)
p2 = plotting('xy',K1,t1,y_cut)

p3 = plotting('xx',K2,t1,y_cut)
p4 = plotting('xy',K2,t1,y_cut)

p5 = plotting('xx',K3,t1,y_cut)
p6 = plotting('xy',K3,t1,y_cut)


p7 = plotting('xx',K1,t2,y_cut)
p8 = plotting('xy',K1,t2,y_cut)

p9 = plotting('xx',K2,t2,y_cut)
p10 = plotting('xy',K2,t2,y_cut)

p11 = plotting('xx',K3,t2,y_cut)
p12 = plotting('xy',K3,t2,y_cut)



p13 = plotting('xx',K1,t3,y_cut)
p14 = plotting('xy',K1,t3,y_cut)

p15 = plotting('xx',K2,t3,y_cut)
p16 = plotting('xy',K2,t3,y_cut)

p17 = plotting('xx',K3,t3,y_cut)
p18 = plotting('xy',K3,t3,y_cut)




#plot

fig, ( (ax1, ax2, ax3), (ax4, ax5, ax6), (ax7, ax8, ax9) ) = plt.subplots(3,3, figsize=(15,15), dpi=400)


plotter(p1,p2,ax1,K1,t1,y_cut,find_etaH(K1,t1))
plotter(p3,p4,ax2,K2,t1,y_cut,find_etaH(K2,t1))
plotter(p5,p6,ax3,K3,t1,y_cut,find_etaH(K3,t1))

plotter(p7,p8,ax4,K1,t2,y_cut,find_etaH(K1,t2))
plotter(p9,p10,ax5,K2,t2,y_cut,find_etaH(K2,t2))
plotter(p11,p12,ax6,K3,t2,y_cut,find_etaH(K3,t2))

plotter(p13,p14,ax7,K1,t3,y_cut,find_etaH(K1,t3))
plotter(p15,p16,ax8,K2,t3,y_cut,find_etaH(K2,t3))
plotter(p17,p18,ax9,K3,t3,y_cut,find_etaH(K3,t3))

#plt.tight_layout()

plt.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=0.4, hspace=None)

handles, labels = ax9.get_legend_handles_labels()
fig.legend(handles, labels, loc='center right')

bbox_props = dict(boxstyle="myrarrow,pad=0.2, width=1500", fc="black", ec="b", lw=1)
t = ax1.text(0.5, 0.95, "K", color='b' ,ha="center", va="top", rotation=0, size=15, bbox=bbox_props, transform=fig.transFigure)

bbox_props = dict(boxstyle="myrarrow,pad=0.2, width=1500", fc="black", ec="b", lw=1)
t = ax4.text(0.05, 0.5, "t", color='b' ,ha="center", va="top", rotation=270, size=15, bbox=bbox_props, transform=fig.transFigure)
plt.savefig("prova.png")

#plt.show()