#!/bin/sh

for K in `seq 0.001 0.001 0.1`
do
  
	cd /home/m/Desktop/tesi/TaylorGreen/magnetic/src/
	./TG $K
	cd /home/m/Desktop/tesi/TaylorGreen/magnetic/script/
	python3 fit_etaH_tot.py $K
done
