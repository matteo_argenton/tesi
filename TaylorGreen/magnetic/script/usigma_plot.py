import numpy as np
import matplotlib.pyplot as plt
from numpy import loadtxt




data_num1 = loadtxt('datanum_u-10.csv', delimiter=',')
data_num2 = loadtxt('datanum_u-3.csv', delimiter=',')
data_num3 = loadtxt('datanum_u-2.csv', delimiter=',')

data_an1 = loadtxt('dataan_u-10.csv', delimiter=',')


data_numsigma1 = loadtxt('datanum_sigma-10.csv', delimiter=',')
data_numsigma2 = loadtxt('datanum_sigma-3.csv', delimiter=',')
data_numsigma2 = loadtxt('datanum_sigma-2.csv', delimiter=',')

data_ansigma1 = loadtxt('dataan_sigma-10.csv', delimiter=',')


#plot


fig, (ax1, ax2) = plt.subplots(2, figsize=(5,5), dpi=200)


ax1.plot(data_an1 ,'c', label = "analytical" + " B=0")
ax1.plot(data_num1,'bo', markersize=1 , label = "numerical" + r", $K$ = $10^{-10}$")
ax1.plot(data_num2,'k^', markersize=1 , label = "numerical" + r", $K$ = $10^{-3}$")
ax1.plot(data_num3,'go', markersize=1 , label = "numerical" + r", $K$ = $10^{-2}$")
ax1.legend()
ax1.grid()
ax1.set(xlabel="Lx[lu]",ylabel=r"u/$u_0$ at y=20")


ax2.plot(data_ansigma1 ,'c', label = "analytical" + "B=0")
ax2.plot(data_numsigma1,'bo', markersize=1 , label = "numerical" + r", $K$ = $10^{-10}$")
ax2.plot(data_numsigma1,'k^', markersize=1.5 , label = "numerical" + r", $K$ = $10^{-3}$")
ax2.plot(data_numsigma2,'go', markersize=1 , label = "numerical" + r", $K$ = $10^{-2}$")
ax2.legend()
ax2.grid()
ax2.set(xlabel="Lx[lu]",ylabel=r"$\sigma_{xx}/P_0$ at y=20")


plt.show()