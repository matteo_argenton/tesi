#generic plotting script (adapted for 3x3 grids)

import numpy as np
import matplotlib.pyplot as plt
from numpy import loadtxt
import sys
import decimal
from matplotlib.patches import BoxStyle



def format_number(num):

    try:
        dec = decimal.Decimal(num)
    except:
        return 'bad'
    tup = dec.as_tuple()
    delta = len(tup.digits) + tup.exponent
    digits = ''.join(str(d) for d in tup.digits)
    if delta <= 0:
        zeros = abs(tup.exponent) - len(tup.digits)
        val = '0.' + ('0'*zeros) + digits
    else:
        val = digits[:delta] + ('0'*tup.exponent) + '.' + digits[delta:]
    val = val.rstrip('0')
    if val[-1] == '.':
        val = val[:-1]
    if tup.sign:
        return '-' + val
    return val



dumpPath = "../dump/"
y_cut=20



class plotting:

    def __init__(self, component, k, time, y_cut):
        self.an  = loadtxt(dumpPath + component + 'an' + str(k)+ "_" +str(time) + "_" + str(y_cut) +'.csv', delimiter=',')
        self.num = loadtxt(dumpPath + component + 'num'+ str(k)+ "_" +str(time) + "_" + str(y_cut) +'.csv', delimiter=',')
        self.ce1 = loadtxt(dumpPath + component + '1'  + str(k)+ "_" +str(time) + "_" + str(y_cut) +'.csv', delimiter=',')
        self.ce2 = loadtxt(dumpPath + component + '2'  + str(k)+ "_" +str(time) + "_" + str(y_cut) +'.csv', delimiter=',')
        self.ce3 = loadtxt(dumpPath + component + '3'  + str(k)+ "_" +str(time) + "_" + str(y_cut) +'.csv', delimiter=',')




def plotter(data1, component, ax, k, time, y_cut):

    ax.title.set_text('K = '+str(k) + ', t=' + str(time))
    ax.plot(data1.an  ,'k-' , label = str(component)+"B=0",alpha=0.2)
    ax.plot(data1.num ,'b-', markersize=1 , label = str(component)+" numeric")
    ax.plot(data1.ce1 ,'rx', markersize=2 , label = str(component)+r"$CE_1$")
    ax.plot(data1.ce2 ,'ko', markersize=2 , label = str(component)+r"$CE_2$")
    ax.plot(data1.ce3 ,'y^', markersize=1 , label = str(component)+r"$CE_3$")

    ax.grid()
    ax.set(ylabel=r"$\sigma_{"+str(component)+"}/P_0$ at y=" + str(y_cut), xlabel="Lx [lu]")
    ax.legend(loc='best', fontsize='small')

    return ax





K1 = float(sys.argv[1])
t1 = int(sys.argv[2])



p1 = plotting('xx',K1,t1,y_cut)
p2 = plotting('xy',K1,t1,y_cut)




#plot

fig, (ax1, ax2) = plt.subplots(2,1, figsize=(7,7), dpi=200)


plotter(p1,'xx',ax1,K1,t1,y_cut)
plotter(p2,'xy',ax2,K1,t1,y_cut)


ax1.text(-0.1, 1.05, "(a)", transform=ax1.transAxes, size=10)
ax2.text(-0.1, 1.05, "(b)", transform=ax2.transAxes, size=10)


#plt.tight_layout()

plt.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=0.4, hspace=0.4)

plt.savefig("prova2.pdf")

#plt.show()