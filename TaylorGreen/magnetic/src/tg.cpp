#include "param.h"
#include "tg_functions.h"
#include "lbm_tg.h"
#include "dump_tg.h"
#include <stdlib.h>
#include <errno.h>
#include <stdio.h>

double K;
unsigned int NSTEPS;

int main(int argc, char* argv[]){


	if(argc<3){
		perror("missing K, t arguments - run as ./TG <Kvalue> <t value>\n");
		exit(EXIT_FAILURE);        //qui c'è un qualche bug perché il programma esce comunque in success. Sarebbe da capire
	}

	K 	   = atof(argv[1]);
	NSTEPS = atoi(argv[2]);


	double *f1       = (double*) malloc(size_ndir);         		//populations, vector field
	double *f2       = (double*) malloc(size_ndir);
	double *rho      = (double*) malloc(size_scalar);	     		//desity, scalar field
	double *ux       = (double*) malloc(size_scalar);
	double *uy       = (double*) malloc(size_scalar);
	double *sigma_xx = (double*) calloc(NX*NY, sizeof(double));		//inizializzato a 0
	double *sigma_xy = (double*) calloc(NX*NY, sizeof(double));		//inizializzato a 0
	double *sigma_yy = (double*) calloc(NX*NY, sizeof(double));		//inizializzato a 0
	double *sigma_yx = (double*) calloc(NX*NY, sizeof(double));		//inizializzato a 0

	double Fi_max = 0.0;     //used to save the maximum value of F_i in the simulation
	double *temp;


	//dump folder existence check/creation
	init_dump_folder();

	knudsen_check();

	dump_settings();

	init_macro_var(rho,ux,uy);

	init_equilibrium(f1,rho,ux,uy);

	for(unsigned int i=0; i<NSTEPS; i++){

		compute_rho(f1,rho);
		compute_u(f1,rho,ux,uy);

			//if( (i==150) || (i==250) || (i==500) || (i==750)){                //controllo molto poco efficiente per estrarre i dati a tempi diversi in una singola run
				//compute_sigma(f1, sigma_xx, sigma_xy, sigma_yy, sigma_yx, ux, uy, rho);
				//get_data_iter(i,rho,ux,uy,sigma_xx, sigma_xy, sigma_yy, sigma_yx);
			//}


		collide(f1,rho,ux,uy,&Fi_max);
		stream(f1,f2);


		temp = f1;
		f1 = f2;
		f2 = temp;

		//get_data_iter(i,rho,ux,uy,sigma_xx,sigma_xy);	   //qui sono valori pre collide/stream, quindi di fatto ad un tempo t-1

	}

	compute_rho(f1,rho);					   //qui aggiorno i valori delle var macro all'ultima collide
	compute_u(f1,rho,ux,uy);
	compute_sigma(f1, sigma_xx, sigma_xy, sigma_yy, sigma_yx, ux, uy, rho);
	get_data(rho,ux,uy,sigma_xx, sigma_xy, sigma_yy, sigma_yx);
	printf("\n maximum Fi = %.10lf\n", Fi_max);


	free(f1);
	free(f2);
	free(rho);
	free(ux);
	free(uy);
	free(sigma_xx);
	free(sigma_xy);
	free(sigma_yy);
	free(sigma_yx);
}