#ifndef _DUMP_TG
#define _DUMP_TG

void init_dump_folder();
void get_data(double*,double*,double*,double*,double*,double*,double*);
void get_data_iter(unsigned int,double*,double*,double*,double*,double*,double*,double*);
void dump_settings();

#endif