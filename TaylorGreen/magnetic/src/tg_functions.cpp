#include <math.h>
#include "param.h"
#include "tg_functions.h"
#include <stdio.h>




void init_macro_var(double *rho, double *ux, double *uy){

	unsigned int X;
	unsigned int Y;

	double kx = 2.0*M_PI/NX;
	double ky = 2.0*M_PI/NY;

	double P;


	for(unsigned int y=0; y<NY; y++){
		for(unsigned int x=0; x<NX; x++){

			//0.5 is summed because the considered point is in the middle of each cell
			//X = x + 0.5;
			//Y = y + 0.5;

			X=x;
			Y=y;

			ux[scalar_index(x,y)] = -u_max*sqrt(ky/kx)*cos(kx*X)*sin(ky*Y);
			uy[scalar_index(x,y)] =  u_max*sqrt(kx/ky)*sin(kx*X)*cos(ky*Y);


			P = -0.25*rho0*u_max*u_max*( (ky/kx)*cos(2.0*kx*X)+(kx/ky)*cos(2.0*ky*Y) );   // sarebbe p0 - expr , vedi cosa cambia lasciando p0=0
			rho[scalar_index(x,y)] = rho0+3.0*P;


			/* tentativo con shift delle velocità come da (6.45) Kruger */

			// ux[scalar_index(x,y)] = -u_max*sqrt(ky/kx)*cos(kx*X)*sin(ky*Y);
			// ux[scalar_index(x,y)] =  ux[scalar_index(x,y)] + K * ux[scalar_index(x,y)]; 
			// uy[scalar_index(x,y)] =  u_max*sqrt(kx/ky)*sin(kx*X)*cos(ky*Y);
			// uy[scalar_index(x,y)] =  uy[scalar_index(x,y)] - K * uy[scalar_index(x,y)];
		}
	}
}




void init_equilibrium(double *f, double *r, double *u, double *v){

	double rho, ux, uy, feq;

	for(unsigned int y = 0; y < NY; y++){
        for(unsigned int x = 0; x < NX; x++){

            rho = r[scalar_index(x,y)];
            ux  = u[scalar_index(x,y)];
            uy  = v[scalar_index(x,y)];

            for(unsigned int i=0; i<ndir; i++){

				f[field_index(x,y,i)] = f_eq(x,y,i,ux,uy,rho);
            }

        }
    }
}



double f_eq(int x, int y, int i, double ux, double uy, double rho){


	int nx    = dirx[i];
	int ny    = diry[i];
	double w  = wi[i];

	double prod = (nx*ux + ny*uy)*(1./cs2);
	double uu   = (ux*ux + uy*uy)*(1./cs2);
	double nn   = (nx*nx + ny*ny)*(1./cs2);

    // Order 2 <- D2Q9
    double feq   = 1.0 + prod + 0.5*(prod*prod - uu);

    #ifdef D2Q17
	    // Order 3 <- D2Q17
	  	feq = feq + (prod/6.0)*(prod*prod - 3.0*uu);
	#endif

  	return w*rho*feq;
}



void compute_sigma(double *f, double *sigma_xx, double *sigma_xy, double *sigma_yy, double *sigma_yx, double *u, double *v, double *r){

	double rho, ux, uy, cidotu, feq;
	double Fix, Fiy;
	const double tauinv = 1.0/tau;

	for(unsigned int y = 0; y < NY; y++){
        for(unsigned int x = 0; x < NX; x++){

        	sigma_xx[scalar_index(x,y)] = 0.0;    //non dovrebbe servire per via della calloc
        	sigma_xy[scalar_index(x,y)] = 0.0;
        	sigma_yy[scalar_index(x,y)] = 0.0;    //non dovrebbe servire per via della calloc
        	sigma_yx[scalar_index(x,y)] = 0.0;
        	rho = r[scalar_index(x,y)];
			ux  = u[scalar_index(x,y)];
			uy  = v[scalar_index(x,y)];

        	for(unsigned int i=0; i<ndir; i++){

        		feq = f_eq(x,y,i,ux,uy,rho);

        		sigma_xx[scalar_index(x,y)] += (-1.0 + 0.5 * tauinv) * dirx[i]*dirx[i]  * ( f[field_index(x,y,i)] - feq );
        		sigma_xy[scalar_index(x,y)] += (-1.0 + 0.5 * tauinv) * dirx[i]*diry[i]  * ( f[field_index(x,y,i)] - feq );
        		sigma_yy[scalar_index(x,y)] += (-1.0 + 0.5 * tauinv) * diry[i]*diry[i]  * ( f[field_index(x,y,i)] - feq );
        		sigma_yx[scalar_index(x,y)] += (-1.0 + 0.5 * tauinv) * diry[i]*dirx[i]  * ( f[field_index(x,y,i)] - feq );    //sarà uguale alla xy
        	}
        	
        }
    }

}

void knudsen_check(){

	double c_s = sqrt(1./3.);
	double kn = nu/(c_s * NX);

	if(kn <= 0.005)
		printf("Il numero di Knudsen per la simulazione corrente è %lf, proseguo. (accettabili <= 0.005 - 0.001)\n", kn);
	else{
		fprintf(stderr, "Numero di knudsen troppo grande (%lf)", kn);
		exit(4);
	}
}