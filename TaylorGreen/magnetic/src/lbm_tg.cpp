#include "param.h"
#include "tg_functions.h"
#include "forcing.h"
#include <math.h>
#include <stdio.h>



void stream(double *f_src, double* f_dst){

	unsigned int x_dst;
	unsigned int y_dst;

	for(unsigned int y = 0; y < NY; y++){
		for(unsigned int x = 0; x < NX; x++){
			for(unsigned int i = 0; i < ndir; i++){

				x_dst = (NX+x-dirx[i]) % NX;       //periodic boundaries
				y_dst = (NY+y-diry[i]) % NY;

				f_dst[field_index(x,y,i)] = f_src[field_index(x_dst,y_dst,i)];
			}
		}
	}
}


void compute_rho(double *f, double *r){

	double rho;

	for(unsigned int y = 0; y < NY; y++){
		for(unsigned int x = 0; x < NX; x++){

			rho = 0.0;

			for(unsigned int i = 0; i < ndir; ++i){

				rho += f[field_index(x,y,i)];
			}

			r[scalar_index(x,y)] = rho;
		}
	}
}



void compute_u(double *f, double *r, double *u, double *v){

	double rho;
	double ux;
	double uy;
	double prefac;

	for(unsigned int y = 0; y < NY; y++){
		for(unsigned int x = 0; x < NX; x++){

			rho = r[scalar_index(x,y)];								//da calcolare prima
			prefac = 1.0 / ( rho * (1.0 + K*K) );				    //prefattore che dipende da rho
			ux  = 0.0;
			uy  = 0.0;


			for(unsigned int i = 0; i < ndir; ++i){			

				ux += ( f[field_index(x,y,i)] * dirx[i] ) + ( K * f[field_index(x,y,i)] * diry[i] );
				uy += ( f[field_index(x,y,i)] * diry[i] ) - ( K * f[field_index(x,y,i)] * dirx[i] );

			}

			u[scalar_index(x,y)] = ux*prefac;
			v[scalar_index(x,y)] = uy*prefac;			

		}
	}
}




void collide(double *f, double *r, double *u, double *v, double* Fi_max){

	double rho, ux, uy, cidotu, feq;
	double source = 0.0;
	double Fi = 0.0;

	const double tauinv   = 1.0/tau; 		// 1/tau
	const double omtauinv = 1.0-tauinv; 	// 1 - 1/tau

		for(unsigned int y = 0; y < NY; y++){
			for(unsigned int x = 0; x < NX; x++){

				rho = r[scalar_index(x,y)];
				ux  = u[scalar_index(x,y)];
				uy  = v[scalar_index(x,y)];

					for(unsigned int i = 0; i < ndir; i++){

						feq = f_eq(x,y,i,ux,uy,rho);

						Fi = F_i(rho, ux, uy, i);
						source = (1 - 0.5*tauinv) * Fi;

						if(abs(Fi) > abs(*Fi_max))    //checks the magnitude of F_i to save the maximum value used in the simulation
							*Fi_max = Fi;

						f[field_index(x,y,i)] = omtauinv*f[field_index(x,y,i)] + tauinv*feq + source;

					}
			}
		}
}
