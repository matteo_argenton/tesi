#include "param.h"

double F_i(double rho, double u, double v, unsigned int i){  //notice that the input values are already extracted from the fields and are not pointers

	double term1 = 0.0, term2 = 0.0, term3 = 0.0, Fi = 0.0;
	
	double g_i_x =  rho * 2 * K * diry[i];
	double g_i_y = -rho * 2 * K * dirx[i];

	term1 =  wi[i] * (dirx[i]*g_i_x + diry[i]*g_i_y) / cs2;
	term2 =  wi[i] * (dirx[i]*g_i_x + diry[i]*g_i_y) * (dirx[i]*u + diry[i]*v) / (cs2*cs2);
	term3 = -wi[i] * (u * g_i_x + v * g_i_y) / cs2;

	Fi = term1 + term2 + term3;

	return Fi;
}
