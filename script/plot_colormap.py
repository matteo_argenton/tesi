import numpy as np
import matplotlib.pyplot as plt
import matplotlib as m

###########################################################################
# param

W = 200
L = 500

rbar = 1

###########################################################################
# Read data

#data = np.genfromtxt('../dump/500x200x20/result214.dat'  , dtype='float64', delimiter=' ')
data = np.genfromtxt('../dump/codice_pre_forzante/500x200x6_inout_normali_1_12_report/result383.dat'  , dtype='float64', delimiter=' ')

x   = data[:,1]
y   = data[:,0]
rh  = data[:,2]
ux  = data[:,3]
uy  = data[:,4]

x    = np.reshape(x  , ( W, L ) )
y    = np.reshape(y  , ( W, L ) )
rh   = np.reshape(rh , ( W, L ) )
ux   = np.reshape(ux , ( W, L ) )
uy   = np.reshape(uy , ( W, L ) )

phi  = rh - rbar

###########################################################################

w=h=15



fig = plt.figure(figsize=(w,h), dpi=200)
ax  = fig.add_subplot(111)


scale = 0.01
vmin  = np.min(phi)*scale
vmax  = np.max(phi)*scale

cm = ax.imshow(phi, vmin=vmin, vmax=vmax, cmap='jet')
ax.streamplot(x, y, ux, uy, color='w', linewidth=2)
fig.colorbar(cm, ax=ax, shrink=.5, label=r'$u(x,y)$')


plt.show()