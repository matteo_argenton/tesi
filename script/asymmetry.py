import numpy as np



###########################################################################
#geometria e densità all'equilibrio

W    = 200
L    = 500

rbar = 1.0


###########################################################################
#acquisizione dati

rho = np.genfromtxt('../dump/500x200x6_inout_normali_18_12/result39.dat', dtype='float64', delimiter=' ', usecols=2)  #2 è la colonna delle densità
rho = np.reshape(rho , ( W, L ) )

#calcolo il valore di phi per il primo e l'ulitmo punto del reticolo e li confronto per capire di quanto si discostano
#in una prossima versione probabilmente posso considerare un certo numero di valori in testa e in coda, per evitare che comportamenti strani
#agli estremi mi restituiscano differenze poco indicative

y = [7,10,13,15]

first    = np.zeros(len(y))
mid      = np.zeros(len(y))
last     = np.zeros(len(y))
diff     = np.zeros(len(y))


for j in range(0,len(y)):										   #considero il primo punto per ogni curva (a diverse distanze dall'in/out)

	first[j]     =  rho[j,0] - rbar
	mid[j]       =  rho[j,L//2] - rbar
	last[j]  	 =  rho[j,L-1] - rbar
	diff[j]  	 =  first[j] - last[j]

      
for j in range(0,len(y)):
	print( "Con y = " + str(y[j]) + " : sx=" + '{:.3g}'.format(first[j]) + " mid = " + '{:.3g}'.format(mid[j]) +" dx = " + '{:.3g}'.format(last[j]) )

print("valore della differenza tra gli estremi:")

for j in range(0,len(y)):
	print( '{:.3g}'.format(diff[j]) )