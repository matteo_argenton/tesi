#script rapido per preparare il confronto tra due file dat di due versioni di codice (uno era da riscalare)

import numpy as np
import pandas as pd

###########################################################################
# param

W = 200
L = 500

###########################################################################
# Read data from test file (1...200)

data = np.genfromtxt('../dump/500x200x6test/result686.dat'  , dtype='float64', delimiter=' ')

y   = data[:,0]
x   = data[:,1]
rh  = data[:,2]
ux  = data[:,3]
uy  = data[:,4]


x = x -1
y = y -1

np.savetxt('scaled.dat', np.transpose([y,x,rh,ux,uy]), delimiter=' ')

###########################################################################
# Read data from old file (0...199)

data = np.genfromtxt('../dump/500x200x6/result686.dat'  , dtype='float64', delimiter=' ')

y   = data[:,0]
x   = data[:,1]
rh  = data[:,2]
ux  = data[:,3]
uy  = data[:,4]

np.savetxt('scaled_old.dat', np.transpose([y,x,rh,ux,uy]), delimiter=' ')