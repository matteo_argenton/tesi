import numpy as np
import matplotlib.pyplot as plt

###########################################################################
# param

W = 200
L = 500

rbar       = 1.0
vin        = 1e-5
in_out_dim = 6
tau        = 1.5
cs2        = 1./3.
nu         = (tau - 0.5)*cs2 



x0_in_start  = 167
x0_out_start = 329


x0in  = x0_in_start  + (in_out_dim / 2.) -1
x0out = x0_out_start + (in_out_dim / 2.) -1


#per invertire inlet e outlet
#x0out  = x0_in_start  + (in_out_dim / 2.) -2
#x0in = x0_out_start + (in_out_dim / 2.) 

# x0in  = x0_in_start  + (in_out_dim / 2.) - L//2 - 2
# x0out = x0_out_start + (in_out_dim / 2.) - L//2 #- 2

#ridefinisco A, che è basato sull'ampiezza in/out, ma è corretto +-1, +-2 per debuggare il fit
A = 6 + 2

############################################################################
# soluzione analitica

def F2(x,y): return np.pi*(1 - np.cosh( np.pi*x/W )*np.cos( np.pi*y/W ) ) / ( np.cosh( np.pi*x/W ) - np.cos( np.pi*y/W ) )**2
def phiModel(x, y): return (nu*vin*A)/(rbar*W**2) * ( F2(x-x0in, y) - F2(x-x0out, y))

###########################################################################
# Read data

# data = np.genfromtxt('../dump/500x200x20/result214.dat'  , dtype='float64', delimiter=' ')
data = np.genfromtxt('../dump/'+ str(L) +'x200x6_inout_normali_18_12/result39.dat'  , dtype='float64', delimiter=' ')


x   = data[:,1]
y   = data[:,0]
rh  = data[:,2]
ux  = data[:,3]
uy  = data[:,4]
#pot = data[:,5]

x    = np.reshape(x  , ( W, L ) )
y    = np.reshape(y  , ( W, L ) )
rh   = np.reshape(rh , ( W, L ) )
ux   = np.reshape(ux , ( W, L ) )
uy   = np.reshape(uy , ( W, L ) )
#pot  = np.reshape(pot, ( W, L ) )

#phi  = pot + (rh - rbar)
phi = rh - rbar

###########################################################################

# x = np.arange(-L/2, L/2)
x = np.arange(0, L)

y1 =  7
y2 = 10
y3 = 13

######################################################################################
# compare with analytic solution
'''
plt.title("Potenziale elettrochimico \u03A6")

plt.plot(x, phiModel(x, y1), '--', linewidth = 1, color = 'red',label='y = ' + str(y1))
plt.plot(x, phi[y1, :], 'ro', markersize=1, label='y_num = ' + str(y1))


plt.plot(x, phiModel(x, y2), '--', linewidth = 1, color = 'blue',label='y = ' + str(y2))
plt.plot(x, phi[y2, :], 'bo', markersize=1, label='y_num = ' + str(y2))

plt.plot(x, phiModel(x, y3), '--', linewidth = 1, color = 'green',label='y = ' + str(y3))
plt.plot(x, phi[y3, :], 'go', markersize=1, label='y_num = ' + str(y3))


plt.legend(loc='best')
plt.xlabel("L")
plt.ylabel("\u03A6")

plt.grid()
plt.show()

'''


#######################################################################################
#new plot

w=h=15


fig = plt.figure(figsize=(w,h), dpi=200)
ax  = fig.add_subplot(111)

#ax.plot(x, phiModel(x, y1), '--', linewidth = 1, color = 'red',label='y = ' + str(y1))
ax.plot(x, phi[y1, :], 'ro', markersize=1, label='y_num = ' + str(y1))


#ax.plot(x, phiModel(x, y2), '--', linewidth = 1, color = 'blue',label='y = ' + str(y2))
ax.plot(x, phi[y2, :], 'bo', markersize=1, label='y_num = ' + str(y2))

#ax.plot(x, phiModel(x, y3), '--', linewidth = 1, color = 'green',label='y = ' + str(y3))
ax.plot(x, phi[y3, :], 'go', markersize=1, label='y_num = ' + str(y3))



ax.grid()
ax.legend()
plt.show()