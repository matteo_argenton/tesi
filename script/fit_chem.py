#Computes the fit from a system configuration dump "result214.dat" and an equilibrium distribution file "resultsNdef.dat"

#Parametri:
# Lunghezza  L= 500
# altezza W = 200
# ampiezza in/out A = 20
# inlet_start L/4
# outlet start (3*L/4 - A)
# velocità in/out 1e-5
# nu = 0.5
# tau = 3.0*nu+0.5

# y è la lista dei valori di distanza verticale da in/out

import numpy as np
import statistics as stat
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

###########################################################################
#geometria

W = 200
L = 500
in_out_dim = 6



#il punto centrale delle curve per comodità è ottenuto dal fit, con contraint che stia tra questi x0_in/out +-1

#x0_in_start  = L//3
#x0_out_start = 2*L//3

x0_in_start  = 167
x0_out_start = 329

x0_in = x0_in_start + (in_out_dim / 2.) -1         #il -1 perché l'estremo inferiore è incluso (si conta da 0)
x0_out = x0_out_start + (in_out_dim / 2.) -1


#print(x0_in)
#print(x0_out)


############################################################################
#funzione analitica per il fit

def F2(x,y): return -np.pi*(1 - np.cosh( np.pi*x/W )*np.cos( np.pi*y/W ) ) / ( np.cosh( np.pi*x/W ) - np.cos( np.pi*y/W ) )**2

def FF2(x, k, x0in, x0out, y): return k * (F2(x-x0in, y) - F2(x-x0out, y))

x  = np.arange(0, L, 1)
y = [7,10,13,15]



##############################################################################
#datahandle da file dump della simulazione - resultsNdef ha i valori di densità all'equilibrio - result* contiene i dati dell'ultima iterazione organizzati come y, x, rho, ux, uy

data_main = np.genfromtxt('../dump/codice_pre_forzante/500x200x6_inout_normali_4_12_report/result39.dat', dtype='float64', delimiter=' ', usecols=(0,1,2,3,4))
#data_n_ = np.genfromtxt('resultsNdef.dat', dtype='float64', delimiter=' ', usecols=2)


plot_phi=data_main[:,np.array([False, False, True, False, False])]


#dato che il file contiene dati sequenziali ho bisogno di nuove variabili y_data = y -1 per considerare effettivamente i dati ad altezza y
y_data = [y[0]-1, y[1]-1, y[2]-1, y[3]-1]



dP_1 = np.zeros(L)  
dP_2 = np.zeros(L)  
dP_3 = np.zeros(L)
dP_4 = np.zeros(L)

dP = [dP_1, dP_2, dP_3, dP_4]

for j in range(0,len(dP)):
	for i in range(0,L):
		dP[j][i] = (plot_phi[y_data[j]*L+i] - 1.0)/plot_phi[y_data[j]*L+i]


##############################################################################
#fit

fit_parameters = []
fit_pcov = []

for i in range(0,len(y)):
	fitk, pcov = curve_fit(lambda x, k, x0in, x0out: FF2(x, k, x0in, x0out, y[i]) , x, dP[i], bounds=((-np.inf, x0_in-1, x0_out-1), (np.inf, x0_in+1, x0_out+1)))
	fit_parameters.append(fitk)
	fit_pcov.append(pcov)




#k singoli ottenuti dai fit	
k_to_print = np.zeros(len(y))
for i in range(0,len(y)):
	k_to_print[i]=fit_parameters[i][0]

print(k_to_print)

#k medio unico
avg_k = stat.mean(k_to_print)


#coordinata centrale inlet (media dei fit)
avg_x0_in = []
for i in range(0,len(y)):
	avg_x0_in.append(fit_parameters[i][1])

#coordinata centrale outlet (mediadei fit)
avg_x0_out = []
for i in range(0,len(y)):
	avg_x0_out.append(fit_parameters[i][2])

avg_in = stat.mean(avg_x0_in)
avg_out = stat.mean(avg_x0_out)

#parametri medi da usare per ogni curva
fit_params_avg = [avg_k, avg_in, avg_out]

#print(avg_k)
print(avg_in)
print(avg_out)

###############################################################################



#codice per i 4 plot separati
'''
fig, axs = plt.subplots(2, 2)


axs[0, 0].plot(x, FF2(x,*fit_parameters[0], y[0]), label='y = ' + str(y[0]))
axs[0, 0].plot(dP[0],'ro', markersize=1, label='y_num = ' + str(y[0]))
axs[0, 0].legend(loc='best')
axs[0, 0].set_title('k='+str(np.format_float_scientific(k_to_print[0], precision = 3)))

axs[0, 1].plot(x, FF2(x,*fit_parameters[1], y[1]), label='y = ' + str(y[1]))
axs[0, 1].plot(dP[1],'ro', markersize=1, label='y_num = ' + str(y[1]))
axs[0, 1].legend(loc='best')
axs[0, 1].set_title('k='+str(np.format_float_scientific(k_to_print[1], precision = 3)))

axs[1, 0].plot(x, FF2(x,*fit_parameters[2], y[2]), label='y = ' + str(y[2]))
axs[1, 0].plot(dP[2],'ro', markersize=1, label='y_num = ' + str(y[2]))
axs[1, 0].legend(loc='best')
axs[1, 0].set_title('k='+str(np.format_float_scientific(k_to_print[2], precision = 3)))

axs[1, 1].plot(x, FF2(x,*fit_parameters[3], y[3]), label='y = ' + str(y[3]))
axs[1, 1].plot(dP[3],'ro', markersize=1, label='y_num = ' + str(y[3]))
axs[1, 1].legend(loc='best')
axs[1, 1].set_title('k='+str(np.format_float_scientific(k_to_print[3], precision = 3)))

for ax in axs.flat:
    ax.set(xlabel='x', ylabel='phi')
'''
######################################################################################
#codice per il plot sovrapposto e riadattato con le impostazioni per dpi e size


w=h=15

fig = plt.figure(figsize=(w,h), dpi=200)
ax  = fig.add_subplot(111)

ax.plot(x, FF2(x, *fit_params_avg, y[0]), '--', linewidth = 1, color = 'red',label='y = ' + str(y[0]))
ax.plot(dP[0],'ro', markersize=1, label='y_num = ' + str(y[0]))


ax.plot(x, FF2(x, *fit_params_avg, y[1]), '--', linewidth = 1, color = 'blue',label='y = ' + str(y[1]))
ax.plot(dP[1],'bo', markersize=1, label='y_num = ' + str(y[1]))


ax.plot(x, FF2(x, *fit_params_avg, y[2]), '--', linewidth = 1, color = 'green',label='y = ' + str(y[2]))
ax.plot(dP[2], 'go', color = 'green', markersize=1, label='y_num = ' + str(y[2]))


plt.legend(loc='best')
plt.xlabel("L")
plt.ylabel(u'\u03A6')
plt.grid()


plt.show()
