#script rapido per controllare cosa succede nel reticolo ad un certo passo (riadattato da notebook jupyter)

import numpy as np
import matplotlib.pyplot as plt


Lx=800
Ly=200

data = np.loadtxt("../dump/800x200x6_inout_normali_18_12/result28.dat")

rho  = data[:,2]
ux   = data[:,3]
uy   = data[:,4]

ux    = np.reshape(ux , (Ly, Lx))  
uy    = np.reshape(uy , (Ly, Lx))  
rho   = np.reshape(rho, (Ly, Lx))  
rhoeq = 1.

u = (ux**2 + uy**2)**0.5

x    = np.arange(0, Lx) 
y    = np.arange(0, Ly) 
X, Y = np.meshgrid(x, y)



############################################################################################
#plot

fig, axs = plt.subplots(2,1)

axs[0].set_title("Linee di flusso della velocità")
axs[0].streamplot(X, Y, ux, uy, color = u, linewidth = 2, cmap ='autumn')

axs[1].set_title("Controllo sulla differenza di densità")
strm = axs[1].imshow(rho-rhoeq, cmap='jet')
fig.colorbar(strm, orientation="horizontal")



plt.show()

'''

fig=plt.figure("Flusso")

plt.subplot(211)
strm1 = plt.streamplot(X, Y, ux, uy, color = u, linewidth = 2, cmap ='autumn') 
plt.colorbar(strm1.lines) 


plt.subplot(212)
strm2 = plt.imshow(rho-rhoeq, cmap='jet')

plt.colorbar(strm2, orientation="horizontal")
plt.title("n-" + u'n\u0304')
plt.xlabel("L")
plt.ylabel("W")

plt.show()

'''

